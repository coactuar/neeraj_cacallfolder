<?php
require_once "logincheck.php";
require_once "functions.php";

$exhib_id = '5d0bca2e4c426ee2731f58af2bae6087b66aaf165b78e21a7dae60db7030956c';
require_once "exhibcheck.php";
$curr_room = 'bonnxt';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg" class="bonnxt">
            <img src="assets/img/stalls/bonnxt.jpg">
            <div id="back-button">
                <a href="exhibitionhalls.php"><i class="fas fa-arrow-alt-circle-left"></i> Back</a>
            </div>
            <a href="assets/resources/bonnxt_1.jpg" id="poster1" class="view">
                <div class="indicator d-6"></div>
            </a>
            <a href="https://player.vimeo.com/video/588646587?h=8c5bec018e" id="video1" class="viewvideo"> </a>
            <a href="assets/resources/bonnxt_2.jpg" id="poster2" class="view">
                <div class="indicator d-6"></div>
            </a>
            <a href="#" data-exhid="<?php echo $exhib_id; ?>" data-userid="<?php echo $_SESSION['userid']; ?>" id="subSampleReq">
                <div class="indicator d-6"></div>
            </a>
            <a href="#" data-exhid="<?php echo $exhib_id; ?>" data-userid="<?php echo $_SESSION['userid']; ?>" id="subProdDet">
                <div class="indicator d-6"></div>
            </a>
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>

<?php require_once "scripts.php" ?>

<?php require_once "exhib-script.php" ?>

<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>