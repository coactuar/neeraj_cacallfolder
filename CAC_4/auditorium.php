<?php
require_once "logincheck.php";
require_once "functions.php";

$audi_id = '0fd013a85fbd8008cc4f0a6a3aa2ce4012c125fccca6112c42b93ef8777e1718';
$audi = new Auditorium();
$audi->__set('audi_id', $audi_id);
$a = $audi->getEntryStatus();
$entry = $a[0]['entry'];
if (!$entry) {
    header('location: lobby.php');
}
$curr_room = 'auditorium';
$webcastUrl = ''; //'https://player.vimeo.com/video/588253414?h=3d4bf1a391&autoplay=1';

$sess_id = 0;
if (isset($_GET['ses'])) {
    $sess_id = $_GET['ses'];
    $sess = new Session();
    $sess->__set('session_id', $sess_id);
    $curr_sess = $sess->getSession();
    if ((empty($curr_sess)) || (!$curr_sess[0]['launch_status'])) {
        header('location: lobby.php');
    }

    $webcastUrl = $sess->getWebcastSessionURL();
    $webcastUrl .= '&autoplay=1';
} else {
    //$webcastUrl .= '?autoplay=1&loop=1';
}
?>
<?php require_once 'header.php';  ?>

<?php require_once 'preloader.php';  ?>

<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/Auditorium_5.png">
            <div id="webcast-area">
                <a id="goFS" href="#" class="fs">Fullscreen</a>
                <iframe src="video1.php" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen title="Insignia"></iframe>
              
              
                <!-- <iframe src="<?= $webcastUrl ?>" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen title="Insignia"></iframe>
            -->
            </div>
            <div id="audiAgenda">
                <a href="" class="showpdf"><i class="far fa-list-alt"></i>Agenda</a>
            </div>
            <!-- <div id="audiDay1">
                <a href="?ses=cdfe4f62d6f2ff1aaa5ef24ffaae44f8ea9bd96b4502afaaacdf4083ab9cf5bb">Day 1</a>
            </div>
            <div id="audiDay2">
                <a href="?ses=4bfe9544ac6624e49f9b60d96e470f25b7955e9caf7cf8f22a3d868a349f33b6">Day 2</a>
            </div> -->
            
            <?php
            if ($sess_id != '0') {
            ?>
                <div id="ask-ques">
                    <a href="#" id="askques"><i class="fas fa-question-circle"></i>Ask Ques</a>
                </div>

                <div class="panel ques">
                    <div class="panel-heading">
                        Ask A Question
                        <a href="#" class="close" id="close_ques"><i class="fas fa-times"></i></a>
                    </div>
                    <div class="panel-content">
                        <div id="ques-message" ></div>
                        <form>
                            <div class="form-group">
                                <textarea class="input" rows="6" name="userques" id="userques" required></textarea>
                            </div>
                            <div class="form-group">
                                <button type="button" name="send_sesques" data-ses="<?= $sess_id ?>" data-user="<?= $userid ?>" class="send_sesques btn btn-sm btn-primary btn-sendmsg">Submit Question</button>
                            </div>
                        </form>
                        <div id="askedQues">
                            <div id="quesList" class="scroll">

                            </div>

                        </div>
                    </div>

                </div>
                <div class="panel poll">
                <div class="panel-heading">
                    Take Poll
                    <a href="#" class="close" id="close_poll"><i class="fas fa-times"></i></a>
                </div>
                <div class="panel-content">
                    <div id="poll-message" ></div>
                    <div id="currpollid" >0</div>
                    <div id="currpoll" ></div>
                    <div id="currpollresults" ></div>
                </div>
            </div>

            <?php
            }
            ?>
        </div>

        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
</div>
<?php require_once "commons.php" ?>
<?php require_once "scripts.php" ?>
<?php require_once "audi-common.php" ?>
<?php require_once "audi-script.php" ?>
<?php require_once "ga.php"; ?>
<?php require_once 'footer.php';  ?>