<?php
require_once 'functions.php';

$errors = [];
$succ = '';

$fname = '';
$lname = '';
$emailid = '';
$mobile = '';
$state = 0;
$city = 0;
$country = 0;
 $teamid = '';
$speciality = 0;
$question='';
$hospital='';
// $updates = '';
// $updatesArr = [];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (empty($_POST['fname'])) {
        $errors['fname'] = 'First Name is required';
    }
    if (empty($_POST['lname'])) {
        $errors['lname'] = 'Last Name is required';
    }
    if (empty($_POST['emailid'])) {
        $errors['email'] = 'Email ID is required';
    }
    if (empty($_POST['mobile'])) {
        $errors['mobile'] = 'Phone No. is required';
    }
    if ($_POST['country'] == '0') {
        $errors['country'] = 'Country is required';
    }
    if ($_POST['state'] == '0') {
        $errors['state'] = 'State is required';
    }
    if ($_POST['city'] == '0') {
        $errors['city'] = 'City is required';
    }
    if ($_POST['speciality'] == '0') {
        $errors['speciality'] = 'EXPERIENCE is required';
    }

    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $emailid = $_POST['emailid'];
    $mobile = $_POST['mobile'];
    $country = $_POST['country'];
    $teamid = $_POST['teamid'];
    if (isset($_POST['state'])) {
        $state = $_POST['state'];
    }
    if (isset($_POST['city'])) {
        $city = $_POST['city'];
    }
    if (isset($_POST['speciality'])) {
        $speciality = $_POST['speciality'];
    }
    $hospital = $_POST['hospital'];
    $question = $_POST['question'];
    // if (isset($_POST['updates'])) {
    //     $updatesArr = $_POST['updates'];
    //     foreach ($updatesArr as $update) {
    //         $updates .= $update . ',';
    //     }
    //     $updates = substr(trim($updates), 0, -1);
    // }

    if (count($errors) == 0) {
        $newuser = new User();

        $newuser->__set('firstname', $fname);
        $newuser->__set('lastname', $lname);
        $newuser->__set('emailid', $emailid);
        $newuser->__set('mobilenum', $mobile);
        $newuser->__set('state', $state);
        $newuser->__set('city', $city);
        $newuser->__set('country', $country);
        $newuser->__set('speciality', $speciality);
        $newuser->__set('teamid', $teamid);
        // $newuser->__set('updates', $updates);
        $newuser->__set('hospital', $hospital);
        $newuser->__set('question', $question);
        $add = $newuser->addUser();
        //var_dump($add);
        $reg_status = $add['status'];

        if ($reg_status == "success") {
            $succ = $add['message'];
            $fname = '';
            $lname = '';
            $emailid = '';
            $mobile = '';
            $country = 0;
            $state = 0;
            $city = 0;
            $speciality = 0;
            $hospital='';
            $question='';
            // $updates = '';
            // $updatesArr = [];
        } else {
            $errors['reg'] = $add['message'];
        }
    }
}

if (isset($_GET['first_name'])) {
    $fname = $_GET['first_name'];
}
if (isset($_GET['last_name'])) {
    $lname = $_GET['last_name'];
}
if (isset($_GET['email_id'])) {
    $emailid = $_GET['email_id'];
}
if (isset($_GET['phone_no'])) {
    $mobile = $_GET['phone_no'];
}
if (isset($_GET['hospital_name'])) {
    $hospital = $_GET['hospital_name'];
}
if (isset($_GET['question_name'])) {
    $question = $_GET['question_name'];
}
if (isset($_GET['teamid'])) {
    $teamid = $_GET['teamid'];
}
?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CAC</title>
    <link rel="stylesheet" href="assets/css/normalize.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body class="reg">
    <div class="container reg-content vh-100">
        <div class="row mt-4">
            <div class="col-12 col-md-10 col-lg-8 mx-auto p-0">
                <img src="assets/img/reg_top.jpg" class="img-fluid" alt="">
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-10 col-lg-8 mx-auto bg-white">
                <div class="register-wrapper p-3 p-sm-1">
                    <!-- <h5 class="text-center mt-4 mb-3">Register for OSTEOKONNECT</h5> -->
                    <?php
                    if (count($errors) > 0) : ?>
                        <div class="alert alert-danger alert-msg">
                            <ul class="list-unstyled">
                                <?php foreach ($errors as $error) : ?>
                                    <li>
                                        <?php echo $error; ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                    <?php if ($succ != '') { ?>
                        <div class="alert alert-success alert-msg">
                            <?= $succ ?>
                        </div>
                    <?php } ?>
                    <form method="POST" class="mt-4">
                        <div class="row mt-2">
                            <div class="col-12 col-md-6">
                                <label>First Name</label>
                                <input type="text" id="fname" name="fname" class="input" value="<?php echo $fname; ?>" autocomplete="off">
                            </div>
                            <div class="col-12 col-md-6">
                                <label>Last Name</label>
                                <input type="text" id="lname" name="lname" class="input" value="<?php echo $lname; ?>" autocomplete="off">
                            </div>
                        </div>
                        <div class="row mt-3 mb-1">
                            <div class="col-12">
                                <label>Email ID</label>
                                <input type="email" id="emailid" name="emailid" class="input" value="<?php echo $emailid; ?>" autocomplete="off">
                            </div>
                        </div>
                        <div class="row mt-3 mb-1">
                            <div class="col-12 col-md-12">
                                <label>NO. OF YEAR EXPERIENCE</label>
                                <select class="input" id="speciality" name="speciality" value="<?= $speciality ?>">
                                    <option value='0'>Select NO. OF YEAR EXPERIENCE</option>
                                    <option value="less than 10 years">Less Than 10 YEARS</option>
                                    <option value="between 10 to 20 years">Between 10 To 20 YEARS</option>
                                    <option value="more than 20 years">More Than 20 YEARS</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-3 mb-1">
                        <div class="col-12 col-md-6">
                                <label>Country</label>
                                <div id="countries">
                                    <select class="input" id="country" name="country" onChange="updateState()">
                                        <option>Select Country</option>
                                    </select>
                                </div>
                            </div>
                            <!-- <div class="col-12 col-md-6">
                                <label>Phone No.</label>
                                <input type="number" id="mobile" name="mobile" class="input" value="<?php echo $mobile; ?>" autocomplete="off" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
                            </div> -->
                            <div class="col-12 col-md-6">
                                <label>State</label>
                                <div id="states">
                                    <select class="input" id="state" name="state" value="<?= $state ?>" onChange="updateCity()">
                                        <option value="0">Select State</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3 mb-1">
                       
                            <div class="col-12 col-md-6">
                                <label>City</label>
                                <div id="cities">
                                    <select class="input" id="city" name="city">
                                        <option value="0">Select City</option>
                                    </select>
                                </div>
                            </div>
             
                                <input type="hidden" id="mobile" name="teamid" class="input" value="<?php echo $teamid; ?>" autocomplete="off" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
                          
                            <div class="col-12 col-md-6">
                                <label>Phone No.</label>
                                <input type="number" id="mobile" name="mobile" class="input" value="<?php echo $mobile; ?>" autocomplete="off" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
                            </div>
                        </div>
                        <!-- <div class="row mt-3 mb-1">
                            <div class="col-12 col-md-6">
                                <label>Speciality</label>
                                <select class="input" id="speciality" name="speciality" value="<?= $speciality ?>">
                                    <option value='0'>Select Speciality</option>
                                    <option value="ORTHOPEDICS">ORTHOPEDICS</option>
                                    <option value="CONSULTING PHYSICIANS">CONSULTING PHYSICIANS</option>
                                    <option value="OBS & GYN">OBS & GYN</option>
                                </select>
                            </div>
                        </div> -->
                        <div class="row  ">

                            <!-- <div class="col-12">
                                <input <?= (in_array('program', $updatesArr)) ? 'checked' : '' ?> type="checkbox" name="updates[]" value="Program"> I would like to receive updates related to this program <br>
                                <input <?= (in_array('integrace', $updatesArr)) ? 'checked' : '' ?> type="checkbox" name="updates[]" value="Integrace"> I would like to receive updates related to Integrace Pvt. Ltd. <br>
                            </div> -->
                            <div class="col-12 col-md-12">
                                <label>HOSPITAL/INSITUTE/ORGANIZATION*</label>
                                <input type="text" id="hospital" name="hospital" class="input" value="<?php echo $hospital; ?>" autocomplete="off" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
                            </div>
                            <div class="col-12 col-md-12">
                                <label>QUESTION & COMMENTS (OPTIONAL)</label>
                                <input type="text" id="question" name="question" class="input" value="<?php echo $question; ?>" autocomplete="off" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
                            </div>
                        </div>

                        <div class="row mt-4 mb-3">
                            <div class="col-12">
                          
                                <input type="image" src="assets/img/reg-btn.jpg" value="Submit" />
                                <a href="./" class="form-cancel"><img src="assets/img/cancel-btn.jpg" alt="Cancel" /></a>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row pb-4">
            <div class="col-12 col-md-10 col-lg-8 mx-auto text-right bg-white">
                <img src="assets/img/reg_logo.png" class="img-fluid" alt="" />
            </div>
        </div>
    </div>

    <div id="code">IPL/O/WI/05082021</div>

    <script src="//code.jquery.com/jquery-latest.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-eMNCOe7tC1doHpGoWe/6oMVemdAVTMs2xqW4mwXrXsW0L84Iytr2wi5v2QjrP/xp" crossorigin="anonymous"></script>
    <script>
        $(function() {
            getCountries();
        });

        function getCountries() {
            $.ajax({
                url: 'control/event.php',
                data: {
                    action: 'getcountries',
                },
                type: 'post',
                success: function(response) {
                    $("#countries").html(response);
                }
            });
        }

        function updateState() {
            var c = $('#country').val();
            if (c != '0') {
                $.ajax({
                    url: 'control/event.php',
                    data: {
                        action: 'getstates',
                        country: c
                    },
                    type: 'post',
                    success: function(response) {
                        $("#states").html(response);
                    }
                });
            }
        }

        function updateCity() {
            var s = $('#state').val();
            if (s != '0') {
                $.ajax({
                    url: 'control/event.php',
                    data: {
                        action: 'getcities',
                        state: s
                    },
                    type: 'post',
                    success: function(response) {
                        $("#cities").html(response);
                    }
                });
            }
        }
    </script>

    <?php require_once 'ga.php';  ?>
    <?php require_once 'footer.php';  ?>