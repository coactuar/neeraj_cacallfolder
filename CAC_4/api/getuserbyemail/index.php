<?php
header("Content-type: application/json");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

require("../config.php");
require("../error.php");
require("../../functions.php");


if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] != 'GET') {
	$error = $ARR_ERROR["5001"];					// JSON Format issues
	$error["desc"] = "HTTP GET Requests only";
	$error = json_encode($error);
	print $error;
	exit;
}


/* Process Query String */
if (!isset($_GET['teamid'])) {
	$error = $ARR_ERROR["4001"];					// JSON Format issues
	$error["desc"] = "Parameter(s) missing";
	$error = json_encode($error);
	print $error;
	exit;
}

$teamid	= $_GET['teamid'];

if ($teamid) {
	$ret = GetUserByTeamId($teamid);
	if ($ret) {
		print $ret;
		exit;
	}
} else {
	$error = $ARR_ERROR["4004"];					// Required JSON Key missing
	$error["desc"] = "Failed to get user";
	$error = json_encode($error);
	print $error;
	exit;
}


function  GetUserByTeamId($teamid)
{
	$user = new User();
	$user->__set('teamid', $teamid);
	$validUser = $user->getUserByTeamId();
	//var_dump($validUser);
	if ($validUser) {
		$response = array(
			'status' => true
		);
	} else {
		$response = array(
			'status' => false
		);
	}

	return json_encode($response);
}
