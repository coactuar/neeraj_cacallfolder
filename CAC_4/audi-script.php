<script>
  $(function() {

    <?php
    if ($sess_id != '0') { ?>
      updateSession('<?= $userid; ?>', '<?= $sess_id; ?>');
    <?php } else { ?>
      checkforlive('<?= $audi_id; ?>');
    <?php } ?>



    $(document).on('click', '#askques', function() {
      $('.poll').removeClass('show');
      $('.ques').toggleClass('show');
    });

    $(document).on('click', '#close_ques', function() {
      $('.ques').toggleClass('show');
    });

    $(document).on('click', '.send_sesques', function() {
      var sess_id = $(this).data('ses');
      var user_id = $(this).data('user');
      var ques = $('#userques').val();

      if (ques != '') {

        $.ajax({
          url: 'control/ques.php',
          data: {
            action: 'submitques',
            sessId: sess_id,
            userId: user_id,
            ques: ques
          },
          type: 'post',
          success: function(message) {
            console.log(message);
            var response = JSON.parse(message);
            var status = response['status'];
            var msg = response['message'];
            if (status == 'success') {
              $('#userques').val('');
              Swal.fire({
                position: 'top-end',
                icon: 'success',
                text: msg,
                showConfirmButton: false,
                timer: 2000
              })

            } else {
              Swal.fire({
                position: 'top-end',
                icon: 'error',
                text: msg,
                showConfirmButton: false,
                timer: 2000
              })
            }

          }
        });
      } else {
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          text: 'Please enter your question',
          showConfirmButton: false,
          timer: 2000
        })

      }

    });


  });

  function checkforlive(audi_id) {
    $.ajax({
      url: 'control/sess.php',
      data: {
        action: 'checkforlive',
        audiId: audi_id
      },
      type: 'post',
      success: function(response) {
        if (response != '0') {
          location.href = '?ses=' + response;
        }
      }
    }).always(function(data) {
      setTimeout(function() {
        checkforlive(audi_id);
      }, 20000);
    });

  }


  function updateSession(user_id, sess_id) {
    $.ajax({
        url: 'control/update.php',
        data: {
          action: 'updatesession',
          sessId: sess_id,
          userId: user_id
        },
        type: 'post',
        success: function(output) {
          console.log(output);
          if (output == '0') {
            location.href = 'login.php';
          }
        }
      })
      .always(function(data) {
        updSes = setTimeout(function() {
          updateSession(user_id, sess_id);
        }, 30000);
      });

  }
</script>

<script src="https://player.vimeo.com/api/player.js"></script>
<script>
  var iframe = document.querySelector('iframe');
  var player = new Vimeo.Player(iframe);
  $('#goFS').on('click', function() {
    player.requestFullscreen().then(function() {
      // the player entered fullscreen
    }).catch(function(error) {
      // an error occurred
      console.log(error);
    });

  });
</script>