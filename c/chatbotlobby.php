<?php
  function currentsession( $string){ 
    //   print_r($string);
    $form =   '<script>                
            var jquery_type =  "'.$string.'";
            jsfunction(jquery_type);
        </script>';
echo $form;

    //function parameters, two variables.
    // echo "<script>var data = $string;<script>";
    //  echo '<script type="text/javascript">',
     
    // 'jsfunction(data);',
    //  '</script>';
//returns the second argument passed into the function
  }
?>


<!DOCTYPE html>
<html>

<head>
   

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">


    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <style>
        body {
            /* margin: 0 auto; */
            /* max-width:100%; */
            /* padding: 0 20px; */
            width: 100%;
        }
        
        .container {
            border: 2px solid #dedede;
            background-color: #dedede;
             border-radius: 10px; 
            width: 100%;
         
        
            /* margin-left: 6%; */
            /* margin-top: 8%; */
           color:white;
   
   text-align:center;  
 box-shadow: 10px 5px 5px rgb(138, 133, 116) ;  
        }
        
        .darker {
            border-color:transparent;
           background-color: transparent;
       color: #00aaff;  
       border-radius: 10%;
      }
        
        .container::after {
            content: "";
            clear: both;
            display: table;
        }
        
        .container img {
            float: right;
            /* max-width: 60px; */
            
           /* margin-right: 20px; */
            border-radius: 50%;
        }
        
        .container img.right {
            float: left;
            margin-left: 20px;
            margin-right: 0;
        }
        
        .time-right {
            float: right;
            color: #aaa;
        }
        
        .time-left {
            float: left;
            color: #999;
        }
    </style>
</head>

<body>



    <!-- <main class="container"> -->


    <!-- <div id='chatborder'>
            <p id="chatlog7" class="chatlog">&nbsp;</p>
            <p id="chatlog6" class="chatlog">&nbsp;</p>
            <p id="chatlog5" class="chatlog">&nbsp;</p>
            <p id="chatlog4" class="chatlog">&nbsp;</p>
            <p id="chatlog3" class="chatlog">&nbsp;</p>
            <p id="chatlog2" class="chatlog">&nbsp;</p>
            <p id="chatlog1" class="chatlog">&nbsp;</p>
        </div> -->
    <div class="container darker" id='mybox'>
        <div id='chatborder'>
            <img src="assets/img/chatbotlogo.png" alt="Avatar" class="right" style="width:30%; ">
            <p id="chatlog1" class="chatlog">&nbsp;</p>
        </div>
        <!-- <span class="time-left">11:01</span> -->
    </div>
    <!-- <div id="notification" style="display: none;">
                <span class="dismiss"><a title="dismiss this notification">x</a></span>
            </div> -->
    <input type="hidden" name="chat" id="chatbox" placeholder="Hi there! Type here to talk to me." onfocus="placeHolder()">
    <!-- </div> -->


    <!-- </main> -->

    <script src="../../dist/jBox.all.js"></script>
    <script src="./js/demo.js"></script>
    <script src="./js/playground-avatars.js"></script>
    <script src="./js/playground-inception.js"></script>
    <script src="./js/playground-login.js"></script>
    <script>
        //links
        //http://eloquentjavascript.net/09_regexp.html
        //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions


        var messages = [], //array that hold the record of each string in chat
            lastUserMessage = "", //keeps track of the most recent input string from the user
            botMessage = "", //var keeps track of what the chatbot is going to say
            botName = 'Coact Bot', //name of the chatbot
            talking = true; //when false the speach function doesn't work
		//var session='<?php echo 'Hi'; ?>';
        
        function newEntry() {
            //if the message from the user isn't empty then run 
            if (document.getElementById("chatbox").value != "") {
                //pulls the value from the chatbox ands sets it to lastUserMessage
                lastUserMessage = document.getElementById("chatbox").value;
                //sets the chat box to be clear
                document.getElementById("chatbox").value = "";
                //adds the value of the chatbox to the array messages
                messages.push(lastUserMessage);
                //Speech(lastUserMessage);  //says what the user typed outloud
                //sets the variable botMessage in response to lastUserMessage
                chatbotResponse();
                //add the chatbot's name and message to the array messages
                messages.push("<b>" + botName + ":</b> " + botMessage);
                // says the message using the text to speech function written below
                Speech(botMessage);
                //outputs the last few array elements of messages to html
                for (var i = 1; i < 8; i++) {
                    if (messages[messages.length - i])
                        document.getElementById("chatlog" + i).innerHTML = messages[messages.length - i];
                }
            }
        }

        //text to Speech
        //https://developers.google.com/web/updates/2014/01/Web-apps-that-talk-Introduction-to-the-Speech-Synthesis-API
        function Speech(say) {
            if ('speechSynthesis' in window && talking) {
                // const synthesis = window.speechSynthesis;
                // var utterance = new SpeechSynthesisUtterance(say);
                var synth = window.speechSynthesis;
            var voices = synth.getVoices();
           
            var u = new SpeechSynthesisUtterance(say);
            u.pitch=1.0;
            u.rate=0.8;
            u.onend = function () { console.log("on end!"); }
            u.onerror = function () { ("on error!"); }
            u.onpause = function () { console.log("on pause"); }
            u.onresume = function () { console.log("on resume"); }
            u.onstart = function () { console.log("on start"); }
            synth.cancel();
            synth.speak(u);
            var r = setInterval(function () {
                console.log(synth.speaking);
                if (!synth.speaking) clearInterval(r);
                else synth.resume();
            }, 14000);
            

                // alert(utterance);
                //msg.voice = voices[10]; // Note: some voices don't support altering params
                //msg.voiceURI = 'native';
                //utterance.volume = 1; // 0 to 1
                //utterance.rate = 0.1; // 0.1 to 10
                //utterance.pitch = 1; //0 to 2
                //utterance.text = 'Hello World';
                //utterance.lang = 'en-US';
            //    window.speechSynthesis.speak(utterance);
            //    synthesis.resume();
                // recognition.addEventListener('end', recognition.start);
            }
        }

        //runs the keypress() function when a key is pressed
        document.onkeypress = keyPress;
        //if the key pressed is 'enter' runs the function newEntry()
        function keyPress(e) {
            var x = e || window.event;
            var key = (x.keyCode || x.which);
            if (key == 13 || key == 3) {
                //runs this function when enter is pressed
                newEntry();
            }
            if (key == 38) {
                console.log('hi')
                    //document.getElementById("chatbox").value = lastUserMessage;
            }
        }

        function fetchdata() {
            $("#mybox").fadeIn("slow");
            var data='<?php echo $_SESSION['userid']; ?>'
                //   alert(data);
            $.ajax({
                url: 'https://chatbot.coactvirtualevents.live/chat',
                type: 'get',
                data: {
                    UserID: data
                },
                success: function(data) {

                    //pulls the value from the chatbox ands sets it to lastUserMessage
                    lastUserMessage = document.getElementById("chatbox").value = data.data;
                    //sets the chat box to be clear
                    document.getElementById("chatbox").value = "";
                    //adds the value of the chatbox to the array messages
                    // messages.push(lastUserMessage);
                    messages.push("<b>" + botName + ":</b> " + lastUserMessage);
                    //Speech(lastUserMessage);  //says what the user typed outloud
                    //sets the variable botMessage in response to lastUserMessage
                    // chatbotResponse();
                    //add the chatbot's name and message to the array messages
                    // messages.push("<b>" + botName + ":</b> " + botMessage);
                    // says the message using the text to speech function written below
		
		    var path = window.location.pathname;
                var page = path.split("/").pop();
               
                if(page=='auditorium1.php' ||page=='auditorium2.php' ||page=='auditorium3.php' ){
                    //alert(page);
                    talking = false; 
                }                   
		 Speech(lastUserMessage);
                     
                    //outputs the last few array elements of messages to html
                    for (var i = 1; i < 8; i++) {
                        if (messages[messages.length - i])
                            document.getElementById("chatlog" + i).innerHTML = messages[messages.length - i];

                        $("#mybox").fadeOut(30000);
                    }


                    // alert(data.data +
                    //     ' ' + data.title)

                }

            });




        }

        $(document).ready(function() {

            setInterval(fetchdata, 120000);
        });


        //clears the placeholder text ion the chatbox
        //this function is set to run when the users brings focus to the chatbox, by clicking on it
        function placeHolder() {
            document.getElementById("chatbox").placeholder = "";
        }
        $("#notification").fadeIn("slow").append('your message');
        $(".dismiss").click(function() {
            $("#notification").fadeOut("slow");
        });
        function jsfunction(data){
            lastUserMessage = document.getElementById("chatbox").value = data;
                    //sets the chat box to be clear
                    // alert(lastUserMessage);
                    document.getElementById("chatbox").value = "";
                    //adds the value of the chatbox to the array messages
                    // messages.push(lastUserMessage);
                    messages.push("<b>" + botName + ":</b> " + lastUserMessage);
                    //Speech(lastUserMessage);  //says what the user typed outloud
                    //sets the variable botMessage in response to lastUserMessage
                    // chatbotResponse();
                    //add the chatbot's name and message to the array messages
                    // messages.push("<b>" + botName + ":</b> " + botMessage);
                    // says the message using the text to speech function written below
			if(data=='Auditorium1' ||data=='Auditorium2'||data=='Auditorium3' ){
               		//alert('hi');
                    talking = false;

                       }
                    Speech(lastUserMessage);
                    for (var i = 1; i < 8; i++) {
                        if (messages[messages.length - i])
                            document.getElementById("chatlog" + i).innerHTML = messages[messages.length - i];

                        $("#mybox").fadeOut(30000);
                    }

            // alert(data);
        }
    </script>
    <style>
        #notification {
            position: fixed;
            top: 0px;
            width: 100%;
            z-index: 105;
            text-align: center;
            font-weight: normal;
            font-size: 14px;
            font-weight: bold;
            color: white;
            background-color: #FF7800;
            padding: 5px;
        }
        
        #notification span.dismiss {
            border: 2px solid #FFF;
            padding: 0 5px;
            cursor: pointer;
            float: right;
            margin-right: 10px;
        }
        
        #notification a {
            color: white;
            text-decoration: none;
            font-weight: bold
        }
    </style>
</body>

</html>