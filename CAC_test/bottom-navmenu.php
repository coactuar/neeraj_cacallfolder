<nav class="navbar bottom-nav">
  <ul class="nav mx-auto">
    <li class="nav-item">
      <a class="nav-link" href="lobby.php" title="Go To Lobby"><i class="fa fa-home"></i><span class="hide-menu">Lobby</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="auditorium.php" title="Go To Auditorium"><i class="fa fa-chalkboard-teacher"></i><span class="hide-menu">Auditorium</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="exhibitionhalls.php" title="Go To Exhibition Halls"><i class="fa fa-box-open"></i><span class="hide-menu">Exhibition Halls</span></a>
    </li>
    <li> <a class="" href="lounge.php" title="Networking Lounge">
        <i class="fas fa-network-wired position-relative">
          <div id="chat-message"></div>
        </i>
        <span class="hide-menu">Networking Lounge</span></a>
    </li>
    <li> <a class="" id="show_talktous" href="#" title="Talk to Us" data-from="<?php echo $_SESSION['userid']; ?>"><i class="fas fa-comment-alt"></i><span class="hide-menu"></span>Talk To Us</a></li>
    <li class="nav-item">
      <a class="nav-link logout" href="logout.php" title="Logout"><i class="fas fa-sign-out-alt"></i>Logout</a>
    </li>
  </ul>

</nav>
<div id="helplines" style="position: absolute;
    bottom: 8px;
    right: 0px;
    text-align: center;
    color: #333;
    width: 11.5%;">
  For assistance:<br>
  <i class="fas fa-phone-square-alt"></i> +917314-855-655

</div>