<?php
require_once '../functions.php';
require_once 'logincheck.php';

$errors = [];
$succ = '';

$exhib_id = '0';
$res_title = '';
$res_url = '';

if ($_GET['ac'] == 'edit') {
  $resId = $_GET['id'];
  $exhib = new Exhibitor();
  $exhib->__set('res_id', $resId);
  $editres = $exhib->getResource();
  //var_dump($editres);
  if (!empty($editres)) {
    $exhib_id = $editres[0]['exhib_id'];;
    $res_title = $editres[0]['resource_title'];;
    $res_url = $editres[0]['resource_url'];;
  } else {
    header('location: exhibitors.php');
  }


  if (isset($_POST['btn-editres'])) {
    if ($_POST['editexhibid'] == '0') {
      $errors['exhib'] = 'Select Exhibitor';
    }

    if (empty($_POST['editresTitle'])) {
      $errors['title'] = 'Resource title is required';
    }
    if (empty($_POST['editresUrl'])) {
      $errors['url'] = 'Resource URL is required';
    }

    $exhib_id =   $_POST['editexhibid'];
    $res_title =   $_POST['editresTitle'];
    $res_url =   $_POST['editresUrl'];


    if (count($errors) == 0) {
      $exhib = new Exhibitor();
      $exhib->__set('res_id', $resId);
      $exhib->__set('exhib_id', $exhib_id);
      $exhib->__set('res_title', $res_title);
      $exhib->__set('res_url', $res_url);


      $upd = $exhib->updResource();
      //var_dump($upd);
      $reg_status = $upd['status'];

      if ($reg_status == "success") {
        $succ = $upd['message'];
        //$exhibName = '';    
      } else {
        $errors['reg'] = $upd['message'];
      }
    }
  }
}


if (isset($_POST['btn-addres'])) {

  if ($_POST['exhibid'] == '0') {
    $errors['exhib'] = 'Select Exhibitor';
  }

  if (empty($_POST['resTitle'])) {
    $errors['title'] = 'Resource title is required';
  }
  if (empty($_POST['resUrl'])) {
    $errors['url'] = 'Resource URL is required';
  }

  if (count($errors) == 0) {
    $exhib_id =   $_POST['exhibid'];
    $res_title =   $_POST['resTitle'];
    $res_url =   $_POST['resUrl'];

    $exhib = new Exhibitor();
    $exhib->__set('exhib_id', $exhib_id);
    $exhib->__set('res_title', $res_title);
    $exhib->__set('res_url', $res_url);

    $add = $exhib->addExhibRes();
    //var_dump($add);
    $reg_status = $add['status'];

    if ($reg_status == "success") {
      $succ = $add['message'];
      $exhib_id = '0';
      $res_title = '';
      $res_url = '';
    } else {
      $errors['reg'] = $add['message'];
    }
  }
}
?>
<?php
require_once 'header.php';
require_once 'nav.php';
?>

<div class="container-fluid">
  <div class="row p-2">
    <div class="col-12 col-md-6 offset-md-3">
      <?php
      if (count($errors) > 0) : ?>
        <div class="alert alert-danger alert-msg">
          <ul class="list-unstyled">
            <?php foreach ($errors as $error) : ?>
              <li>
                <?php echo $error; ?>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      <?php endif; ?>
      <?php if ($succ != '') { ?>
        <div class="alert alert-success alert-msg">
          <?= $succ ?>
        </div>
      <?php } ?>

      <?php if ($_GET['ac'] == 'add') { ?>
        <form method="post" action="" class="form">
          <div class="form-group">
            <?php
            $exhib = new Exhibitor();
            $exhibList = $exhib->getExhibitors();
            if (empty($exhibList)) {
              header('location: exhib.php?ac=add');
            }
            ?>
            <select name="exhibid" id="exhibid" class="form-control">
              <option value="0" <?= ($exhib_id == '0') ? 'selected' : '' ?>>Select Exhibitor</option>
              <?php
              foreach ($exhibList as $a) {
              ?>
                <option value="<?= $a['exhib_id'] ?>" <?= ($exhib_id == $a['exhib_id']) ? 'selected' : '' ?>><?= $a['exhib_name'] ?></option>
              <?php
              }
              ?>
            </select>
          </div>
          <div class="form-group">
            <input type="text" name="resTitle" id="restitle" class="form-control" value="<?= $res_title ?>" placeholder="Enter Resource Title">
          </div>
          <div class="form-group">
            <input type="text" name="resUrl" id="resUrl" class="form-control" value="<?= $res_url ?>" placeholder="Enter Resource URL">
          </div>
          <div class="form-group">
            <button class="btn btn-primary" type="submit" name="btn-addres">Add Resource</button>
          </div>
        </form>
      <?php }
      if ($_GET['ac'] == 'edit') { ?>
        <form method="post" action="" class="form">
          <div class="form-group">
            <?php
            $exhib = new Exhibitor();
            $exhibList = $exhib->getExhibitors();
            if (empty($exhibList)) {
              header('location: exhib.php?ac=add');
            }
            ?>
            <select name="editexhibid" id="editexhibid" class="form-control">
              <option value="0" <?= ($exhib_id == '0') ? 'selected' : '' ?>>Select Exhibitor</option>
              <?php
              foreach ($exhibList as $a) {
              ?>
                <option value="<?= $a['exhib_id'] ?>" <?= ($exhib_id == $a['exhib_id']) ? 'selected' : '' ?>><?= $a['exhib_name'] ?></option>
              <?php
              }
              ?>
            </select>
          </div>
          <div class="form-group">
            <input type="text" name="editresTitle" id="editrestitle" class="form-control" value="<?= $res_title ?>" placeholder="Enter Resource Title">
          </div>
          <div class="form-group">
            <input type="text" name="editresUrl" id="editresUrl" class="form-control" value="<?= $res_url ?>" placeholder="Enter Resource URL">
          </div>
          <div class="form-group">
            <button class="btn btn-primary" type="submit" name="btn-editres">Update Resource</button>
          </div>
        </form>
      <?php } ?>
    </div>

  </div>

</div>

<?php
require_once 'scripts.php';
?>
<?php
require_once 'footer.php';
?>