<?php
require_once "logincheck.php";
$curr_room = 'posters';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/Eposterwall.png">
            <div id="back-button">
                <a href="lobby.php"><i class="fas fa-arrow-alt-circle-left"></i> Back</a>
            </div>
            <!-- <a href="https://player.vimeo.com/video/589446228" id="exhVideo" class="viewvideo" data-vidid="1234"> -->
            <a href="assets/posters/Dr A C Johary - TKA in Valgus Knee.jpg" id="exhVideo_1" class="viewvideo1 view" data-vidid="113451">
                <div class="indicator d-6"></div>
            </a> 
            <a href="assets/posters/Dr A S P V S Saketh Infected THA .pdf" id="exhVideo1_1" class="viewvideo1 showpdf" data-vidid="113461">
                <div class="indicator d-6"></div>
            </a> 
              
            <a href="assets/posters/Dr.A.SUBBURAM A Case presentation on REVISION TOTAL KNEE ARTHROPLASTY.pdf" id="exhVideo21" class="viewvideo1 showpdf" data-vidid="113471">
                <div class="indicator d-6"></div>
            </a> 
            <a href="assets/posters/Dr Aditya Narula MCL INSUFFICIENCY DURING PRIMARY TKR.pdf" id="exhVideo31" class="viewvideo1 showpdf" data-vidid="113481">
                <div class="indicator d-6"></div>
            </a> 
            <a href="assets/posters/Dr Archit Agarwal Total Hip Arthroplasty in Tuberculosis of Hip.pdf" id="exhVideo41" class="viewvideo1 showpdf" data-vidid="113491">
                <div class="indicator d-6"></div>
            </a> 
            <a href="assets/posters/Dr Daksh Gadi TKR in post HTO valgus knee.pdf" id="exhVideo51" class="viewvideo1 showpdf" data-vidid="1134912">
                <div class="indicator d-6"></div>
            </a> 
            <a href="assets/posters/Dr Guganesh P - Revision THA.png" id="exhVideo61" class="viewvideo1 view" data-vidid="1134922">
                <div class="indicator d-6"></div>
            </a> 
            <a href="assets/posters/Dr Neerav jain Revision THA.pdf" id="exhVideo7" class="viewvideo1 showpdf" data-vidid="1134923">
                <div class="indicator d-6"></div>
            </a> 
            <a href="assets/posters/Dr Imran Akhtar UnCEMENTED THR IN NECK FRACTURE .png" id="exhVideo8" class="viewvideo1 view" data-vidid="11349234">
                <div class="indicator d-6"></div>
            </a> 
            <a href="assets/posters/Dr Intekhab OVERCOMING THE CHALLENGES IN COMPLEX PRIMARY TKR.pdf" id="exhVideo9" class="viewvideo1 showpdf" data-vidid="1134925">
                <div class="indicator d-6"></div>
            </a> 
            <a href="assets/posters/DR Harsh Sanjay Shah THA with Distal Loading Stem.pdf" id="exhVideo10" class="viewvideo1 showpdf" data-vidid="1134926">
                <div class="indicator d-6"></div>
            </a> 
            <a href="assets/posters/DR Vishnu S Knee mEtallosis.pdf" id="exhVideo11" class="viewvideo1 showpdf" data-vidid="1134927">
                <div class="indicator d-6"></div>
            </a> 
            <a href="assets/posters/Dr Sunil Kumar ePoster .pdf" id="exhVideo12" class="viewvideo1 showpdf" data-vidid="11349258">
                <div class="indicator d-6"></div>
            </a> 
            
            <!-- <a href="
            ">
            <div class="indicator d-6"></div>
            </a>
            <a href="bondk.php" id="bondk">
                <div class="indicator d-6"></div>
            </a>
            <a href="esoz.php" id="esoz">
                <div class="indicator d-6"></div>
            </a>
            <a href="bonnxt.php" id="bonnxt">
                <div class="indicator d-6"></div>
            </a>
            <a href="bonk2.php" id="bonk2">
                <div class="indicator d-6"></div>
            </a>
            <a href="dubinor.php" id="dubinor">
                <div class="indicator d-6"></div>
            </a>
            <a href="milicalod3.php" id="milicalod3">
                <div class="indicator d-6"></div>
            </a>
            <a href="dubinor-ointments.php" id="dubinor-ointments">
                <div class="indicator d-6"></div>
            </a>
            <a href="bmdcamps.php" id="bmdcamp">
                <div class="indicator d-6"></div>
            </a>
            <a href="ebovpg.php" id="ebovpg">
                <div class="indicator d-6"></div>
            </a>
            <a href="vkonnecthealth.php" id="vkonnecthealth">
                <div class="indicator d-6"></div>
            </a> -->

        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<script>
    $(function() {
        $('.viewvideo1').on('click', function() {
            var vid_id = $(this).data('vidid');
            var userid="<?php echo $_SESSION['userid']; ?>"
    // alert(vid_id);
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updatePostView',
                    vidId: vid_id,
                    userId:userid 
                },
                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
</script>

<?php require_once "scripts.php" ?>

<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>