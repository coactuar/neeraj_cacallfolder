<?php
require_once '../functions.php';
require_once 'logincheck.php';

$audi1_id = '0fd013a85fbd8008cc4f0a6a3aa2ce4012c125fccca6112c42b93ef8777e1718';

$sess = new Session();
?>
<?php
require_once 'header.php';
require_once 'nav.php';
?>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>
<div class="container-fluid">
    <div id="superdashboard">
        <div class="row">
            <div class="col-12 col-md-4">
                <div id="audi01" class="audi">
                    <?php
                    $sess->__set('audi_id', $audi1_id);
                    $audiUrl1 = $sess->getCurrLiveSession();
                    $sess1Id = $audiUrl1[0]['session_id'];
                    $sess->__set('session_id', $sess1Id);
                    $sess1Url = $sess->getWebcastSessionURL();
                    if (!empty($sess1Url)) {
                        //$sess1Url .= '?muted=1';
                    }
                    ?>
                    <div class="title">Auditorium</div>
                    <div class="video">
                        <iframe src="../video1.php" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen title=""></iframe>
                    </div>
                    <div class="title">Viewers: <div id="audi1viewer" style="display: inline-block;">0</div>
                    </div>


                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="title">Questions</div>
                <div id="audi1ques" class="details scroll vh-100"></div>
            </div>
            <div class="col-12 col-md-4">
                <div class="title">Viewers</div>
                <div id="audi1views" class="details scroll vh-100"></div>
            </div>

        </div>
    </div>
</div>
<?php
require_once 'scripts.php';
?>
<script>
    function audiViews(sess, ele) {
        $.ajax({
            url: '../control/sess.php',
            data: {
                action: 'getLiveSessionViewerCount',
                sessId: sess
            },
            type: 'post',
            success: function(output) {
                $(ele).text(output);
            }
        });

    }

    function sessViewers(sess, ele) {
        $.ajax({
            url: '../control/sess.php',
            data: {
                action: 'getLiveSessionViewers',
                sessId: sess
            },
            type: 'post',
            success: function(output) {
                $(ele).html(output);

            }
        });

    }

    function audiQues(sess, ele) {
        $.ajax({
            url: '../control/sess.php',
            data: {
                action: 'getSessionQuestions',
                sessId: sess
            },
            type: 'post',
            success: function(output) {
                //console.log(output);
                $(ele).html(output);

            }
        });

    }

    function showQA1() {
        $('#audi01 .tabs a').removeClass('active');
        $('#qa-audi1').addClass('active');
        $('#questions-audi1').css('display', 'block');
        $('#view-audi1').css('display', 'none');
    }

    function showViewers1() {
        $('#audi01 .tabs a').removeClass('active');
        $('#viewers-audi1').addClass('active');
        $('#questions-audi1').css('display', 'none');
        $('#view-audi1').css('display', 'block');
    }

    /* function showQA2() {
        $('#audi02 .tabs a').removeClass('active');
        $('#qa-audi2').addClass('active');
        $('#questions-audi2').css('display', 'block');
        $('#view-audi2').css('display', 'none');
    }

    function showViewers2() {
        $('#audi02 .tabs a').removeClass('active');
        $('#viewers-audi2').addClass('active');
        $('#questions-audi2').css('display', 'none');
        $('#view-audi2').css('display', 'block');
    }

    function showQA3() {
        $('#audi03 .tabs a').removeClass('active');
        $('#qa-audi3').addClass('active');
        $('#questions-audi3').css('display', 'block');
        $('#view-audi3').css('display', 'none');
    }

    function showViewers3() {
        $('#audi03 .tabs a').removeClass('active');
        $('#viewers-audi3').addClass('active');
        $('#questions-audi3').css('display', 'none');
        $('#view-audi3').css('display', 'block');
    } */
</script>
<?php
if (!empty($audiUrl1)) {
?>
    <script>
        audiViews('<?= $sess1Id; ?>', '#audi1viewer');
        audiQues('<?= $sess1Id; ?>', '#audi1ques');
        sessViewers('<?= $sess1Id; ?>', '#audi1views');

        setInterval(function() {
            audiViews('<?= $sess1Id; ?>', '#audi1viewer');
            audiQues('<?= $sess1Id; ?>', '#audi1ques');
            sessViewers('<?= $sess1Id; ?>', '#audi1views');
        }, 30000);
    </script>
<?php } ?>

<?php
require_once 'footer.php';
?>