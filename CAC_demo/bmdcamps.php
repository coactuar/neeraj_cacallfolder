<?php
require_once "logincheck.php";
require_once "functions.php";

$exhib_id = '2f23b50346c0be5a33633ec864f7d032381393e427c9e9e3a4928b2db4ea2325';
require_once "exhibcheck.php";
$curr_room = 'bmdcamp';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg" class="bmdcamp">
            <img src="assets/img/stalls/bmdcamp.jpg">
            <div id="back-button">
                <a href="exhibitionhalls.php"><i class="fas fa-arrow-alt-circle-left"></i> Back</a>
            </div>
            <a href="assets/resources/bmdcamp_1.jpg" id="poster1" class="view">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/bmdcamp_2.jpg" id="poster2" class="view">
                <div class="indicator d-6"></div>
            </a>
            <a href="#" data-exhid="<?php echo $exhib_id; ?>" data-userid="<?php echo $_SESSION['userid']; ?>" id="subCampReq">
                <div class="indicator d-6"></div>
            </a>
            <a href="https://s3.ap-northeast-1.amazonaws.com/output.stream/ffd-vod/docs/Osteo/IJORO+-+BON+K2+lbl+8+july+Reference+file+c2c+(1).pdf" class="viewpoppdf resdl" id="dlBrochure" data-docid="c95a556d1e2a3180410b64b47db07fc2228525d155fc98be2cabfc79540bd8a9">
                <div class="indicator d-6"></div>
            </a>

        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>

<?php require_once "scripts.php" ?>

<?php require_once "exhib-script.php" ?>

<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>