<?php
require_once "logincheck.php";
$curr_room = 'exhibitionhall';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/Final.png">
            <div id="back-button">
                <a href="lobby.php"><i class="fas fa-arrow-alt-circle-left"></i> Back</a>
            </div>
            <!-- <a href="https://player.vimeo.com/video/589446228" id="exhVideo" class="viewvideo" data-vidid="1234"> -->
            <a href="https://player.vimeo.com/video/624516133?h=883c0aa38f" id="exhVideo" class="viewvideo vidview" data-vidid="11345">
                <div class="indicator d-6"></div>
            </a> 
            <a href="https://player.vimeo.com/video/624516477?h=24b0fa74d7" id="exhVideo1" class="viewvideo vidview" data-vidid="11346">
                <div class="indicator d-6"></div>
            </a> 
              
            <a href="https://player.vimeo.com/video/624516600?h=8aea1ead1c" id="exhVideo2" class="viewvideo vidview" data-vidid="11347">
                <div class="indicator d-6"></div>
            </a> 
            <a href="https://player.vimeo.com/video/624517503?h=c09c544bc4" id="exhVideo3" class="viewvideo vidview" data-vidid="11348">
                <div class="indicator d-6"></div>
            </a> 
            <a href="https://player.vimeo.com/video/624515954?h=33a5b32453" id="exhVideo4" class="viewvideo vidview" data-vidid="11349">
                <div class="indicator d-6"></div>
            </a> 
            <a href="https://player.vimeo.com/video/624517124?h=37f76ecd8a" id="exhVideo5" class="viewvideo vidview" data-vidid="113491">
                <div class="indicator d-6"></div>
            </a> 
            <a href="https://player.vimeo.com/video/624517658?h=a64afa814b" id="exhVideo6" class="viewvideo vidview" data-vidid="113492">
                <div class="indicator d-6"></div>
            </a> 
            
            <!-- <a href="
            ">
            <div class="indicator d-6"></div>
            </a>
            <a href="bondk.php" id="bondk">
                <div class="indicator d-6"></div>
            </a>
            <a href="esoz.php" id="esoz">
                <div class="indicator d-6"></div>
            </a>
            <a href="bonnxt.php" id="bonnxt">
                <div class="indicator d-6"></div>
            </a>
            <a href="bonk2.php" id="bonk2">
                <div class="indicator d-6"></div>
            </a>
            <a href="dubinor.php" id="dubinor">
                <div class="indicator d-6"></div>
            </a>
            <a href="milicalod3.php" id="milicalod3">
                <div class="indicator d-6"></div>
            </a>
            <a href="dubinor-ointments.php" id="dubinor-ointments">
                <div class="indicator d-6"></div>
            </a>
            <a href="bmdcamps.php" id="bmdcamp">
                <div class="indicator d-6"></div>
            </a>
            <a href="ebovpg.php" id="ebovpg">
                <div class="indicator d-6"></div>
            </a>
            <a href="vkonnecthealth.php" id="vkonnecthealth">
                <div class="indicator d-6"></div>
            </a> -->

        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<script>
    $(function() {
        $('.viewvideo').on('click', function() {
            var vid_id = $(this).data('vidid');
            var userid="<?php echo $_SESSION['userid']; ?>"
    //  alert(vid_id);
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },
                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
</script>

<?php require_once "scripts.php" ?>

<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>