<?php
require_once "logincheck.php";
$curr_room = 'digital_cert';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>

<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg" class="digicert">
            <img src="assets/img/bg-blank.jpg">
            <div id="certText"  >Click on the Certificate to download.</div>
            <div id="cert-area">
                <div class="cert">
                    <a href="#" ><img class="photo-jacket" class="viewvideo1" onclick="dlCert()" data-vidid="5254" src="assets/img/certificate.jpg" /></a>
                    <div class="name-text"><?= $user_name ?></div>
                </div>
                <a href="#" id="dlCert" class="viewvideo1" data-vidid="5254" onclick="dlCert()">Download Certificate</a>
            </div>

        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>

<?php require_once "scripts.php" ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2.0.2/FileSaver.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/1.3.2/html2canvas.min.js"></script>
<script>
    function dlCert() {
        
        html2canvas(document.querySelector("#cert-area"), {
            backgroundColor: "#000000"
        }).then(c => {
            c.toBlob(function(blob) {
                saveAs(blob, "<?= $user_name ?>_certificate.jpg");
            });
        });
    }
</script>
<script>
    $(function() {
        $('.viewvideo1').on('click', function() {
            var vid_id = $(this).data('vidid');
            var userid="<?php echo $_SESSION['userid']; ?>"
        //  alert(vid_id);
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updatePostView',
                    vidId: vid_id,
                    userId:userid 
                },
                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
</script>
<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>