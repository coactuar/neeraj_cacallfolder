<?php
require_once 'functions.php';

$succ = false;
$errors = [];

$q01 = '';
$q02 = '';
$q03 = '';
$q04 = '';
$q05 = '';
$q06 = '';
$q07 = '';
$q08 = '';

$q13 = '';
$q14 = '';
$q15 = '';
$q16 = '';
$q17 = '';
$q18 = '';
$q19 = '';
$q20 = '';
$q21 = '';

$q29 = '';
$q30 = '';
$q31 = '';
$q32 = '';
$q33 = '';

$userid = '';

if (isset($_POST['fbsub-btn'])) {

    if (
        empty($_POST['q01']) ||
        empty($_POST['q02']) ||
        empty($_POST['q03']) ||
        empty($_POST['q04']) ||
        empty($_POST['q05']) ||
        empty($_POST['q06']) ||
        empty($_POST['q07']) ||
        empty($_POST['q08']) ||
        
        empty($_POST['q13']) ||
      
        empty($_POST['q15']) ||
        empty($_POST['q16']) ||
        empty($_POST['q17']) ||
        empty($_POST['q18']) ||
        empty($_POST['q19']) ||
        empty($_POST['q20']) ||
        empty($_POST['q21']) ||
        
        empty($_POST['q29']) ||
        empty($_POST['q30']) ||
        empty($_POST['q31']) ||
        empty($_POST['q32']) ||
        empty($_POST['q33']) 
        

    ) {
        $errors['reply'] = 'Please answer all questions.';
    }
    if (isset($_POST['userid'])) {
        $userid = $_POST['userid'];
    } else {
        header('location: ./');
    }

    if (isset($_POST['q01'])) {
        $q01 = $_POST['q01'];
    }
    if (isset($_POST['q02'])) {
        $q02 = $_POST['q02'];
    }
    if (isset($_POST['q03'])) {
        $q03 = $_POST['q03'];
    }
    if (isset($_POST['q04'])) {
        $q04 = $_POST['q04'];
    }
    if (isset($_POST['q05'])) {
        $q05 = $_POST['q05'];
    }
    if (isset($_POST['q06'])) {
        $q06 = $_POST['q06'];
    }
    if (isset($_POST['q07'])) {
        $q07 = $_POST['q07'];
    }
    if (isset($_POST['q08'])) {
        $q08 = $_POST['q08'];
    }
    if (isset($_POST['q09'])) {
        $q09 = $_POST['q09'];
    }
    if (isset($_POST['q10'])) {
        $q10 = $_POST['q10'];
    }
    if (isset($_POST['q11'])) {
        $q11 = $_POST['q11'];
    }
    if (isset($_POST['q12'])) {
        $q12 = $_POST['q12'];
    }
    if (isset($_POST['q13'])) {
        $q13 = $_POST['q13'];
    }
    if (isset($_POST['q14'])) {
        $q14 = $_POST['q14'];
    }
    if (isset($_POST['q15'])) {
        $q15 = $_POST['q15'];
    }
    if (isset($_POST['q16'])) {
        $q16 = $_POST['q16'];
    }
    if (isset($_POST['q17'])) {
        $q17 = $_POST['q17'];
    }
    if (isset($_POST['q18'])) {
        $q18 = $_POST['q18'];
    }
    if (isset($_POST['q19'])) {
        $q19 = $_POST['q19'];
    }
    if (isset($_POST['q20'])) {
        $q20 = $_POST['q20'];
    }
    if (isset($_POST['q21'])) {
        $q21 = $_POST['q21'];
    }
    if (isset($_POST['q22'])) {
        $q22 = $_POST['q22'];
    }
    if (isset($_POST['q23'])) {
        $q23 = $_POST['q23'];
    }
    if (isset($_POST['q24'])) {
        $q24 = $_POST['q24'];
    }
    if (isset($_POST['q25'])) {
        $q25 = $_POST['q25'];
    }
    if (isset($_POST['q26'])) {
        $q26 = $_POST['q26'];
    }
    if (isset($_POST['q27'])) {
        $q27 = $_POST['q27'];
    }
    if (isset($_POST['q28'])) {
        $q28 = $_POST['q28'];
    }
    if (isset($_POST['q29'])) {
        $q29 = $_POST['q29'];
    }
    if (isset($_POST['q30'])) {
        $q30 = $_POST['q30'];
    }
    if (isset($_POST['q31'])) {
        $q31 = $_POST['q31'];
    }
    if (isset($_POST['q32'])) {
        $q32 = $_POST['q32'];
    }
    if (isset($_POST['q33'])) {
        $q33 = $_POST['q33'];
    }
    if (isset($_POST['q34'])) {
        $q34 = $_POST['q34'];
    }
    if (isset($_POST['q35'])) {
        $q35 = $_POST['q35'];
    }
    if (isset($_POST['q36'])) {
        $q36 = $_POST['q36'];
    }



    if (count($errors) == 0) {
        $fb = new Feedback();
        $fb->__set('user_id', $userid);
        $fb->__set('q01', $_POST['q01']);
        $fb->__set('q02', $_POST['q02']);
        $fb->__set('q03', $_POST['q03']);
        $fb->__set('q04', $_POST['q04']);
        $fb->__set('q05', $_POST['q05']);
        $fb->__set('q06', $_POST['q06']);
        $fb->__set('q07', $_POST['q07']);
        $fb->__set('q08', $_POST['q08']);
       
        $fb->__set('q13', $_POST['q13']);
       
        $fb->__set('q15', $_POST['q15']);
        $fb->__set('q16', $_POST['q16']);
        $fb->__set('q17', $_POST['q17']);
        $fb->__set('q18', $_POST['q18']);
        $fb->__set('q19', $_POST['q19']);
        $fb->__set('q20', $_POST['q20']);
        $fb->__set('q21', $_POST['q21']);
       
       
        $fb->__set('q29', $_POST['q29']);
        $fb->__set('q30', $_POST['q30']);
        $fb->__set('q31', $_POST['q31']);
        $fb->__set('q32', $_POST['q32']);
        $fb->__set('q33', $_POST['q33']);
        

        $subFeedback = $fb->submitFeedback();

        //var_dump($subFeedback);

        if ($subFeedback['status'] == 'success') {
            $succ = true;
        } else {
            $errors['msg'] = $subFeedback['message'];
        }
    }
}

?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $event_title ?></title>
    <link rel="stylesheet" href="assets/css/normalize.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <Style>
        th{
            font-size:90%;
        }
        #bg{
            background-image: none !important;
        }
    </style>
</head>

<body id="bg">

    <div class="container-fluid">
        <div class="row bg-white">
            <div class="col-12 p-0">
                <h1 class="text-center">
                     Feedback Form
                </h1>
            </div>
        </div>
        <div class="row bg-white color-grey py-2">
            <div class="col-12 col-md-8 mx-auto">
                <h5 class="reg-title">
                    Thank you for attending CAC
                </h5>
                <p>Please let us know about your experience regarding the program, faculty, and its relevace for clinical practice.</p>
            </div>
        </div>
        <div class="row bg-white color-grey">
            <div class="col-12 col-md-8 mx-auto">
                <?php if (!$succ) { ?>
                    <div id="register-area">
                        <?php
                        if (count($errors) > 0) : ?>
                            <div class="alert alert-danger">
                                <ul class="list-unstyled">
                                    <?php foreach ($errors as $error) : ?>
                                        <li>
                                            <?php echo $error; ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endif;
                        ?>
                        <form method="POST">
                            <input type="hidden" id="userid" name="userid" class="input" value="<?= $_SESSION['userid'] ?>" required>

                            <div class="row mt-3 mb-1">
                                <div class="col-12">
                                    <table class="table">
                                        <tr>
                                            <td align="left"><strong>1. Indicate the reason you came to the meeting:</strong></td>
                                            <th width="150"></strong>Please check all that applied</strong></th>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">to develop clinical skills</td>
                                            <td><input type="radio" name="q29" <?= ($q29 == 'to develop clinical skills') ? 'checked' : '' ?> value="to develop clinical skills"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">to develop interpretive and diagnostic skills</td>
                                            <td><input type="radio" name="q30" <?= ($q30 == 'to develop interpretive and diagnostic skills') ? 'checked' : '' ?> value="to develop interpretive and diagnostic skills"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">to acquire new information on the subject</td>
                                            <td><input type="radio" name="q31" <?= ($q31 == 'to acquire new information on the subject') ? 'checked' : '' ?> value="to acquire new information on the subject"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">to review the subject</td>
                                            <td><input type="radio" name="q32" <?= ($q32 == 'to review the subject') ? 'checked' : '' ?> value="to review the subject"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">to meet CME requirements</td>
                                            <td><input type="radio" name="q33" <?= ($q33 == 'to meet CME requirements') ? 'checked' : '' ?> value="to meet CME requirements"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-12">
                                    <strong>2. Please rate the overall aspects of this educational activity on the basis of:</strong>
                                </div>
                            </div>
                            <div class="row mt-3 mb-1">
                                <div class="col-12">
                                    <table class="table">
                                        <tr align="center">
                                            <td align="left"><b></b></td>
                                            <th>Poor</th>
                                            <th>Below Average</th>
                                            <th>Average</th>
                                            <th>Above</th>
                                            <th>Outstanding</th>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Educational content</td>
                                            <td><input type="radio" name="q01" <?= ($q01 == 'Poor') ? 'checked' : '' ?> value="Poor"></td>
                                            <td><input type="radio" name="q01" <?= ($q01 == 'Below Average') ? 'checked' : '' ?> value="Below Average"></td>
                                            <td><input type="radio" name="q01" <?= ($q01 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                            <td><input type="radio" name="q01" <?= ($q01 == 'Above') ? 'checked' : '' ?> value="Above"></td>
                                            <td><input type="radio" name="q01" <?= ($q01 == 'Outstanding') ? 'checked' : '' ?> value="Outstanding"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Relevance to practice</td>
                                            <td><input type="radio" name="q02" <?= ($q02 == 'Poor') ? 'checked' : '' ?> value="Poor"></td>
                                            <td><input type="radio" name="q02" <?= ($q02 == 'Below Average') ? 'checked' : '' ?> value="Below Average"></td>
                                            <td><input type="radio" name="q02" <?= ($q02 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                            <td><input type="radio" name="q02" <?= ($q02 == 'Above') ? 'checked' : '' ?> value="Above"></td>
                                            <td><input type="radio" name="q02" <?= ($q02 == 'Outstanding') ? 'checked' : '' ?> value="Outstanding"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Questions and discussions</td>
                                            <td><input type="radio" name="q03" <?= ($q03 == 'Poor') ? 'checked' : '' ?> value="Poor"></td>
                                            <td><input type="radio" name="q03" <?= ($q03 == 'Below Average') ? 'checked' : '' ?> value="Below Average"></td>
                                            <td><input type="radio" name="q03" <?= ($q03 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                            <td><input type="radio" name="q03" <?= ($q03 == 'Above') ? 'checked' : '' ?> value="Above"></td>
                                            <td><input type="radio" name="q03" <?= ($q03 == 'Outstanding') ? 'checked' : '' ?> value="Outstanding"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Oral presentations</td>
                                            <td><input type="radio" name="q04" <?= ($q04 == 'Poor') ? 'checked' : '' ?> value="Poor"></td>
                                            <td><input type="radio" name="q04" <?= ($q04 == 'Below Average') ? 'checked' : '' ?> value="Below Average"></td>
                                            <td><input type="radio" name="q04" <?= ($q04 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                            <td><input type="radio" name="q04" <?= ($q04 == 'Above') ? 'checked' : '' ?> value="Above"></td>
                                            <td><input type="radio" name="q04" <?= ($q04 == 'Outstanding') ? 'checked' : '' ?> value="Outstanding"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Quality of presenters</td>
                                            <td><input type="radio" name="q05" <?= ($q05 == 'Poor') ? 'checked' : '' ?> value="Poor"></td>
                                            <td><input type="radio" name="q05" <?= ($q05 == 'Below Average') ? 'checked' : '' ?> value="Below Average"></td>
                                            <td><input type="radio" name="q05" <?= ($q05 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                            <td><input type="radio" name="q05" <?= ($q05 == 'Above') ? 'checked' : '' ?> value="Above"></td>
                                            <td><input type="radio" name="q05" <?= ($q05 == 'Outstanding') ? 'checked' : '' ?> value="Outstanding"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Selection of topics</td>
                                            <td><input type="radio" name="q06" <?= ($q06 == 'Poor') ? 'checked' : '' ?> value="Poor"></td>
                                            <td><input type="radio" name="q06" <?= ($q06 == 'Below Average') ? 'checked' : '' ?> value="Below Average"></td>
                                            <td><input type="radio" name="q06" <?= ($q06 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                            <td><input type="radio" name="q06" <?= ($q06 == 'Above') ? 'checked' : '' ?> value="Above"></td>
                                            <td><input type="radio" name="q06" <?= ($q06 == 'Outstanding') ? 'checked' : '' ?> value="Outstanding"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Overall quality of activity</td>
                                            <td><input type="radio" name="q07" <?= ($q07 == 'Poor') ? 'checked' : '' ?> value="Poor"></td>
                                            <td><input type="radio" name="q07" <?= ($q07 == 'Below Average') ? 'checked' : '' ?> value="Below Average"></td>
                                            <td><input type="radio" name="q07" <?= ($q07 == 'Average') ? 'checked' : '' ?> value="Average"></td>
                                            <td><input type="radio" name="q07" <?= ($q07 == 'Above') ? 'checked' : '' ?> value="Above"></td>
                                            <td><input type="radio" name="q07" <?= ($q07 == 'Outstanding') ? 'checked' : '' ?> value="Outstanding"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="row mb-1">
                                <div class="col-12">
                                    <strong>3. How will you change your practice as a result of attending this activity? Select all that apply</strong>
                                </div>
                            </div>
                            <div class="row mt-3 mb-1">
                                <div class="col-12">
                                    <table class="table">
                                        <tr align="left">
                                            <td><input type="radio" name="q08" <?= ($q08 == 'Create/revise protocols, policies, and/or procedures') ? 'checked' : '' ?> value="Create/revise protocols, policies, and/or procedures"> Create/revise protocols, policies, and/or procedures</td>
                                            <td><input type="radio" name="q08" <?= ($q08 == 'This activity validated my current practice') ? 'checked' : '' ?> value="This activity validated my current practice"> This activity validated my current practice</td>
                                        </tr>
                                        <tr align="left">
                                            <td><input type="radio" name="q08" <?= ($q08 == 'Change the management and/or treatment of my patients') ? 'checked' : '' ?> value="Change the management and/or treatment of my patients"> Change the management and/or treatment of my patients</td>
                                            <td><input type="radio" name="q08" <?= ($q08 == 'I will not make any changes to my practice') ? 'checked' : '' ?> value="I will not make any changes to my practice"> I will not make any changes to my practice</td>
                                        </tr>
                                        <tr align="left">
                                            <td><input type="radio" name="q08" <?= ($q08 == ' Other, please specify:') ? 'checked' : '' ?> value=" Other, please specify:">  Other, please specify:
                                              
                                            
                                        </tr>
                                       
                                    </table>
                                </div>
                            </div>
                            <div class="row mt-3 mb-1">
                                <div class="col-12">
                                    <table class="table">
                                        
                                        <tr align="center">
                                            <td align="left"><strong>4. Has this activity met your identified needs and professional practice gaps?</strong></td>
                                            <td><input type="radio" name="q14" <?= ($q14 == 'YES') ? 'checked' : '' ?> value="YES"> YES</td>
                                            <td><input type="radio" name="q14" <?= ($q14 == 'NO') ? 'checked' : '' ?> value="NO"> NO</td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-12">
                                    <strong>   5. Please rate the overall impact of this activity objectives on:</strong>
                                </div>
                            </div>
                            <div class="row mt-3 mb-1">
                                <div class="col-12">
                                    <table class="table">
                                        <tr align="center">
                                            <td align="left"><b></b></td>
                                            <th>Not Applicable</th>
                                            <th>Low Impact</th>
                                            <th>Moderate Impact</th>
                                            <th>High Impact</th>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Knowledge </td>
                                            <td><input type="radio" name="q15" <?= ($q15 == 'Not Applicable') ? 'checked' : '' ?> value="Not Applicable"></td>
                                            <td><input type="radio" name="q15" <?= ($q15 == 'Low Impact') ? 'checked' : '' ?> value="Low Impact"></td>
                                            <td><input type="radio" name="q15" <?= ($q15 == 'Moderate Impact') ? 'checked' : '' ?> value="Moderate Impact"></td>
                                            <td><input type="radio" name="q15" <?= ($q15 == 'High Impact') ? 'checked' : '' ?> value="High Impact"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Competence </td>
                                            <td><input type="radio" name="q16" <?= ($q16 == 'Not Applicable') ? 'checked' : '' ?> value="Not Applicable"></td>
                                            <td><input type="radio" name="q16" <?= ($q16 == 'Low Impact') ? 'checked' : '' ?> value="Low Impact"></td>
                                            <td><input type="radio" name="q16" <?= ($q16 == 'Moderate Impact') ? 'checked' : '' ?> value="Moderate Impact"></td>
                                            <td><input type="radio" name="q16" <?= ($q16 == 'High Impact') ? 'checked' : '' ?> value="High Impact"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Performance </td>
                                            <td><input type="radio" name="q17" <?= ($q17 == 'Not Applicable') ? 'checked' : '' ?> value="Not Applicable"></td>
                                            <td><input type="radio" name="q17" <?= ($q17 == 'Low Impact') ? 'checked' : '' ?> value="Low Impact"></td>
                                            <td><input type="radio" name="q17" <?= ($q17 == 'Moderate Impact') ? 'checked' : '' ?> value="Moderate Impact"></td>
                                            <td><input type="radio" name="q17" <?= ($q17 == 'High Impact') ? 'checked' : '' ?> value="High Impact"></td>
                                        </tr>
                                        <tr align="center">
                                            <td align="left">Patient outcomes </td>
                                            <td><input type="radio" name="q18" <?= ($q18 == 'Not Applicable') ? 'checked' : '' ?> value="Not Applicable"></td>
                                            <td><input type="radio" name="q18" <?= ($q18 == 'Low Impact') ? 'checked' : '' ?> value="Low Impact"></td>
                                            <td><input type="radio" name="q18" <?= ($q18 == 'Moderate Impact') ? 'checked' : '' ?> value="Moderate Impact"></td>
                                            <td><input type="radio" name="q18" <?= ($q18 == 'High Impact') ? 'checked' : '' ?> value="High Impact"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                          
                            <div class="row mt-3 mb-1">
                                <div class="col-12">
                                    <table class="table">
                                        <tr align="center">
                                            <td align="left"><b>  6. What influenced you to attend this meeting?</b></td>
                                            <th width="150">Course description</th>
                                            <th width="100">List of faculty</th>
                                            <th width="100">List of topics</th>
                                            <th width="100">Host site</th>
                                        </tr>
                                        <tr align="center">
                                            <td align="left"> </td>
                                            <td><input type="radio" name="q19" <?= ($q19 == 'Course description') ? 'checked' : '' ?> value="Course description"></td>
                                            <td><input type="radio" name="q19" <?= ($q19 == 'List of faculty') ? 'checked' : '' ?> value="List of faculty"></td>
                                            <td><input type="radio" name="q19" <?= ($q19 == 'List of topics') ? 'checked' : '' ?> value="List of topics"></td>
                                            <td><input type="radio" name="q19" <?= ($q19 == 'Host site') ? 'checked' : '' ?> value="Host site"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-12">
                                    <strong>7. Based on your needs, provide suggestions for future program topics/formats:</strong>
                                    <br>
                                    <textarea name="q20" id="q20" rows="4" class="input"><?= $q20 ?></textarea>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-12">
                                    <strong>General Comments:</strong>
                                    <br>
                                    <textarea name="q21" id="q21" rows="4" class="input"><?= $q21 ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="fbsub-btn" id="btnSubmit" class="btn btn-primary" value="Submit">
                            </div>
                        </form>
                    </div>
                <?php } else { ?>
                    <div id="registration-confirmation">
                        <div class="alert alert-success">
                            Thanks for giving us your valuable feedback!<br>
                        </div>

                    </div>
                <?php } ?>

            </div>
        </div>
       

        <script src="//code.jquery.com/jquery-latest.js"></script>
        <?php require_once 'ga.php';  ?>
        <?php require_once 'footer.php';  ?>