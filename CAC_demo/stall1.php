<?php
require_once "logincheck.php";
require_once "functions.php";

$exhib_id = 'cbc7ad6944f62b7685c3e703cf9b5484ca7afc831be6506af6797655d71ad333';
// require_once "exhibcheck.php";
$curr_room = 'bonk2';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg" class="bonk2">
            <img src="assets/img/stalls/Integrace_1.png">
            <div id="back-button">
                <a href="lobby.php"><i class="fas fa-arrow-alt-circle-left"></i>Back</a>
            </div>
            <a href="assets/resources/11291Cawonpore.jpg" id="poster1" class="view">
                <div class="indicator d-6"></div>
            </a>
            <a href="https://player.vimeo.com/video/616701149?h=79c06f3d4e" id="video1" class="viewvideo1 viewvideo" data-vidid="1234"> </a>
            <a href="assets/resources/11291 Cawonpore.jpg" id="poster2" class="view">
                <div class="indicator d-6"></div>
            </a>
            <!-- <a href="#" data-exhid="<?php echo $exhib_id; ?>" data-userid="<?php echo $_SESSION['userid']; ?>" id="subSampleReq">
                <div class="indicator d-6"></div>
            </a>
            <a href="#" data-exhid="<?php echo $exhib_id; ?>" data-userid="<?php echo $_SESSION['userid']; ?>" id="subProdDet">
                <div class="indicator d-6"></div>
            </a> -->
            <a href="assets/resources/11413.pdf"  id="subSampleReq" class="showpdf viewvideo" data-vidid="1142">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/IJORO - BON K2 lbl 8 july Reference file c2c (1).pdf" class="showpdf viewvideo" data-vidid="1144" id="subProdDet">
                <div class="indicator d-6"></div>
            </a>
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<script>
    $(function() {
        $('.viewvideo1').on('click', function() {
            var vid_id = $(this).data('vidid');
            var userid="<?php echo $_SESSION['userid']; ?>"
        //   alert(vid_id);
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updateVideoView',
                    vidId: vid_id,
                    userId:userid 
                },
                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
</script>
<script>
    $(function() {
        $('.viewvideo').on('click', function() {
            var vid_id = $(this).data('vidid');
            var userid="<?php echo $_SESSION['userid']; ?>"
        //  alert(vid_id);
            $.ajax({
                url: 'control/exhib.php',
                data: {
                    action: 'updatePostView',
                    vidId: vid_id,
                    userId:userid 
                },
                type: 'post',
                success: function(response) {
                    //console.log(response);
                }
            });

        });
    });
</script>
<?php require_once "scripts.php" ?>

<?php require_once "exhib-script.php" ?>

<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>