<?php
require_once 'functions.php';

$errors = [];
$succ = '';

$emailid = '';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  if (empty($_POST['emailid'])) {
    $errors['email'] = 'Email ID is required';
  }

  $emailid = $_POST['emailid'];

  if (count($errors) == 0) {
    $user = new User();
    $user->__set('emailid', $emailid);
    $login = $user->userLogin();
    //var_dump($login);
    $reg_status = $login['status'];
    if ($reg_status == "error") {
      $errors['login'] = $login['message'];
    }
  }
}
?>

<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>CAC</title>
  <link rel="stylesheet" href="assets/css/normalize.min.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
  <link rel="stylesheet" href="assets/css/all.min.css">
  <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>
  <div class="container-fluid">
    <div class="row py-3">
      <div class="col-12 col-md-7 d-grid m-auto">
        <a href="register.php">
          <img src="assets/img/login_left.png" class="img-fluid" alt="">
        </a>

      </div>
      <div class="col-12 col-md-5 d-grid m-auto">
        <div class="right-area-wrapper h-100 text-center">
          <div class="ratio ratio-16x9">
            <iframe src="https://player.vimeo.com/video/611572500?autoplay=1" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
          </div>
          <img src="assets/img/coming-soon.png" class="img-fluid mt-2 soon" style="max-width: 40% !important;" alt="">
          <div id="timer">
            <div class="row">
              <div class="col-12">
                <div id="countdown"></div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
    <div class="row pb-3">
      <div class="col-12 col-md-7">
        <div class="login-area-wrapper">
          <div class="login-wrapper p-3 p-sm-1">
            <?php
            if (count($errors) > 0) : ?>
              <div class="alert alert-danger alert-msg">
                <ul class="list-unstyled">
                  <?php foreach ($errors as $error) : ?>
                    <li>
                      <?php echo $error; ?>
                    </li>
                  <?php endforeach; ?>
                </ul>
              </div>
            <?php endif; ?>
            <form method="POST">
              <div class="row mt-3 mb-1">
                <div class="col-12">
                  <label>Email ID</label>
                  <input type="text" name="emailid" id="emailid" class="input" placeholder="Enter your Email ID" value="<?= $emailid ?>">
                </div>
              </div>
              <div class="row mb-3">
                <div class="col-12">
                  <input type="submit" id="btnLogin" class="form-submit btn-login" value="Login">
                  <br><br>
                  If not registered, <a href="register.php">click here</a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-5">
        <div class="text-center text-white ">
          <!-- For assistance:<br>
          <i class="fas fa-phone-square-alt"></i> +917314-855-655 -->

          <img src="assets/img/login_right2.png" class="img-fluid" width="450px" alt="">
        </div>
      </div>
    </div>
  </div>
  <div id="code">IPL/O/WI/05082021</div>
  <script src="//code.jquery.com/jquery-latest.js"></script>
  <script src="assets/js/mag-popup.js"></script>
  <script type="text/javascript" src="assets/js/jquery.syotimer.min.js"></script>
  <script>
    $(document).ready(function() {

      $('#countdown').syotimer({
        year: 2021,
        month: 8,
        day: 21,
        hour: 11,
        minute: 30,
        timeZone: 0,
        ignoreTransferTime: true,
        layout: 'dhms',
        afterDeadline: function() {
          $('#timer').fadeOut();
          $('.soon').fadeOut();
        }

      });

    });
  </script>
  <?php require_once 'ga.php';  ?>
  <?php require_once 'footer.php';  ?>