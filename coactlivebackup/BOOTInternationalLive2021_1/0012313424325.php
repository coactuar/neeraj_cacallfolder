<?php
require_once 'functions.php';

$errors = [];
$succ = '';

$title = '';
$fname = '';
$lname = '';
$emailid = '';
$mobile = '';
$pincode = '';
$country = 0;
$state = 0;
$city = 0;
$topics = '';
$updates = '';
$app = '0';

$topicsArr = [];
$updatesArr = [];

if (isset($_POST['reguser-btn'])) {
    if (empty($_POST['title'])) {
        $errors['title'] = 'Title is required';
    }
    if (empty($_POST['fname'])) {
        $errors['fname'] = 'First Name is required';
    }
    if (empty($_POST['lname'])) {
        $errors['lname'] = 'Last Name is required';
    }
    if (empty($_POST['emailid'])) {
        $errors['email'] = 'Email ID is required';
    }
    if (empty($_POST['mobile'])) {
        $errors['mobile'] = 'Phone No. is required';
    }
    if (empty($_POST['pincode'])) {
        $errors['pincode'] = 'PINCODE is required';
    }
    if ($_POST['country'] == '0') {
        $errors['country'] = 'Country is required';
    }
    if ($_POST['state'] == '0') {
        $errors['state'] = 'State is required';
    }
    if ($_POST['city'] == '0') {
        $errors['city'] = 'City is required';
    }


    $title = $_POST['title'];
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $emailid = $_POST['emailid'];
    $mobile = $_POST['mobile'];
    $pincode = $_POST['pincode'];

    if (isset($_POST['country'])) {
        $country = $_POST['country'];
    }
    if (isset($_POST['country'])) {
        $country = $_POST['country'];
    }
    if (isset($_POST['state'])) {
        $state = $_POST['state'];
    }
    if (isset($_POST['city'])) {
        $city = $_POST['city'];
    }


    if (isset($_POST['topic'])) {
        $topicsArr = $_POST['topic'];
        foreach ($topicsArr as $topic) {
            $topics .= $topic . ',';
        }
        $topics = substr(trim($topics), 0, -1);
    }
    if (isset($_POST['updates'])) {
        $updatesArr = $_POST['updates'];
        foreach ($updatesArr as $update) {
            $updates .= $update . ',';
        }
        $updates = substr(trim($updates), 0, -1);
    }

    $app = $_POST['app'];

    if (count($errors) == 0) {
        $newuser = new User();
        $newuser->__set('title', $title);
        $newuser->__set('firstname', $fname);
        $newuser->__set('lastname', $lname);
        $newuser->__set('emailid', $emailid);
        $newuser->__set('mobilenum', $mobile);
        $newuser->__set('country', $country);
        $newuser->__set('state', $state);
        $newuser->__set('city', $city);
        $newuser->__set('pincode', $pincode);
        $newuser->__set('verified', $app);
        $newuser->__set('topic_interest', $topics);
        $newuser->__set('updates', $updates);

        $add = $newuser->addUser();
        //var_dump($add);
        $reg_status = $add['status'];

        if ($reg_status == "success") {
            $succ = $add['message'];
            $title = '';
            $fname = '';
            $lname = '';
            $emailid = '';
            $mobile = '';
            $pincode = '';
            $country = 0;
            $state = 0;
            $city = 0;
            $topics = '';
            $updates = '';
            $app = '';
            $topics = '';
            $updates = '';
            $topicsArr = [];
            $updatesArr = [];
        } else {
            $errors['reg'] = $add['message'];
        }
    }
}

if (!empty($_GET['title'])) {
    $title = $_GET['title'];
    $app = '1';
}

if (!empty($_GET['first_name'])) {
    $fname = $_GET['first_name'];
}

if (!empty($_GET['last_name'])) {
    $lname = $_GET['last_name'];
}

if (!empty($_GET['email_id'])) {
    $emailid = $_GET['email_id'];
}

if (!empty($_GET['phone_no'])) {
    $mobile = $_GET['phone_no'];
}

if (!empty($_GET['speciality'])) {
    $specialty = $_GET['speciality'];
}

if (!empty($_GET['education'])) {
    $education = $_GET['education'];
}

if (!empty($_GET['areaofintrest'])) {
    $aoi = $_GET['areaofintrest'];
}


?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $event_title ?></title>
    <link rel="stylesheet" href="assets/css/normalize.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <link rel="stylesheet" href="assets/css/jquery-ui.css" />
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>

    <div class="container bg-white reg-content">
        <div class="row">
            <div class="col-12 p-0">
                <img src="assets/img/reg-banner.png" class="img-fluid" alt="">
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-8 mx-auto p-2">
                <h5>Register for BOOT International Live!</h5>
                <?php
                if (count($errors) > 0) : ?>
                    <div class="alert alert-danger alert-msg">
                        <ul class="list-unstyled">
                            <?php foreach ($errors as $error) : ?>
                                <li>
                                    <?php echo $error; ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
                <?php if ($succ != '') { ?>
                    <div class="alert alert-success alert-msg">
                        <?= $succ ?>
                    </div>
                <?php } ?>
                <form method="POST">
                    <div class="row mt-2">
                        <div class="col-12 col-md-2">
                            <label>Title<sup class="req">*</sup></label>
                            <input type="text" id="title" name="title" class="input" value="<?php echo $title; ?>" autocomplete="off">
                        </div>
                        <div class="col-12 col-md-5">
                            <label>First Name<sup class="req">*</sup></label>
                            <input type="text" id="fname" name="fname" class="input" value="<?php echo $fname; ?>" autocomplete="off">
                        </div>
                        <div class="col-12 col-md-5">
                            <label>Last Name<sup class="req">*</sup></label>
                            <input type="text" id="lname" name="lname" class="input" value="<?php echo $lname; ?>" autocomplete="off">
                        </div>
                    </div>
                    <div class="row mb-1">
                        <div class="col-12">
                            Please enter the name correctly as the same will be used for creating Attendance Certificate.
                        </div>
                    </div>
                    <div class="row mt-3 mb-1">
                        <div class="col-12 col-md-6">
                            <label>Email ID<sup class="req">*</sup></label>
                            <input type="email" id="emailid" name="emailid" class="input" value="<?php echo $emailid; ?>" autocomplete="off">
                        </div>
                        <div class="col-12 col-md-6">
                            <label>Phone No.<sup class="req">*</sup></label>
                            <input type="number" id="mobile" name="mobile" class="input" value="<?php echo $mobile; ?>" autocomplete="off" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
                        </div>
                    </div>
                    <div class="row mt-3 mb-1">
                        <div class="col-12 col-md-6">
                            <label>PINCODE<sup class="req">*</sup></label>
                            <input type="text" id="pincode" name="pincode" class="input" value="<?php echo $pincode; ?>" autocomplete="off" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
                        </div>
                        <div class="col-12 col-md-6">
                            <label>Country<sup class="req">*</sup></label>
                            <div id="countries">
                                <select class="input" id="country" name="country" onChange="updateState()">
                                    <option>Select Country</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="row mt-3 mb-1">
                        <div class="col-12 col-md-6">
                            <label>State<sup class="req">*</sup></label>
                            <div id="states">
                                <select class="input" id="state" name="state" onChange="updateCity()">
                                    <option value="0">Select State</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <label>City<sup class="req">*</sup></label>
                            <div id="cities">
                                <select class="input" id="city" name="city">
                                    <option value="0">Select City</option>
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="row mt-3 mb-1">
                        <div class="col-12 text-left">
                            <label>Select Topics of Interest:<sup class="req">*</sup></label>
                            <label>What topics would be attended in each of the 3 days<sup class="req">*</sup></label>
                            <label><b>Day 1</b></label>
                            <input <?= (in_array('upper-trauma', $topicsArr)) ? 'checked' : '' ?> type="checkbox" name="topic[]" value="upper-trauma">Upper Extremity Trauma <br>
                            <input <?= (in_array('foot-ankle', $topicsArr)) ? 'checked' : '' ?> type="checkbox" name="topic[]" value="foot-ankle">Foot &amp; Ankle Trauma<br>
                            <input <?= (in_array('sine-and-pathological', $topicsArr)) ? 'checked' : '' ?> type="checkbox" name="topic[]" value="sine-and-pathological">Spine Trauma & Pathological fractures<br>
                            <label><b>Day 2</b></label>
                            <input <?= (in_array('knee-trauma', $topicsArr)) ? 'checked' : '' ?> type="checkbox" name="topic[]" value="knee-trauma">Knee Trauma<br>
                            <input <?= (in_array('hip-and-pelvic-trauma', $topicsArr)) ? 'checked' : '' ?> type="checkbox" name="topic[]" value="hip-and-pelvic-trauma">Hip & Pelvic Trauma<br>
                            <input <?= (in_array('infections-management', $topicsArr)) ? 'checked' : '' ?> type="checkbox" name="topic[]" value="infections-management">Infections management<br>
                            <label><b>Day 3</b></label>
                            <input <?= (in_array('ota-president-oration', $topicsArr)) ? 'checked' : '' ?> type="checkbox" name="topic[]" value="ota-president-oration">OTA President Oration<br>
                            <input <?= (in_array('breakthrough-innovation-in-trauma', $topicsArr)) ? 'checked' : '' ?> type="checkbox" name="topic[]" value="breakthrough-innovation-in-trauma">Breakthrough Innovation in Trauma<br>
                            <input <?= (in_array('take-home-messages', $topicsArr)) ? 'checked' : '' ?> type="checkbox" name="topic[]" value="take-home-messages">Take home Messages<br>

                        </div>
                    </div>
                    <div class="row divider">
                        <div class="col-12 mt-1">
                            <input <?= (in_array('program', $updatesArr)) ? 'checked' : '' ?> type="checkbox" name="updates[]" value="program">I would like to receive updates related to this program <br>
                            <input <?= (in_array('integrace', $updatesArr)) ? 'checked' : '' ?> type="checkbox" name="updates[]" value="integrace">I would like to receive updates related to Integrace Pvt. Ltd. <br>
                        </div>
                    </div>
                    <div class="row mt-2 mb-3">
                        <div class="col-12">
                            <small><sup class="req">*</sup> denotes mandatory fields.</small><br><br>
                            <input type="hidden" id="app" name="app" value="<?= $app ?>">
                            <input type="submit" name="reguser-btn" id="btnSubmit" class="form-submit btn-register" value="" />
                            <a href="./" class="form-cancel"><img src="assets/img/cancel-btn.jpg" alt="" /></a>
                        </div>

                    </div>
                </form>
            </div>
        </div>
        <div class="row bg-white">
            <div class="col-12">
                <img src="assets/img/line-h.jpg" class="img-fluid" alt="" />
            </div>
        </div>
        <div class="row bg-white p-2">
            <div class="col-4 bor-right p-2 text-center">
                <img src="assets/img/in-assoc.png" class="img-fluid bot-img" alt="" />
            </div>
            <div class="col-4 p-2 text-center">
                <img src="assets/img/sci-partner.png" class="img-fluid bot-img" alt="" />
            </div>
            <div class="col-4 bor-left p-2 text-center color-grey">
                <img src="assets/img/brought-by.png" class="img-fluid bot-img" alt="" />
                <div class="visit">
                    Visit us at <a href="https://www.integracehealth.com/about.html" class="link" target="_blank">https://www.integracehealth.com/about.html</a>
                </div>
            </div>
        </div>

    </div>
    <div id="code">IPL/O/BR/09042021</div>
    <script src="//code.jquery.com/jquery-latest.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script>
        $(function() {
            getCountries();
        });

        function getCountries() {
            $.ajax({
                url: 'control/event.php',
                data: {
                    action: 'getcountries',
                    country: '<?= $country ?>'
                },
                type: 'post',
                success: function(response) {
                    $("#countries").html(response);
                }
            });
        }

        function updateState() {
            var c = $('#country').val();
            if (c != '0') {
                $.ajax({
                    url: 'control/event.php',
                    data: {
                        action: 'getstates',
                        country: c
                    },
                    type: 'post',
                    success: function(response) {

                        $("#states").html(response);
                    }
                });
            }
        }

        function updateCity() {
            var s = $('#state').val();
            if (s != '0') {
                $.ajax({
                    url: 'control/event.php',
                    data: {
                        action: 'getcities',
                        state: s
                    },
                    type: 'post',
                    success: function(response) {
                        $("#cities").html(response);
                    }
                });
            }
        }
    </script>
    <?php require_once 'ga.php';  ?>
    <?php require_once 'footer.php';  ?>