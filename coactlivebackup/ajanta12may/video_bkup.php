<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_email"]))
	{
		header("location: index.php");
		exit;
	}
?>    
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<style>
html, body{
    height:100%;
}
body{
    margin:0;
    padding:0;
}
#videoPlayer{
    width:100%;
    height:100vh;
}
</style>
<script type="text/javascript" src="//player.wowza.com/player/latest/wowzaplayer.min.js"></script>

</head>

<body>
<div id="videoPlayer"></div>
<script src="js/jquery.min.js"></script>
<script type="text/javascript">
myPlayer = WowzaPlayer.create('videoPlayer',
    {
    "license":"PLAY2-f9DvN-9hUXF-64dZB-j3Vaw-ERHJp",
    //"sourceURL":"https://wowzaprod272-i.akamaihd.net/hls/live/1007426/bfeda308/playlist.m3u8",
    "sourceURL" : "https://wowzaprod272-i.akamaihd.net/hls/live/1007426/fe1a7efe/playlist.m3u8",
    "autoPlay":false,
    "volume":"75",
    "mute":false,
    "loop":false,
    "audioOnly":false,
    "stringErrorStreamUnavailable" : "Please try again later.",
    "posterFrameURL" : "img/poster.jpg",
    }
);
//myPlayer.play();
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-13');
</script>

</body>
</html>
