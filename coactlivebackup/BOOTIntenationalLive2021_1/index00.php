<?php
header('Location: http://coact.live/BOOTInternationalLive2021/');
exit;
include('commons/header.php');
?>
<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php echo $event_title; ?></title>
  <link rel="stylesheet" href="assects/css/bootstrap.min.css">
  <link rel="stylesheet" href="assects/css/all.min.css">
  <link rel="stylesheet" href="assects/css/styles.css">
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>

</head>

<body>
  <div class="container-fluid mx-auto">

    <div class="mt-5" >
      <div class="row">
        <div class="col-12 col-md-7 col-lg-7 ">
          <div >
              <a href="0012313424325.php"><img src="assects/img/login-banner.png" alt="" class="img-fluid" /></a>
          </div>
		  <!--
          <div id="loginform-area">
            If already registered, login here:
            <form method="post">
           
              <div class="form-group">

                <input type="text" name="loginEmail" placeholder="Enter Email ID" class="input" value="">
              </div>
              <div class="form-group">
                <input type="submit" name="mainlogin-btn" id="btnLogin" class="btn btn-login" value="">
              </div>
            </form>
          </div>
-->
        </div>
        <div class="col-12 col-md-5 col-lg-5 text-center" >
		
        <iframe class="" src="https://player.vimeo.com/video/536240918?autoplay=1&amp;loop=1&amp;muted=1 " width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
		 
     
          <!--
          <div id="reg-btn">
            <a href="register.php"><img src="img/reg-now-btn.png" alt="" /></a>
          </div>
-->

        </div>
      </div>
    </div>
  </div>
  <div id="code">IPL/O/BR/09042021</div>



  <script src="assects/js/jquery.min.js"></script>
  <script src="assects/js/bootstrap.min.js"></script>
  <script>
    $(document).ready(function() {
      /*$('#message').hide();
    $("#loginForm").submit(function(event){
		submitForm();
		return false;
	});*/

    });

    function submitForm() {
      $.ajax({
        type: "POST",
        url: "chklogin.php",
        cache: false,
        data: $('form#loginForm').serialize(),
        success: function(response) {
          if (response == 'login') {
            $("#login-modal").modal('hide');
            location.href = 'lobby';
          } else if (response == '0') {
            $('#message').text('You are not registered').removeClass().addClass('alert alert-danger').fadeIn();
            return false;
          } else {
            $('#message').text(response).removeClass().addClass('alert alert-danger').fadeIn();
            return false;
          }
        },
        error: function() {
          alert("Error");
        }
      });
    }
  </script>
 
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <!--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-20"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-20');
</script>-->

</body>

</html>