<?php
require_once 'functions.php';

$errors = [];
$succ = '';

$fname = '';
$lname = '';
$emailid = '';
$mobile = '';
$country = 0;
$state = 0;
$city = 0;
$college = '';
$yearpassed = '';
$mci = '';
$topics = '';
$updates = '';

$topicsArr = [];
$updatesArr = [];

if (isset($_POST['reguser-btn'])) {
    if (empty($_POST['fname'])) {
        $errors['fname'] = 'First Name is required';
    }
    if (empty($_POST['lname'])) {
        $errors['lname'] = 'Last Name is required';
    }
    if (empty($_POST['emailid'])) {
        $errors['email'] = 'Email ID is required';
    }
    if (empty($_POST['mobile'])) {
        $errors['mobile'] = 'Mobile No. is required';
    }
    if (empty($_POST['mobile'])) {
        $errors['mci'] = 'MCI Registration is required';
    }

    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $emailid = $_POST['emailid'];
    $mobile = $_POST['mobile'];

    if (isset($_POST['country'])) {
        $country = $_POST['country'];
    }
    if (isset($_POST['country'])) {
        $country = $_POST['country'];
    }
    if (isset($_POST['state'])) {
        $state = $_POST['state'];
    }
    if (isset($_POST['city'])) {
        $city = $_POST['city'];
    }
    if (isset($_POST['college'])) {
        $college = $_POST['college'];
    }
    if (isset($_POST['yearpassed'])) {
        $yearpassed = $_POST['yearpassed'];
    }
    $mci = $_POST['mci'];

    if (isset($_POST['topic'])) {
        $topicsArr = $_POST['topic'];
        foreach ($topicsArr as $topic) {
            $topics .= $topic . ',';
        }
        $topics = substr(trim($topics), 0, -1);
    }
    if (isset($_POST['updates'])) {
        $updatesArr = $_POST['updates'];
        foreach ($updatesArr as $update) {
            $updates .= $update . ',';
        }
        $updates = substr(trim($updates), 0, -1);
    }

    if (count($errors) == 0) {
        $newuser = new User();
        $newuser->__set('firstname', $fname);
        $newuser->__set('lastname', $lname);
        $newuser->__set('emailid', $emailid);
        $newuser->__set('mobilenum', $mobile);
        $newuser->__set('country', $country);
        $newuser->__set('state', $state);
        $newuser->__set('city', $city);
        $newuser->__set('college', $college);
        $newuser->__set('yearpassed', $yearpassed);
        $newuser->__set('mci', $mci);
        $newuser->__set('topic_interest', $topics);
        $newuser->__set('updates', $updates);

        $add = $newuser->addUser();
        //var_dump($add);
        $reg_status = $add['status'];

        if ($reg_status == "success") {
            $succ = $add['message'];
            $fname = '';
            $lname = '';
            $emailid = '';
            $mobile = '';
            $country = 0;
            $state = 0;
            $city = 0;
            $college = '';
            $yearpassed = '';
            $mci = '';
            $topics = '';
            $updates = '';
            $topicsArr = [];
            $updatesArr = [];
        } else {
            $errors['reg'] = $add['message'];
        }
    }
}

?>
<?php require_once 'header.php';  ?>
<div class="container bg-white reg-content">
    <div class="row">
        <div class="col-10 offset-1 col-md-6 offset-md-3 text-center p-2">
            <img src="assets/img/reg-top-banner.png" class="img-fluid" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-12 p-0">
            <img src="assets/img/top-bar.jpg" class="img-fluid" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 offset-md-3 p-2">
            <h5>Register for BOOT</h5>
            <?php
            if (count($errors) > 0) : ?>
                <div class="alert alert-danger alert-msg">
                    <ul class="list-unstyled">
                        <?php foreach ($errors as $error) : ?>
                            <li>
                                <?php echo $error; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
            <?php if ($succ != '') { ?>
                <div class="alert alert-success alert-msg">
                    <?= $succ ?>
                </div>
            <?php } ?>
            <form method="POST">
                <div class="row mt-2">
                    <div class="col-12 col-md-6">
                        <label>First Name<sup class="req">*</sup></label>
                        <input type="text" id="fname" name="fname" class="input" value="<?php echo $fname; ?>" autocomplete="off">
                    </div>
                    <div class="col-12 col-md-6">
                        <label>Last Name<sup class="req">*</sup></label>
                        <input type="text" id="lname" name="lname" class="input" value="<?php echo $lname; ?>" autocomplete="off">
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-12">
                        Please enter the name correctly as the same will be used for creating Attendance Certificate.
                    </div>
                </div>
                <div class="row mt-3 mb-1">
                    <div class="col-12">
                        <label>Email ID<sup class="req">*</sup></label>
                        <input type="email" id="emailid" name="emailid" class="input" value="<?php echo $emailid; ?>" autocomplete="off">
                    </div>
                </div>
                <div class="row mt-3 mb-1">
                    <div class="col-12 col-md-6">
                        <label>Mobile No.<sup class="req">*</sup></label>
                        <input type="number" id="mobile" name="mobile" class="input" value="<?php echo $mobile; ?>" autocomplete="off" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
                    </div>
                    <div class="col-12 col-md-6">
                        <label>Country</label>
                        <div id="countries">
                            <select class="input" id="country" name="country" onChange="updateState()">
                                <option>Select Country</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="row mt-3 mb-1">
                    <div class="col-12 col-md-6">
                        <label>State</label>
                        <div id="states">
                            <select class="input" id="state" name="state" onChange="updateCity()">
                                <option value="0">Select State</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <label>City</label>
                        <div id="cities">
                            <select class="input" id="city" name="city">
                                <option value="0">Select City</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row mt-3 mb-1">
                    <div class="col-12 col-md-6">
                        <label>Academic College</label>
                        <input type="text" id="college" name="college" class="input" value="<?php echo $college; ?>" autocomplete="off">
                    </div>
                    <div class="col-12 col-md-6">
                        <label>Year of Passing</label>
                        <div id="years">
                            <select class="input" id="yearpassed" name="yearpassed">
                                <option value='0'>Select Year of Passing</option>
                                <?php
                                for ($i = 1900; $i <= 2020; $i++) {
                                ?>
                                    <option value="<?= $i ?>" <?= ($yearpassed == $i) ? 'selected' : '' ?>><?= $i ?></option>
                                <?php
                                }
                                ?>

                            </select>
                        </div>
                    </div>

                </div>
                <div class="row mt-3 mb-1">
                    <div class="col-12 col-md-6">
                        <label>MCI Registration<sup class="req">*</sup></label>
                        <input type="text" id="mci" name="mci" class="input" value="<?php echo $mci; ?>" autocomplete="off">
                    </div>
                </div>
                <div class="row mt-3 mb-1">
                    <div class="col-12 text-left">
                        <label>Select Topics of Interest:</label>
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <label><b>Day 1</b></label>
                                <input <?= (in_array('High Risk Pregnancy', $topicsArr)) ? 'checked' : '' ?> type="checkbox" name="topic[]" value="High Risk Pregnancy">High Risk Pregnancy <br>
                                <input <?= (in_array('Infertility', $topicsArr)) ? 'checked' : '' ?> type="checkbox" name="topic[]" value="Infertility">Infertility<br>
                                <input <?= (in_array('Endoscopy', $topicsArr)) ? 'checked' : '' ?> type="checkbox" name="topic[]" value="Endoscopy">Endoscopy
                            </div>
                            <div class="col-12 col-md-6"><label><b>Day 2</b></label>
                                <input <?= (in_array('Foetal Medicine', $topicsArr)) ? 'checked' : '' ?> type="checkbox" name="topic[]" value="Foetal Medicine">Foetal Medicine<br>
                                <input <?= (in_array('Urogynaecology', $topicsArr)) ? 'checked' : '' ?> type="checkbox" name="topic[]" value="Urogynaecology">Urogynaecology<br>
                                <input <?= (in_array('Oncology', $topicsArr)) ? 'checked' : '' ?> type="checkbox" name="topic[]" value="Oncology">Oncology
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row divider">
                    <div class="col-12 mt-1">
                        <input <?= (in_array('program', $updatesArr)) ? 'checked' : '' ?> type="checkbox" name="updates[]" value="program">I would like to receive updates related to this program <br>
                        <input <?= (in_array('integrace', $updatesArr)) ? 'checked' : '' ?> type="checkbox" name="updates[]" value="integrace">I would like to receive updates related to Integrace Pvt. Ltd. <br>
                    </div>
                </div>
                <div class="row mt-2 mb-3">
                    <div class="col-12">
                        <small><sup class="req">*</sup> denotes mandatory fields.</small><br><br>
                        <input type="submit" name="reguser-btn" id="btnSubmit" class="form-submit btn-register" value="" />
                        <a href="./" class="form-cancel"><img src="assets/img/btn-cancel.jpg" alt="" /></a>
                    </div>

                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-10 offset-md-1">
            <img src="assets/img/reg-bottom-banner.png" class="img-fluid" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-12 p-0">
            <img src="assets/img/bottom-bar.jpg" class="img-fluid" alt="">
        </div>
    </div>


    <div class="left-art d-none d-md-block">
        <img sizes="(min-width: 400px) 80vw, 100vw" srcset="assets/img/left-art-120.png 375w,
          assets/img/left-art-180.png 1500w">
    </div>

</div>
<script src="//code.jquery.com/jquery-latest.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script>
    $(function() {
        getCountries();
    });

    function getCountries() {
        $.ajax({
            url: 'control/event.php',
            data: {
                action: 'getcountries',
                country: '<?= $country ?>'
            },
            type: 'post',
            success: function(response) {
                $("#countries").html(response);
            }
        });
    }

    function updateState() {
        var c = $('#country').val();
        if (c != '0') {
            $.ajax({
                url: 'control/event.php',
                data: {
                    action: 'getstates',
                    country: c
                },
                type: 'post',
                success: function(response) {

                    $("#states").html(response);
                }
            });
        }
    }

    function updateCity() {
        var s = $('#state').val();
        if (s != '0') {
            $.ajax({
                url: 'control/event.php',
                data: {
                    action: 'getcities',
                    state: s
                },
                type: 'post',
                success: function(response) {
                    $("#cities").html(response);
                }
            });
        }
    }
</script>
<?php require_once 'ga.php';  ?>
<?php require_once 'footer.php';  ?>