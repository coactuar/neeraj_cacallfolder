<?php
require_once "../control/sesAdminCheck.php";
?>
<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Dashboard</title>
  <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/styles.css">

</head>

<body class="admin">
  <nav class="navbar navbar-expand-md bg-dark">
    <!--<a class="navbar-brand" href="#"><img src="../img/logo.png" class="logo"></a>-->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="dashboard.php">Dashboard</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="users.php">Registered Users</a>
        </li>


      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Hello, <?php echo $_SESSION["admin"]; ?>!</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="?action=logout">Logout</a>
        </li>
      </ul>

    </div>
  </nav>

  <div class="container-fluid bg-white color-grey">

    <div class="row mt-1 p-2">
      <div class="col-12 col-md-4 offset-md-1">
        <h6>Registered Users</h6>
        <?php
        $sql = "select * from tbl_users";
        $res = mysqli_query($link, $sql);
        echo 'Total Registered Users: ' . mysqli_num_rows($res) . '<br>';
        // $sql = "select * from tbl_users where verified='1'";
        // $res = mysqli_query($link, $sql);
        // echo 'Total Verified Users: ' . mysqli_num_rows($res).'<br>';
        ?>
      </div>
    </div>
  </div>


  <script src="../js/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script>
    $(function() {

    });
  </script>
  <?php
  function tosecs($seconds)
  {
    $t = round($seconds);
    return sprintf('%02d:%02d:%02d', ($t / 3600), ($t / 60 % 60), $t % 60);
  }
  ?>
</body>

</html>