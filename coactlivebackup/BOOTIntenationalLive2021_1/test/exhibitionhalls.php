<?php
require_once "logincheck.php";
$curr_room = 'exhibitionhall';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/exhibitionhall.jpg" usemap="#image-map">
            <!-- <map name="image-map">
                <area title="TV" href="https://player.vimeo.com/video/481733317" class="viewvideo" coords="4583,1166,3353,445" shape="rect">
                <area alt="Dubinor" title="Dubinor" href="dubinor.php" coords="1388,1944,1961,2524" shape="rect">
                <area alt="EVOB PG" title="EVOB PG" href="ebovpg.php" coords="2894,2806,2258,2262" shape="rect">
                <area alt="Stiloz" title="Stiloz" href="stiloz.php" coords="3304,2368,2915,1951" shape="rect">
                <area alt="Dubinor Ointments" title="Dubinor Ointments" href="dubinor-ointments.php" coords="4300,1930,4731,2375" shape="rect">
                <area alt="Vkonnecthealth" title="Vkonnecthealth" href="vkonnecthealth.php" coords="4774,2213,5360,2778" shape="rect">
                <area alt="BMD camp" title="BMD camp" href="bmdCamp.php" coords="6328,1837,5777,2312" shape="rect">
                <area alt="BonDk" title="BonDk" href="bondk.php" coords="1820,1428,2265,1803" shape="rect">
                <area alt="Esoz" title="Esoz" href="esoz.php" coords="2534,1520,2972,1937" shape="rect">
                <area alt="Colsmart" title="Colsmart" href="colsmartA.php" coords="3615,1414,4046,1781" shape="rect">
                <area alt="BonK2" title="BonK2" href="bonk2.php" coords="5071,1477,4576,1916" shape="rect">
                <area alt="Lizolid" title="Lizolid" href="lizolid.php" coords="5346,1378,5834,1788" shape="rect">
            </map> -->
            <map name="image-map">
                <area target="" alt="BON DK" title="BON DK" href="bondk.php" coords="1918,1808,1911,1617,1932,1469,2201,1448,2250,1596,2278,1808" shape="poly">
                <area target="" alt="ESOZ" title="ESOZ" href="esoz.php" coords="2596,1985,2575,1617,2879,1589,2957,1730,2964,1978" shape="poly">
                <area target="" alt="Collasmart-A" title="Collasmart-A" href="colsmartA.php" coords="3671,1815,3664,1596,3692,1448,3953,1448,4010,1589,4010,1815" shape="poly">
                <area target="" alt="BON K2" title="BON K2" href="bonk2.php" coords="4604,1943,4625,1695,4653,1554,4922,1561,4971,1589,4985,1943" shape="poly">
                <area target="" alt="Lizolid" title="Lizolid" href="lizolid.php" coords="5395,1798,5423,1558,5459,1417,5720,1410,5755,1572,5741,1798" shape="poly">
                <area target="" alt="Dubinor" title="Dubinor" href="dubinor.php" coords="1536,2519,1487,2116,1572,2046,1833,2081,1932,2237,1967,2512" shape="poly">
                <area target="" alt="Stiloz" title="Stiloz" href="stiloz.php" coords="2943,2402,2929,2155,2964,1985,3331,1978,3324,2155,3324,2409" shape="poly">
                <area target="" alt="Dubinor Ointments" title="Dubinor Ointments" href="dubinor-ointments.php" coords="4342,2382,4307,2205,4307,1944,4660,1951,4759,2135,4717,2389" shape="poly">
                <area target="" alt="BMD Camps" title="BMD Camps" href="bmdcamp.php" coords="5777,2347,5805,2064,5854,1852,6158,1852,6243,1965,6194,2347" shape="poly">
                <area target="" alt="EBOV PG" title="EBOV PG" href="ebovpg.php" coords="2399,2835,2399,2538,2455,2354,2766,2340,2858,2517,2886,2828" shape="poly">
                <area target="" alt="VKONNECT HEALTH" title="VKONNECT HEALTH" href="vkonnecthealth.php" coords="4823,2792,4844,2488,4886,2297,5339,2297,5310,2488,5282,2771" shape="poly">
            </map>
            <a href="https://player.vimeo.com/video/481733317" id="exhVideo" class="viewvideo"></a>

        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "scripts.php" ?>
<script src="assets/js/image-map.js"></script>
<script>
    ImageMap('img[usemap]', 500);
</script>
<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>