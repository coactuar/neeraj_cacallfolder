<?php
require_once "logincheck.php";
$curr_room = 'games';

?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>

<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/bg.jpg" usemap="#image-map">
            <map name="image-map">

            </map>
            <a class="game" href="#" style="position:absolute;left:20%;top:20%;color:white;font-size:20px;" onclick="gamescore('sudoku')">Sudoku</a>
            <a class="game" href="#" style="position:absolute;left:50%;top:20%;color:white;font-size:20px;" onclick="gamescore('image-puzzle')">Image Puzzle</a>
            <a class="game" href="#" style="position:absolute;left:20%;top:40%;color:white;font-size:20px;" onclick="gamescore('car-race')">Car Race</a>
            <a class="game" href="#" style="position:absolute;left:50%;top:40%;color:white;font-size:20px;" onclick="gamescore('word-search')">Word Search</a>
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>

<?php require_once "scripts.php" ?>
<script>
    function gamescore(a) {
        $.ajax({
            url: 'control/lb.php',
            data: {
                action: 'updpoints',
                userId: '<?= $_SESSION['userid'] ?>',
                activity: 'PLAY_GAME',
                loc: a
            },
            type: 'post',
            success: function(message) {}
        });
        window.open('games/' + a + '', '_blank');
    }
</script>
<script src="assets/js/image-map.js"></script>
<script>
    ImageMap('img[usemap]', 500);
</script>

<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>