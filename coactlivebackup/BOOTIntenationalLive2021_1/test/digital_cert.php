<?php
require_once "logincheck.php";
$curr_room = 'digital_cert';

?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>

<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/bg.jpg">

            <?php

            $id = $_SESSION['userid'];
            $img = imagecreatefromjpeg('digitalcert/temp/image.jpg');
            $white = imagecolorallocate($img, 255, 255, 255);
            $font = "C:\Windows\Fonts\arial.ttf";
            imagettftext($img, 55, 0, 1600, 1000, $white, $font, $user_name);

            imagejpeg($img, "digitalcert/cert/Boot_crt_" . $user_name . $id . ".JPG", 100);

            $url = '//' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']) . '/digitalcert/cert/Boot_crt_' . $user_name . $id . ".JPG";
            ?>

            <a href="<?= $url; ?>" target="_blank" style="position: absolute;top: 11%;z-index: 100;width: 47%;left: 21%;" download><img src="<?= $url; ?>" width=500px /></a><br>
            <small>(Right-click and select Save link as)</small><br><br>

        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>

<?php require_once "scripts.php" ?>

<script src="assets/js/image-map.js"></script>
<script>
    ImageMap('img[usemap]', 500);
</script>
<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>