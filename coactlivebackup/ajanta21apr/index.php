<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Ajanta - Digital Dialogues :: Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<nav class="nav navbar bg-white">
    <a class="navbar-brand" href="#"><img src="img/logo.png" class="img-fluid" alt=""/></a>
    <a class="navbar-brand" href="#"><img src="img/info.jpg" class="img-fluid logo2" alt=""/></a> 
</nav>
<div class="container-fluid">
    <div class="row title">
      <div class="col-12 col-md-8 offset-md-2 text-center">
      &nbsp;
      </div>
    </div>  
    <div class="row mt-2 mb-2">
      <div class="col-8 col-md-7 col-lg-5 offset-lg-1 offset-md-0 offset-2 text-center">
        <img src="img/experts.jpg" class="img-fluid" alt=""/> 
      </div>
      <div class="col-10 col-md-5 col-lg-4 offset-md-0 offset-1 text-center">
            <div class="login">
                <form id="login-form" method="post" role="form">
                  <div id="login-message"></div>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Name" aria-label="Name" aria-describedby="basic-addon1" name="name" id="name" required>
                  </div>
                  
                  <div class="input-group">
                    <input type="email" class="form-control" placeholder="Email" aria-label="Email" aria-describedby="basic-addon1" name="email" id="email" required>
                  </div>
                  <div class="input-group">
                    <input type="number" class="form-control" placeholder="Mobile No." aria-label="Mobile No." aria-describedby="basic-addon1" name="mobnum" id="mobnum" maxlength="11" required oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
                  </div>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Location" aria-label="Location" aria-describedby="basic-addon1" name="location" id="location" required>
                  </div>
                  <div class="input-group">
                    <button id="login" class="btn btn-primary btn-sm login-button" type="submit">Login</button>
                  </div>
                </form>
            </div>
        
        </div>
    </div>
    
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#login-form', function()
{  
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
      {
        window.location.href='webcast.php';  
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
  });
  
  return false;
});
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-13');
</script>

</body>
</html>