<?php
require_once 'constants.php';
class User
{

    private $ds;

    function __construct()
    {
        $this->ds = new DataSource();
    }

    public function isMemberExists($email)
    {
        $query = 'SELECT * FROM tbl_users WHERE emailid = ?';
        $paramType = 's';
        $paramValue = array(
            $email
        );
        $insertRecord = $this->ds->select($query, $paramType, $paramValue);
        $count = 0;
        if (is_array($insertRecord)) {
            $count = count($insertRecord);
        }
        return $count;
    }
    public function isMemberExistsById($userid)
    {
        $query = 'SELECT * FROM tbl_users WHERE userid = ?';
        $paramType = 's';
        $paramValue = array(
            $userid
        );
        $insertRecord = $this->ds->select($query, $paramType, $paramValue);
        $count = 0;
        if (is_array($insertRecord)) {
            $count = count($insertRecord);
        }
        return $count;
    }

    public function isMemberValid($userid)
    {
        $query = "SELECT * FROM tbl_users WHERE userid = ? and active='1' and verified='1'";
        $paramType = 's';
        $paramValue = array(
            $userid
        );
        $insertRecord = $this->ds->select($query, $paramType, $paramValue);
        $count = 0;
        if (is_array($insertRecord)) {
            $count = count($insertRecord);
        }
        return $count;
    }

    public function registerMember()
    {
        $result = $this->isMemberExists($_POST["emailid"]);
        if ($result < 1) {
            $fname = $_POST['fname'];
            $lname = $_POST['lname'];
            $email = $_POST['emailid'];
            $phone = $_POST['phone'];
            $country = $_POST['country'];
            $state = $_POST['state'];
            $city = $_POST['city'];
            $topics = '';
            if (isset($_POST['topic'])) {
                $topicsArr = $_POST['topic'];
                foreach ($topicsArr as $topic) {
                    $topics .= $topic . ',';
                }
                $topics = substr(trim($topics), 0, -1);
            }
            $updates = '';
            if (isset($_POST['updates'])) {
                $updatesArr = $_POST['updates'];
                foreach ($updatesArr as $update) {
                    $updates .= $update . ',';
                }
                $updates = substr(trim($updates), 0, -1);
            }
            $isp = $_POST['isp'];


            $userid = bin2hex(random_bytes(24)); // generate unique token
            $token = bin2hex(random_bytes(50)); // generate unique token
            $reg_date   = date('Y/m/d H:i:s');
            $token_exp_date  = date('Y/m/d H:i:s', time() + 60 * 60 * 24);
            $verified = 0;
            $active = 1;

            $query = 'INSERT INTO tbl_users(userid,first_name, last_name, emailid, phone_num, country, state, city, topic_interest, updates, isp, reg_date, token, token_expire,verified, active) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
            $paramType = 'sssssiiissssssii';
            $paramValue = array(
                $userid,
                $fname,
                $lname,
                $email,
                $phone,
                $country,
                $state,
                $city,
                $topics,
                $updates,
                $isp,
                $reg_date,
                $token,
                $token_exp_date,
                $verified,
                $active
            );
            $memberId = $this->ds->insert($query, $paramType, $paramValue);
            if (!empty($memberId)) {
                require 'sendemail.php';
                //send verification email
                //$reglink = "https://coact.live/BOOTInternationalLive/verify.php?i=" . $userid . "&t=" . $token;
                //$siteurl = 'https://coact.live/BOOTInternationalLive';

                $reglink = "https://coact.live/BOOT/verify.php?i=" . $userid . "&t=" . $token;
                $siteurl = 'https://coact.live/BOOT/';

                $name = $fname . ' ' . $lname;
                $message = file_get_contents('https://coact.live/BOOT/email_templates/reg-email.html');
                $message = str_replace('%name%', $name, $message);
                $message = str_replace('%reglink%', $reglink, $message);
                $message = str_replace('%siteurl%', $siteurl, $message);

                $mail = new SendEmail();
                $mail->to = $email;
                $mail->name = $name;
                $mail->subject = 'Verify your Email-Id for BOOT International Live';
                //$mail->message = $message;

                $mail->sendTemplate($name, $reglink, $siteurl);

                $response = array("status" => "success", "message" => "Verification mail sent.");
            }
        } else if ($result == 1) {
            $response = array("status" => "error", "message" => "Email already exists.");
        }
        return $response;
    }

    public function getMemberEmail($userid)
    {
        $query = 'select emailid from tbl_users where userid=? limit 1';
        $paramType = 's';
        $paramValue = array(
            $userid
        );
        $email = $this->ds->select($query, $paramType, $paramValue);
        return $email[0]['emailid'];
    }

    public function getMemberName($userid)
    {
        $query = 'select first_name, last_name from tbl_users where userid=? limit 1';
        $paramType = 's';
        $paramValue = array(
            $userid
        );
        $loginUser = $this->ds->select($query, $paramType, $paramValue);
        return $loginUser[0]['first_name'] . ' ' . $loginUser[0]['last_name'];
    }

    public function getMemberNameByEmail($email)
    {
        $query = 'select first_name, last_name from tbl_users where emailid=? limit 1';
        $paramType = 's';
        $paramValue = array(
            $email
        );
        $loginUser = $this->ds->select($query, $paramType, $paramValue);
        return $loginUser[0]['first_name'] . ' ' . $loginUser[0]['last_name'];
    }

    public function getMember($userid)
    {
        $query = 'SELECT * FROM tbl_users WHERE userid = ?';
        $paramType = 's';
        $paramValue = array(
            $userid
        );
        $member = $this->ds->select($query, $paramType, $paramValue);
        return $member;
    }

    public function getMemberbyEmail($emailid)
    {
        $query = 'SELECT * FROM tbl_users WHERE emailid = ?';
        $paramType = 's';
        $paramValue = array(
            $emailid
        );
        $member = $this->ds->select($query, $paramType, $paramValue);
        return $member;
    }

    public function getMemberbyId($userid)
    {
        $query = 'SELECT * FROM tbl_users WHERE id = ?';
        $paramType = 's';
        $paramValue = array(
            $userid
        );
        $loginUser = $this->ds->select($query, $paramType, $paramValue);
        return $loginUser;
    }

    public function loginMember($email)
    {
        $loginUserResult = $this->getMemberByEmail($email);

        if (!empty($loginUserResult)) {

            $user_id = $loginUserResult[0]['userid'];

            $verified = $loginUserResult[0]['verified'];

            if (!$verified) {
                $loginStatus =  'You are not verified. Please check your email for verification link.<br><a href="resend.php?i=' . $user_id . '">Resend Verification Link</a>';
                return $loginStatus;
            }

            $active = $loginUserResult[0]['active'];

            if (!$active) {
                $loginStatus = 'You are not authorized to enter the conference.';
                return $loginStatus;
            }

            if ($verified && $active) {
                $today   = date('Y/m/d H:i:s');
                $logout_date   = date('Y/m/d H:i:s', time() + 30);

                $dateTimestamp1 = strtotime($loginUserResult[0]["logout_date"]);
                $dateTimestamp2 = strtotime($today);

                if ($dateTimestamp1 > $dateTimestamp2) {
                    $loginStatus =  "You are already logged in from another location. Please logout from other location and try again.";
                    return $loginStatus;
                } else {
                    $query = "UPDATE tbl_users set login_date = ?, logout_date=? where emailid=?";
                    $paramType = 'sss';
                    $paramValue = array(
                        $today,
                        $logout_date,
                        $email
                    );

                    $this->ds->execute($query, $paramType, $paramValue);

                    $query = "Insert into tbl_user_logins(user_id, join_time, leave_time) values(?, ?, ?)";
                    $paramType = 'sss';
                    $paramValue = array(
                        $user_id,
                        $today,
                        $logout_date
                    );

                    $this->ds->execute($query, $paramType, $paramValue);

                    $_SESSION['user_id'] = $loginUserResult[0]['userid'];
                    $_SESSION['user_first_name'] = $loginUserResult[0]['first_name'];
                    $_SESSION['user_last_name'] = $loginUserResult[0]['last_name'];
                    $_SESSION['user_emailid'] = $loginUserResult[0]['emailid'];

                    //$status =  $_SESSION['user_id'];
                    ///return $status;
                    $url = "lobby";
                    header("Location: $url");
                }
            }
        } else {
            $loginStatus = "You are not registered.";

            //throwError(INVALID_USER, $loginStatus);
            return $loginStatus;
        }
    }

    /*public function logoutMember($email)
    {
        $logoutUserResult = $this->getMember($email);
        if (!empty($logoutUserResult)) {
            
            $logout_date   = date('Y/m/d H:i:s');
            
            $query="UPDATE tbl_users set logout_date=?, current_room=? where emailid=?";
            $paramType = 'sss';
            $paramValue = array(
                $logout_date,
                '',
                $email
            );
            
            $this->ds->execute($query, $paramType, $paramValue);

            unset($_SESSION['user_id']);

            $url = "index.php";//"conf-feedback.php";
            header("Location: $url");
            
        }
    
    }*/



    public function getLiveAttendeesCount()
    {
        $today = date('Y/m/d H:i:s');
        $query = "SELECT * FROM tbl_users where logout_date > ?";
        $paramType = 's';
        $paramValue = array(
            $today
        );

        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $count;
    }

    public function getLiveAttendees()
    {
        $today = date('Y/m/d H:i:s');
        $query = "SELECT * FROM tbl_users where logout_date > ?";
        $paramType = 's';
        $paramValue = array(
            $today
        );

        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $count;
    }

    public function getPageAttendeesCount($room)
    {
        $today = date('Y/m/d H:i:s');
        $query = "SELECT * FROM tbl_users where logout_date > ? and current_room=?";
        $paramType = 'ss';
        $paramValue = array(
            $today,
            $room
        );

        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $count;
    }

    public function updateMemberLoginStatus($userid, $room)
    {
        $loginUserResult = $this->getMember($userid);
        $loggedin = 0;

        if (!empty($loginUserResult)) {

            $today = date("Y/m/d H:i:s");

            //$dateTimestamp1 = strtotime($loginUserResult[0]['logout_date']);
            //$dateTimestamp2 = strtotime($today);
            // if ($dateTimestamp1 < $dateTimestamp2) {

            // } else {
            $logout_date  = date('Y/m/d H:i:s', time() + 30);
            $query = "UPDATE tbl_users set logout_date=?, current_room=? where userid=?";
            $paramType = 'sss';
            $paramValue = array(
                $logout_date,
                $room,
                $userid
            );

            $this->ds->execute($query, $paramType, $paramValue);

            $query = "select * from tbl_user_logins where user_id=? and leave_time >= ? limit 1";
            $paramType = 'ss';
            $paramValue = array(
                $userid,
                $today
            );
            //$loginUser = $this->ds->select($query, $paramType, $paramValue);
            $count = $this->ds->getRecordCount($query, $paramType, $paramValue);
            if ($count > 0) {
                $leave_time  = date('Y/m/d H:i:s', time() + 30);
                $query = "UPDATE tbl_user_logins set leave_time=? where user_id=? and leave_time >= ?";
                $paramType = 'sss';
                $paramValue = array(
                    $leave_time,
                    $userid,
                    $today
                );

                $this->ds->execute($query, $paramType, $paramValue);
            }
            $loggedin = 1;
            // }

            return $loggedin;
        }
    }

    function getonlinemembers($keyword)
    {
        $today   = date('Y/m/d H:i:s');
        $query = "select * from tbl_users where logout_date > ? and ((first_name like '%$keyword%') or (last_name like '%$keyword%')) order by first_name asc, last_name asc";
        $paramType = 's'; //ssss';
        $paramValue = array(
            $today,
            /*$keyword,
            $keyword,
            $keyword,
            $keyword*/
        );

        $online = $this->ds->select($query, $paramType, $paramValue);

        return $online;
    }

    function sendEmail($from, $to, $message)
    {
        $sender = $this->getMemberbyId($from);
        $sender_name = $sender[0]['first_name'] . ' ' . $sender[0]['last_name'];
        $sender_email = $sender[0]['emailid'];
        $sender_phone = $sender[0]['phone_num'];

        $receiver = $this->getMemberbyId($to);
        $receiver_name = $receiver[0]['first_name'] . ' ' . $receiver[0]['last_name'];
        $receiver_email = $receiver[0]['emailid'];

        $msg = 'Dear ' . $receiver_name . ',';
        $msg .= '<br>';
        $msg .= $sender_name . ' has send you following message while attending our online conference.';
        $msg .= '<br>' . $message;
        $msg .= '<br><br>';
        $msg .= 'You can contact ' . $sender_name . ' on here:';
        $msg .= '<br>Email: ' . $sender_email;
        $msg .= '<br>Phone No. ' . $sender_phone;
        $msg .= '<br><br>Thank you!';

        require 'sendemail.php';
        $mail = new SendEmail();
        $mail->to = $receiver_email;
        $mail->name = $receiver_name;
        $mail->subject = 'You received an email from ' . $sender_name;
        $mail->message = $msg;

        $mail->send();
    }

    function sendChatToUser($from, $to, $msg)
    {
        $chat_time   = date('Y/m/d H:i:s');

        $chat_time   = date('Y/m/d H:i:s');
        $query = "insert into tbl_attendee_chat(user_id_from, user_id_to, message, chat_time) values(?,?,?,?)";
        $paramType = 'ssss';
        $paramValue = array(
            $from,
            $to,
            $msg,
            $chat_time
        );

        $chatid = $this->ds->insert($query, $paramType, $paramValue);
        return $chatid;
    }

    public function getUserChatHistory($to, $from)
    {
        $query = "select first_name, message, user_id_from, chat_time from tbl_attendee_chat,tbl_users where ((user_id_from=? AND user_id_to =?) OR (user_id_from=? AND user_id_to=?)) AND tbl_attendee_chat.user_id_from = tbl_users.userid order by chat_time asc";
        $paramType = 'ssss'; //ssss';
        $paramValue = array(
            $from,
            $to,
            $to,
            $from
        );

        $history = $this->ds->select($query, $paramType, $paramValue);

        return $history;
    }

    /*My Briefcase - Chat Inbox*/
    function getMemberChats($userid)
    {
        $query = "(SELECT distinct(user_id_from),first_name, last_name FROM tbl_attendee_chat, tbl_users where user_id_to =? and tbl_attendee_chat.user_id_from=tbl_users.id) UNION (SELECT distinct(user_id_to),first_name, last_name FROM tbl_attendee_chat,`tbl_users` where user_id_from =?  and tbl_attendee_chat.user_id_to=tbl_users.userid)";
        $paramType = 'ss'; //ssss';
        $paramValue = array(
            $userid,
            $userid
        );

        $chats = $this->ds->select($query, $paramType, $paramValue);

        return $chats;
    }

    function getMemberTotalUnreadChatCount($userid)
    {
        $query = "select * from tbl_attendee_chat where user_id_to=? and read_status='0'";
        $paramType = 's'; //ssss';
        $paramValue = array(
            $userid
        );

        $chats = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $chats;
    }

    function getMemberUnreadChatCount($from, $userid)
    {
        $query = "select * from tbl_attendee_chat where (user_id_from=? and user_id_to=? AND read_status='0')";
        $paramType = 'ss'; //ssss';
        $paramValue = array(
            $from,
            $userid
        );

        $chats = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $chats;
    }


    function getMemberDownloads($userid)
    {
        $query = "select DISTINCT tbl_exhibitor_resource_downloads.resource_id, exhibitor_name, tbl_exhibitor_resources.resource_title from tbl_exhibitor_resource_downloads, tbl_exhibitor_resources, tbl_exhibitors where tbl_exhibitor_resource_downloads.resource_id=tbl_exhibitor_resources.resource_id AND user_id=? and tbl_exhibitor_resources.exhibitor_id=tbl_exhibitors.exhibitor_id";
        $paramType = 's'; //ssss';
        $paramValue = array(
            $userid,
        );

        $downloads = $this->ds->select($query, $paramType, $paramValue);

        return $downloads;
    }

    function getMemberVideos($userid)
    {
        $query = "SELECT DISTINCT tbl_exhibitor_videos_views.video_id, exhibitor_name, video_title FROM `tbl_exhibitor_videos_views`, tbl_exhibitor_videos, tbl_exhibitors where tbl_exhibitor_videos_views.video_id=tbl_exhibitor_videos.video_id AND user_id = ? and tbl_exhibitor_videos.exhibitor_id=tbl_exhibitors.exhibitor_id";
        $paramType = 's'; //ssss';
        $paramValue = array(
            $userid,
        );

        $videos = $this->ds->select($query, $paramType, $paramValue);

        return $videos;
    }

    public function resendVer($userid)
    {

        $name = $this->getMemberName($userid);
        $email = $this->getMemberEmail($userid);

        $token = bin2hex(random_bytes(50)); // generate unique token
        $token_exp_date  = date('Y/m/d H:i:s', time() + 60 * 60 * 24);
        $verified = 0;

        $query = "UPDATE tbl_users set token = ?, token_expire=?, verified=? where userid=?";
        $paramType = 'ssis';
        $paramValue = array(
            $token,
            $token_exp_date,
            $verified,
            $userid
        );

        $this->ds->execute($query, $paramType, $paramValue);


        require 'sendemail.php';
        //send verification email
        $reglink = "https://coact.live/BOOT/verify.php?i=" . $userid . "&t=" . $token;
        $siteurl = 'https://coact.live/BOOT/';

        $message = file_get_contents('https://coact.live/BOOT/email_templates/reg-email.html');
        $message = str_replace('%name%', $name, $message);
        $message = str_replace('%reglink%', $reglink, $message);
        $message = str_replace('%siteurl%', $siteurl, $message);

        $mail = new SendEmail();
        $mail->to = $email;
        $mail->name = $name;
        $mail->subject = 'Verify your Email-Id for BOOT International Live';
        //$mail->message = $message;

        $mail->sendTemplate($name, $reglink, $siteurl);

        $response = array("status" => "success", "message" => "Verification mail sent.");


        return $response;
    }

    public function userVerified($verified, $userid)
    {
        $today_date   = date('Y/m/d H:i:s');

        $query = "update tbl_users set token_expire = ?, verified = ? where userid = ?";
        $paramType = 'sis';
        $paramValue = array(
            $today_date,
            $verified,
            $userid
        );

        $this->ds->execute($query, $paramType, $paramValue);
    }

    public function userLogout($userid)
    {
        $logout_date   = date('Y/m/d H:i:s');

        $query = "UPDATE tbl_users set logout_date=? where userid=?";
        $paramType = 'ss';
        $paramValue = array(
            $logout_date,
            $userid
        );

        $this->ds->execute($query, $paramType, $paramValue);

        $query = "UPDATE tbl_user_logins set leave_time=? where user_id=? and leave_time>=?";
        $paramType = 'sss';
        $paramValue = array(
            $logout_date,
            $userid,
            $logout_date
        );

        $this->ds->execute($query, $paramType, $paramValue);

        session_destroy();
        header("location: ./");
    }

    public function sendMsgTeam($to, $from, $msg, $src)
    {
        $chat_time   = date('Y/m/d H:i:s');
        $src = substr($src, 0, -4);
        $src = str_replace('_', ' ', $src);
        $src = ucwords(str_replace('-', ' ', $src));
        $query = "insert into tbl_team_chat(user_id_from, user_id_to, message, chat_time, source) values(?, ?, ?, ?, ?)";
        $paramType = 'sssss';
        $paramValue = array(
            $from,
            $to,
            $msg,
            $chat_time,
            $src
        );

        $this->ds->execute($query, $paramType, $paramValue);
    }

    public function getTeamChatHistory($to, $from)
    {
        $query = "select first_name, last_name, message, user_id_from, chat_time from tbl_team_chat,tbl_users where ((user_id_from=? AND user_id_to =?) OR (user_id_from=? AND user_id_to=?)) AND (tbl_team_chat.user_id_from = tbl_users.userid OR tbl_team_chat.user_id_to = tbl_users.userid) order by chat_time asc";
        $paramType = 'ssss'; //ssss';
        $paramValue = array(
            $from,
            $to,
            $to,
            $from
        );

        $history = $this->ds->select($query, $paramType, $paramValue);

        return $history;
    }

    public function updateTeamReadStatus($user_from, $user_to)
    {
        $query = "update tbl_team_chat set read_status = '1' where user_id_to =? and user_id_from =?";
        $paramType = 'ss';
        $paramValue = array(
            $user_to,
            $user_from
        );
        $this->ds->execute($query, $paramType, $paramValue);
    }
}
