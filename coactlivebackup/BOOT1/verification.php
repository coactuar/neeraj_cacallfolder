<?php
require_once 'controls/config.php';
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $event_title; ?></title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/all.min.css">
<link rel="stylesheet" href="css/styles.css">

</head>

<body>

	<div class="container">
        <div class="row no-margin">
            <div class="col-12">
                <img src="img/reg-banner.jpg" class="img-fluid" alt=""/> 
            </div>
        </div>
        <div class="row bg-white color-grey">
            <div class="col-12 text-center">
                 <h3 class="reg-title">Verify your Email-ID</h3>
            </div>
        </div>
        <div class="row bg-white color-grey">
            <div class="col-12 col-md-8 offset-md-2">
                
                  <div id="registration-confirmation">
                      <div class="alert alert-success">
                      Verification Link has been emailed to you.</b><br>
                      Please check your email regarding instructions on how to login.
                      </div>
                      
                  <a href="./">Continue to Login</a> 
                  </div>
                
                 
            </div>
        </div>
        <div class="row bg-white">
            <div class="col-12">
                <img src="img/line-h.jpg" class="img-fluid" alt=""/> 
            </div>
        </div>
        <div class="row bg-white p-2">
            <div class="col-4 bor-right p-2 text-center">
                <img src="img/in-assoc.png" class="img-fluid bot-img" alt=""/>
            </div>
            <div class="col-4 p-2 text-center">
                <img src="img/sci-partner.png" class="img-fluid bot-img" alt=""/>
            </div>
          <div class="col-4 bor-left p-2 text-center color-grey">
                <img src="img/brought-by.png" class="img-fluid bot-img" alt=""/>
                <div class="visit">
                Visit us at <a href="https://www.integracehealth.com/about.html" class="link" target="_blank">https://www.integracehealth.com/about.html</a>
                </div>
          </div>
        </div>
        <!--<div class="row no-margin">
            <div class="col-12">
                <img src="img/reg-bottom-banner.png" class="img-fluid" alt=""/> 
            </div>
        </div>-->
        <!--<div class="row bg-white color-grey">
            <div class="col-4 offset-8 text-center">
                 Visit us at <a href="https://www.integracehealth.com/about.html" class="link" target="_blank">https://www.integracehealth.com/about.html</a>
            </div>
        </div>-->
        <!--<div class="row bg-white color-grey">
              <div class="col-12 p-2 text-center">
            Visit us at <a href="https://www.integracehealth.com/about.html" class="link" target="_blank">https://www.integracehealth.com/about.html</a> </div>
          </div>-->
        
	</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<!--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-20"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-20');
</script>
-->
</body>
</html>