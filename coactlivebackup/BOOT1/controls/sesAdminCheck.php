<?php
    require_once "config.php";

    if(!isset($_SESSION["admin_user"]))
	{
        //session_destroy();
		header("location: ./");
		exit;
	}
    if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            unset($_SESSION['admin_user']);
            header("location: ./");
            exit;
        }

    }
?>