<?php
require_once "../controls/config.php";
require_once "../functions.php";

if (isset($_POST['action']) && !empty($_POST['action'])) {

  $action = $_POST['action'];

  switch ($action) {


    case 'getusers':

      if (isset($_POST["page"])) {
        $page  = $_POST["page"];
      } else {
        $page = 1;
      }

      $start_from = ($page - 1) * $limit;
      $today = date('Y/m/d H:i:s');
      $loggedin = 0;


      $sql = "SELECT COUNT(id) as count FROM tbl_users";
      $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link));
      $row = mysqli_fetch_assoc($rs_result);
      $total_records = $row['count'];
      $total_pages = ceil($total_records / $limit);
?>

      <div class="row">
        <div class="col-6"> Total Users: <?php echo $total_records; ?> </div>
        <!--<div class="col-6">
                    Currently Logged In: <?php echo $loggedin; ?>
                </div>-->
      </div>
      <div class="row">
        <div class="col-12">
          <table class="table table-dark table-striped">
            <thead class="thead-inverse">
              <tr>
                <th>Name</th>
                <th>Info</th>
                <th>Topic of Interests</th>
                <th>Mobile ISP</th>
                <th>Updates Requested</th>
                <th>Registered On</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php
              $query = "select tbl_users.id, tbl_users.first_name, tbl_users.last_name, tbl_users.emailid, tbl_users.phone_num, countries.name as country, states.name as state, cities.name as city, tbl_users.topic_interest, tbl_users.updates, tbl_users.isp, tbl_users.reg_date from tbl_users, cities, states, countries where tbl_users.city=cities.id AND tbl_users.state=states.id AND tbl_users.country=countries.id LIMIT $start_from, $limit";
              $res = mysqli_query($link, $query) or die(mysqli_error($link));
              $loggedin = 0;
              while ($data = mysqli_fetch_assoc($res)) {
                $logout = 0;
              ?>
                <tr>
                  <td><?php echo $data['first_name'] . ' ' . $data['last_name']; ?></td>
                  <td><?php echo $data['emailid'] . '<br>' . $data['phone_num'] . '<br>' . $data['city'] . ',' . $data['state'] . '<br>' . $data['country']; ?></td>
                  <td><?php
                      $topics = $data['topic_interest'];
                      $topic = explode(',', $topics);
                      foreach ($topic as $t) {
                        switch ($t) {
                          case 'upper-trauma':
                            echo 'Upper Extremity Trauma' . '<br>';
                            break;
                          case 'pelvic-hip':
                            echo 'Pelvic Trauma &amp; Hip Fractures/Recon' . '<br>';
                            break;
                          case 'foot-ankle':
                            echo 'Foot &amp; Ankle Injuries and Spine Trauma' . '<br>';
                            break;
                          case 'knee-trauma':
                            echo 'Knee Trauma' . '<br>';
                            break;
                          case 'sports-injuries':
                            echo 'Sports Injuries, Arthroscopic Procedures' . '<br>';
                            break;
                          case 'presentations':
                            echo 'Interesting Case Presentations' . '<br>';
                            break;
                        }
                      }
                      ?></td>
                  <td><?php echo $data['isp']; ?></td>
                  <td><?php
                      $updates = $data['updates'];
                      $update = explode(',', $updates);
                      foreach ($update as $u) {
                        switch ($u) {
                          case 'program':
                            echo 'Program' . '<br>';
                            break;
                          case 'integrace':
                            echo 'Integrace Pvt. Ltd.' . '<br>';
                            break;
                        }
                      }
                      ?></td>
                  <td><?php
                      if ($data['reg_date'] != '') {
                        $date = date_create($data['reg_date']);
                        echo date_format($date, "M d, H:i a");
                      } else {
                        echo '-';
                      }
                      ?></td>
                  <!--                            <td><?php
                                                      if ($data['login_date'] != '') {
                                                        $date = date_create($data['login_date']);
                                                        echo date_format($date, "M d, H:i a");
                                                      } else {
                                                        echo '-';
                                                      }
                                                      ?>
                            </td>
                            <td><?php
                                $today = date("Y/m/d H:i:s");

                                $dateTimestamp1 = strtotime($data['logout_date']);
                                $dateTimestamp2 = strtotime($today);
                                //echo $row[5];
                                if ($dateTimestamp1 > $dateTimestamp2) {
                                  if ($data['current_room'] != '') {
                                    echo 'Inside ' . $data['current_room'];
                                  } else {
                                    echo '<i>still logged in</i>';
                                  }
                                  //$loggedin += 1; 
                                } else {
                                  if ($data['logout_date'] != '') {
                                    $date = date_create($data['logout_date']);
                                    echo 'Logged out on <br>' . date_format($date, "M d, H:i a");
                                  } else {
                                    echo '-';
                                  }
                                  $logout = 1;
                                }
                                ?>
                            </td>
-->
                  <!--<td>
                                <select class="input" id="active<?php echo $data['id']; ?>" onChange="updActive('<?php echo $data['id']; ?>')">
                                    <option value="0" <?php if ($data['active'] == '0') echo 'selected'; ?>>Not Active</option>
                                    <option value="1" <?php if ($data['active'] == '1') echo 'selected'; ?>>Active</option>
                                </select>
                            </td>-->
                  <td><a class="btn-delete" href="#" onClick="delUser('<?php echo $data['id']; ?>')"><i class="far fa-trash-alt"></i></a></td>
                </tr>
              <?php
              }
              ?>
          </table>
        </div>
      </div>
      <nav>
        <ul class="pagination pagination-sm" id="pagination">
          <?php if (!empty($total_pages)) : for ($i = 1; $i <= $total_pages; $i++) :
              if ($i == 1) : ?>
                <li onClick="update(<?php echo $i; ?>)" class="page-item <?php if ($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i; ?>"> <a class="page-link" href="#"><?php echo $i; ?></a> </li>
              <?php else : ?>
                <li onClick="update(<?php echo $i; ?>)" class="page-item <?php if ($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i; ?>"> <a class="page-link" href="#"><?php echo $i; ?></a> </li>
              <?php endif; ?>
          <?php endfor;
          endif; ?>
        </ul>
      </nav>
    <?php


      break;

    case 'logoutuser':
      $logout_date  = date('Y/m/d H:i:s', time());
      $sql = "update tbl_users set logout_date='$logout_date' where id='" . $_POST['userid'] . "'";
      $rs_result = mysqli_query($link, $sql);
      break;
    case 'upduserverify':
      $sql = "update tbl_users set verified='" . $_POST['ver'] . "' where id='" . $_POST['userid'] . "'";
      $rs_result = mysqli_query($link, $sql);
      echo 'succ';
      break;
    case 'upduseractive':
      $sql = "update tbl_users set active='" . $_POST['act'] . "' where id='" . $_POST['userid'] . "'";
      $rs_result = mysqli_query($link, $sql);
      echo 'succ';
      break;
    case 'deluser':
      $userId = $_POST['userId'];

      $sql = "delete from tbl_users where id = '$userId'";
      $rs_result = mysqli_query($link, $sql);
      //echo $sql;
      echo "succ";

      break;

    case 'getsessions':
    ?>
      <form>
        <div class="row">
          <div class="col-12">
            <table class="table table-striped table-dark">
              <thead class="thead-inverse">
                <tr>
                  <th width="150">Start Time</th>
                  <th width="150">Auditorium</th>
                  <th>Session Title</th>
                  <th width="150">Live Status</th>
                  <th width="150">Enable Entry</th>
                  <th width="200">Session Status</th>
                  <th width="100">Show</th>
                  <th width="110"></th>
                </tr>
              </thead>
              <tbody>
                <?php
                $query = "select tbl_sessions.session_id, start_time, session_title, live_status, launch_status, session_status, show_session, audi_name from tbl_sessions, tbl_auditoriums where tbl_sessions.audi_id=tbl_auditoriums.audi_id order by live_status desc, start_time asc";
                $res = mysqli_query($link, $query) or die(mysqli_error($link));
                while ($data = mysqli_fetch_assoc($res)) {
                ?>
                  <tr>
                    <td><?php
                        $date = date_create($data['start_time']);
                        echo date_format($date, "M d, H:i a"); ?></td>
                    <td><b><?php echo $data['audi_name']; ?></b></td>
                    <td><b><?php echo $data['session_title']; ?></b></td>
                    <td><a href="#" onClick="updateLiveStatus('<?php echo $data['session_id']; ?>','<?php echo $data['live_status']; ?>')" class="btn btn-sm <?php if ($data['live_status'] == '1') {
                                                                                                                                                                echo 'btn-danger';
                                                                                                                                                              } else echo 'btn-success';  ?>">
                        <?php if ($data['live_status'] == '1') {
                          echo 'Unmark';
                        } else echo 'Mark';  ?>
                        Live</a></td>
                    <td><select class="input" id="entry<?php echo $data['session_id']; ?>" onChange="updEntry('<?php echo $data['session_id']; ?>')">
                        <option value="0" <?php if ($data['launch_status'] == '0') echo 'selected'; ?>>No</option>
                        <option value="1" <?php if ($data['launch_status'] == '1') echo 'selected'; ?>>Yes</option>
                      </select></td>
                    <td><select class="input" id="session<?php echo $data['session_id']; ?>" onChange="updSession('<?php echo $data['session_id']; ?>')">
                        <option value="yet" <?php if ($data['session_status'] == 'yet') echo 'selected'; ?>>Yet to Start</option>
                        <option value="next" <?php if ($data['session_status'] == 'next') echo 'selected'; ?>>Coming Up Next</option>
                        <option value="live" <?php if ($data['session_status'] == 'live') echo 'selected'; ?>>LIVE Now</option>
                        <option value="over" <?php if ($data['session_status'] == 'over') echo 'selected'; ?>>Completed</option>
                      </select></td>
                    <td><select class="input" id="show<?php echo $data['session_id']; ?>" onChange="updShow('<?php echo $data['session_id']; ?>')">
                        <option value="0" <?php if ($data['show_session'] == '0') echo 'selected'; ?>>No</option>
                        <option value="1" <?php if ($data['show_session'] == '1') echo 'selected'; ?>>Yes</option>
                      </select></td>
                    <td><a class="btn-edit" href="editsession.php?ses=<?php echo $data['session_id']; ?>"><i class="far fa-edit"></i></a> <a class="btn-delete" href="#" onClick="delSes('<?php echo $data['session_id']; ?>')"><i class="far fa-trash-alt"></i></a></td>
                  </tr>
                <?php
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </form>
    <?php


      break;

    case 'updatelivestatus':

      $newval = !$_POST['val'];

      $sql = "select * from tbl_sessions where session_id = '" . $_POST['sesid'] . "'";
      $rs_result = mysqli_query($link, $sql);
      $data = mysqli_fetch_assoc($rs_result);
      $audi_id = $data['audi_id'];

      $sql = "Update tbl_sessions set live_status ='0' where audi_id='" . $audi_id . "'";
      $rs_result = mysqli_query($link, $sql);

      if ($newval) {
        $sql = "Update tbl_sessions set live_status ='$newval' where session_id = '" . $_POST['sesid'] . "' and audi_id='" . $audi_id . "'";
        $rs_result = mysqli_query($link, $sql);
      }


      break;

    case 'updateentrystatus':
      $newval = $_POST['val'];
      $sesId = $_POST['sesId'];

      $ses = new Session();
      $ses->updateEntryStatus($sesId, $newval);
      //$sql = "Update tbl_sessions set launch_status ='$newval' where id = '$sesId'";  
      //$rs_result = mysqli_query($link,$sql);  
      //echo $sql;
      echo "succ";

      break;

    case 'updatesessionstatus':
      $newval = $_POST['val'];
      $sesId = $_POST['sesId'];

      $ses = new Session();
      $ses->updateSessionStatus($sesId, $newval);
      //$sql = "Update tbl_sessions set session_status ='$newval' where id = '$sesId'";  
      //$rs_result = mysqli_query($link,$sql);  
      //echo $sql;
      echo "succ";

      break;

    case 'updateshowstatus':
      $newval = $_POST['val'];
      $sesId = $_POST['sesId'];

      $ses = new Session();
      $ses->updateShowStatus($sesId, $newval);
      //echo $sql;
      echo "succ";

      break;

    case 'delsession':
      $sesId = $_POST['sesId'];

      $ses = new Session();
      $ses->delSession($sesId);
      //$sql = "delete from tbl_sessions where session_id = '$sesId'";  
      //$rs_result = mysqli_query($link,$sql);  
      //echo $sql;
      echo "succ";

      break;

    case 'getexhibitors':
    ?>
      <form>
        <div class="row">
          <div class="col-12">
            <table class="table table-striped table-dark">
              <thead class="thead-inverse">
                <tr>
                  <!--<th width="120">Logo</th>-->
                  <th width="200">Exhibitor</th>
                  <th width="200">Exhibitor ID</th>
                  <th></th>
                  <th width="110"></th>
                  <th width="110"></th>
                </tr>
              </thead>
              <tbody>
                <?php
                $query = "select * from tbl_exhibitors order by active desc, exhibitor_name asc";
                $res = mysqli_query($link, $query) or die(mysqli_error($link));
                while ($data = mysqli_fetch_assoc($res)) {
                ?>
                  <tr>

                    <td><b><?php echo $data['exhibitor_name']; ?></b></td>
                    <td><?php echo $data['exhibitor_id']; ?></td>
                    <td align="center"><a href="managers.php?h=<?php echo $data['exhibitor_id']; ?>" class="btn btn-sm btn-primary">Managers</a> <a href="videos.php?h=<?php echo $data['exhibitor_id']; ?>" class="btn btn-sm btn-secondary">Videos</a> <a href="resources.php?h=<?php echo $data['exhibitor_id']; ?>" class="btn btn-sm btn-warning">Resources</a> <a href="visitors.php?h=<?php echo $data['exhibitor_id']; ?>" class="btn btn-sm btn-danger">Visitors</a> </td>
                    <td><select class="input" id="active<?php echo $data['id']; ?>" onChange="updActive('<?php echo $data['id']; ?>')">
                        <option value="0" <?php if ($data['active'] == '0') echo 'selected'; ?>>No</option>
                        <option value="1" <?php if ($data['active'] == '1') echo 'selected'; ?>>Yes</option>
                      </select>
                    </td>
                    <td><a class="btn-edit" href="editexhibitor.php?exh=<?php echo $data['exhibitor_id']; ?>"><i class="far fa-edit"></i></a> <a class="btn-delete" href="#" onClick="delExhib('<?php echo $data['exhibitor_id']; ?>')"><i class="far fa-trash-alt"></i></a></td>
                  </tr>
                <?php
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </form>
    <?php


      break;

    case 'updateexhibactive':
      $newval = $_POST['val'];
      $exhId = $_POST['exhId'];

      $sql = "Update tbl_exhibitor_logins set active ='$newval' where id = '$exhId'";
      $rs_result = mysqli_query($link, $sql);
      //echo $sql;
      echo "succ";

      break;

    case 'delexhib':
      $exhId = $_POST['exhId'];

      $sql = "delete from tbl_exhibitor_visitors where exhibit_hall_id in (select exhibit_hall_id from tbl_exhibitor_logins where id = '$exhId')";
      $rs_result = mysqli_query($link, $sql);

      $sql = "delete from tbl_exhibitors where exhibitor_id = '$exhId'";
      $rs_result = mysqli_query($link, $sql);

      $sql = "delete from tbl_exhibitor_logins where id = '$exhId'";
      $rs_result = mysqli_query($link, $sql);
      //echo $sql;
      echo "succ";

      break;

    case 'getresources':

      if (isset($_POST["page"])) {
        $page  = $_POST["page"];
      } else {
        $page = 1;
      }

      $start_from = ($page - 1) * $limit;

      $sql = "SELECT COUNT(id) FROM tbl_exhibitor_resources";
      $rs_result = mysqli_query($link, $sql);
      $row = mysqli_fetch_row($rs_result);
      $total_records = $row[0];
      $total_pages = ceil($total_records / $limit);
    ?>
      <div class="row">
        <div class="col-12"> Total Resources:
          <div class="res_count"><?php echo $total_records; ?></div>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <table class="table table-striped table-dark">
            <thead class="thead-inverse">
              <tr>
                <th>Exhibitor</th>
                <th>Resource Name</th>
                <th width="110"></th>
              </tr>
            </thead>
            <tbody>
              <?php
              $query = "select tbl_exhibitor_resources.id,exhibitor_name, resource_title, download_count, tbl_exhibitor_resources.active from tbl_exhibitor_resources, tbl_exhibitor_logins where tbl_exhibitor_resources.exhib_hall_id = tbl_exhibitor_logins.exhibit_hall_id order by resource_title asc LIMIT $start_from, $limit";
              $res = mysqli_query($link, $query) or die(mysqli_error($link));

              while ($data = mysqli_fetch_assoc($res)) {
              ?>
                <tr>
                  <td><?php echo $data['exhibitor_name']; ?></td>
                  <td><?php
                      echo $data['resource_title'];
                      echo '<br><b>Downloads:</b> ' . $data['download_count'];
                      ?></td>
                  <td><a class="btn-edit" href="editexhibitor.php?exh=<?php echo $data['id']; ?>"><i class="far fa-edit"></i></a> <a class="btn-delete" href="#" onClick="delExhib('<?php echo $data['id']; ?>')"><i class="far fa-trash-alt"></i></a></td>
                </tr>
              <?php
              }
              ?>
          </table>
        </div>
      </div>
      <nav>
        <ul class="pagination pagination-sm" id="pagination">
          <?php if (!empty($total_pages)) : for ($i = 1; $i <= $total_pages; $i++) :
              if ($i == 1) : ?>
                <li onClick="update(<?php echo $i; ?>)" class="page-item <?php if ($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i; ?>"> <a class="page-link" href="#"><?php echo $i; ?></a> </li>
              <?php else : ?>
                <li onClick="update(<?php echo $i; ?>)" class="page-item <?php if ($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i; ?>"> <a class="page-link" href="#"><?php echo $i; ?></a> </li>
              <?php endif; ?>
          <?php endfor;
          endif; ?>
        </ul>
      </nav>
    <?php


      break;

    case 'getvideos':

      if (isset($_POST["page"])) {
        $page  = $_POST["page"];
      } else {
        $page = 1;
      }

      $start_from = ($page - 1) * $limit;

      $sql = "SELECT COUNT(id) FROM tbl_exhibitor_videos";
      $rs_result = mysqli_query($link, $sql);
      $row = mysqli_fetch_row($rs_result);
      $total_records = $row[0];
      $total_pages = ceil($total_records / $limit);
    ?>
      <div class="row">
        <div class="col-12"> Total Videos:
          <div class="res_count"><?php echo $total_records; ?></div>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <table class="table table-striped table-dark">
            <thead class="thead-inverse">
              <tr>
                <th>Exhibitor</th>
                <th>Video Title</th>
                <th width="110"></th>
              </tr>
            </thead>
            <tbody>
              <?php
              $query = "select tbl_exhibitor_videos.id, exhibitor_name, video_title, youtube_id, tbl_exhibitor_videos.active from tbl_exhibitor_videos, tbl_exhibitor_logins where tbl_exhibitor_videos.exhib_hall_id=tbl_exhibitor_logins.exhibit_hall_id order by video_title asc LIMIT $start_from, $limit";
              $res = mysqli_query($link, $query) or die(mysqli_error($link));
              while ($data = mysqli_fetch_assoc($res)) {
              ?>
                <tr>
                  <td><?php echo $data['exhibitor_name']; ?></td>
                  <td><?php echo $data['video_title'];
                      echo '<br><b>YouTube ID:</b> ' . $data['youtube_id'];
                      ?></td>
                  <td><a class="btn-edit" href="editvideo.php?exh=<?php echo $data['id']; ?>"><i class="far fa-edit"></i></a> <a class="btn-delete" href="#" onClick="delVideo('<?php echo $data['id']; ?>')"><i class="far fa-trash-alt"></i></a></td>
                </tr>
              <?php
              }
              ?>
          </table>
        </div>
      </div>
      <nav>
        <ul class="pagination pagination-sm" id="pagination">
          <?php if (!empty($total_pages)) : for ($i = 1; $i <= $total_pages; $i++) :
              if ($i == 1) : ?>
                <li onClick="updateVideo(<?php echo $i; ?>)" class="page-item <?php if ($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i; ?>"> <a class="page-link" href="#"><?php echo $i; ?></a> </li>
              <?php else : ?>
                <li onClick="updateVideo(<?php echo $i; ?>)" class="page-item <?php if ($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i; ?>"> <a class="page-link" href="#"><?php echo $i; ?></a> </li>
              <?php endif; ?>
          <?php endfor;
          endif; ?>
        </ul>
      </nav>
    <?php


      break;

    case 'getfeedbacks':

      if (isset($_POST["page"])) {
        $page  = $_POST["page"];
      } else {
        $page = 1;
      }

      $start_from = ($page - 1) * $limit;

      $sql = "SELECT COUNT(id) FROM tbl_conf_feedback";
      $rs_result = mysqli_query($link, $sql);
      $row = mysqli_fetch_row($rs_result);
      $total_records = $row[0];
      $total_pages = ceil($total_records / $limit);
    ?>
      <div class="row">
        <div class="col-12">
          <table class="table table-striped table-dark">
            <thead class="thead-inverse">
              <tr>
                <th width="150">Name</th>
                <th width="200">Email ID</th>
                <th width="150">Experience Rating</th>
                <th>Comments/Suggestion</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $query = "select * from tbl_conf_feedback order by feedback_time desc LIMIT $start_from, $limit";
              $res = mysqli_query($link, $query) or die(mysqli_error($link));
              while ($data = mysqli_fetch_assoc($res)) {
              ?>
                <tr>
                  <td><?php echo $data['name']; ?></td>
                  <td><?php echo $data['emailid']; ?></td>
                  <td><?php echo $data['exp_rating']; ?></td>
                  <td><?php echo nl2br($data['comments']); ?></td>
                </tr>
              <?php
              }
              ?>
          </table>
        </div>
      </div>
      <nav>
        <ul class="pagination pagination-sm" id="pagination">
          <?php if (!empty($total_pages)) : for ($i = 1; $i <= $total_pages; $i++) :
              if ($i == 1) : ?>
                <li onClick="updateVideo(<?php echo $i; ?>)" class="page-item <?php if ($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i; ?>"> <a class="page-link" href="#"><?php echo $i; ?></a> </li>
              <?php else : ?>
                <li onClick="updateVideo(<?php echo $i; ?>)" class="page-item <?php if ($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i; ?>"> <a class="page-link" href="#"><?php echo $i; ?></a> </li>
              <?php endif; ?>
          <?php endfor;
          endif; ?>
        </ul>
      </nav>
    <?php


      break;

    case 'getvisitors':

      if (isset($_POST["page"])) {
        $page  = $_POST["page"];
      } else {
        $page = 1;
      }

      $start_from = ($page - 1) * $limit;

      $exhib_hall_id = $_POST['exhib_hall_id'];

      $sql = "SELECT COUNT(*) as count FROM tbl_exhibitor_visitors where exhibit_hall_id = '$exhib_hall_id'";
      $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link));
      $row = mysqli_fetch_assoc($rs_result);
      $total_visits = $row['count'];
      //echo $sql;

      $sql = "SELECT COUNT(DISTINCT attendee_id) as count FROM tbl_exhibitor_visitors where exhibit_hall_id  = '$exhib_hall_id'";
      $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link));
      $row = mysqli_fetch_assoc($rs_result);
      $total_visitors = $row['count'];
      // echo $sql;
      $total_pages = ceil($total_visitors / $limit);
    ?>
      <div class="row">
        <div class="col-6"> Total Visitors: <?php echo $total_visitors; ?> </div>
        <div class="col-6"> Total Visits: <?php echo $total_visits; ?> </div>
      </div>
      <div class="row">
        <div class="col-12">
          <table class="table table-dark table-striped">
            <thead class="thead-inverse">
              <tr>
                <th>Name</th>
                <th>Contact </th>
                <th>Company</th>
                <th width="200">Last Visit Time</th>
                <th width="150">No. of Visits</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $query = "select DISTINCT attendee_id from tbl_exhibitor_visitors where exhibit_hall_id = '$exhib_hall_id' LIMIT $start_from, $limit";
              $res = mysqli_query($link, $query) or die(mysqli_error($link));
              //echo $query;
              $loggedin = 0;
              while ($a = mysqli_fetch_assoc($res)) {
                $u = "select * from tbl_exhibitor_visitors, tbl_users where tbl_exhibitor_visitors.attendee_id = tbl_users.id AND attendee_id='" . $a['attendee_id'] . "' order by entry_time desc limit 1";
                //echo $u;
                $r = mysqli_query($link, $u);
                $data = mysqli_fetch_assoc($r);
              ?>
                <tr>
                  <td><?php echo $data['first_name'] . ' ' . $data['last_name']; ?></td>
                  <td><?php echo $data['emailid'] . '<br>' . $data['phone_num']; ?></td>
                  <td><?php echo $data['designation'] . ',<br>' . $data['company']; ?></td>
                  <td><?php
                      if ($data['entry_time'] != '') {
                        $date = date_create($data['entry_time']);
                        echo date_format($date, "M d, H:i a");
                      } else {
                        echo '-';
                      }
                      ?></td>
                  <td><?php
                      $c = "select count(*) as count from tbl_exhibitor_visitors where attendee_id='" . $data['attendee_id'] . "' and exhibit_hall_id ='$exhib_hall_id'";
                      $r = mysqli_query($link, $c);
                      $d = mysqli_fetch_assoc($r);
                      echo $d['count'];
                      ?></td>
                </tr>
              <?php
              }
              ?>
          </table>
        </div>
      </div>
      <nav>
        <ul class="pagination pagination-sm" id="pagination">
          <?php if (!empty($total_pages)) : for ($i = 1; $i <= $total_pages; $i++) :
              if ($i == 1) : ?>
                <li onClick="update(<?php echo $i; ?>)" class="page-item <?php if ($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i; ?>"> <a class="page-link" href="#"><?php echo $i; ?></a> </li>
              <?php else : ?>
                <li onClick="update(<?php echo $i; ?>)" class="page-item <?php if ($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i; ?>"> <a class="page-link" href="#"><?php echo $i; ?></a> </li>
              <?php endif; ?>
          <?php endfor;
          endif; ?>
        </ul>
      </nav>
    <?php


      break;

    case 'getpolls':


      $sql = "SELECT COUNT(id) FROM tbl_polls";
      $rs_result = mysqli_query($link, $sql);
      $row = mysqli_fetch_row($rs_result);
      $total_records = $row[0];
    ?>
      <div class="row user-info">
        <div class="col-6"> Total Poll Questions:
          <div id="ques_count" style="display:inline-block"><?php echo $total_records; ?></div>
        </div>
      </div>
      <div class="row user-details">
        <div class="col-12">
          <table class="table table-striped">
            <thead class="thead-inverse">
              <tr>
                <th>Poll Question</th>
                <th>Session</th>
                <th width="200" align="center"></th>
                <th width="100" align="center"></th>
              </tr>
            </thead>
            <tbody>
              <?php
              $query = "select tbl_polls.poll_id,tbl_polls.session_id, poll_question, session_title, active, poll_over, show_results from tbl_polls, tbl_sessions where tbl_polls.session_id=tbl_sessions.session_id";
              $res = mysqli_query($link, $query) or die(mysqli_error($link));
              while ($data = mysqli_fetch_assoc($res)) {
              ?>
                <tr>
                  <td><?php echo $data['poll_question']; ?></td>
                  <td><?php echo $data['session_title']; ?></td>
                  <td width="100"><?php
                                  if (!$data['poll_over']) {
                                  ?>
                      <a href="#" onClick="updatePoll('<?php echo $data['poll_id']; ?>','<?php echo $data['session_id']; ?>','<?php echo $data['active']; ?>')" class="btn btn-sm <?php if ($data['active'] == '1') {
                                                                                                                                                                                    echo 'btn-danger';
                                                                                                                                                                                  } else echo 'btn-success';  ?> mb-1"><?php if ($data['active'] == '1') {
                                                                                                                                                                                                                          echo 'Deactivate';
                                                                                                                                                                                                                        } else echo 'Activate';  ?> Poll</a>
                      <a href="pollresults.php?id=<?php echo $data['poll_id']; ?>" class="btn btn-sm btn-success mb-1">View Results</a>
                      <a href="#" onClick="updatePollRes('<?php echo $data['poll_id']; ?>','<?php echo $data['session_id']; ?>','<?php echo $data['show_results']; ?>')" class="btn btn-sm <?php if ($data['show_results'] == '1') {
                                                                                                                                                                                              echo 'btn-danger';
                                                                                                                                                                                            } else echo 'btn-success';  ?> mb-1"><?php if ($data['show_results'] == '1') {
                                                                                                                                                                                                                                    echo 'Hide';
                                                                                                                                                                                                                                  } else echo 'Show';  ?> Results</a>
                      <a href="#" onClick="endPoll('<?php echo $data['poll_id']; ?>','1')" class="btn btn-sm btn-danger mb-1">End Poll</a>
                    <?php
                                  } else {
                    ?>
                      <a href="#" onClick="endPoll('<?php echo $data['poll_id']; ?>','0')" class="btn btn-sm btn-success mb-1">Enable Poll</a>
                    <?php
                                  }

                    ?></td>
                  <td width="100"><a class="btn-edit" href="editpoll.php?p=<?php echo $data['poll_id']; ?>"><i class="far fa-edit"></i></a> <a class="btn-delete" href="#" onClick="delPoll('<?php echo $data['poll_id']; ?>')"><i class="far fa-trash-alt"></i></a></td>
                </tr>
              <?php
              }
              ?>
          </table>
        </div>
      </div>
    <?php


      break;

    case 'endpoll':
      $pollid = $_POST['pollId'];
      $val = $_POST['val'];

      $poll = new Poll();
      $poll->endPoll($pollid, $val);

      break;

    case 'delpoll':
      $pollid = $_POST['poll'];

      $poll = new Poll();
      $poll->delPoll($pollid);

      break;

    case 'updatepoll':
      $pollid = $_POST['pollId'];
      $sessid = $_POST['sessId'];
      $val = !$_POST['val'];

      $poll = new Poll();
      $poll->updatePoll($pollid, $sessid, $val);

      break;

    case 'updatepollres':
      $pollid = $_POST['pollId'];
      $sessid = $_POST['sessId'];
      $val = !$_POST['val'];

      $poll = new Poll();
      $poll->updatePollRes($pollid, $sessid, $val);

      break;

    case 'getpollresults':

      $poll_id = $_POST['pid'];

      $poll = new Poll();

      $pollAnsCount = $poll->getAnsCount($poll_id);
      //            echo $pollAnsCount;
      $total_count = $pollAnsCount;

      $curr_poll = $poll->getPoll($poll_id);
      //var_dump( $curr_poll);
      $poll_ques = $curr_poll[0]['poll_question'];
      $poll_opt1 = $curr_poll[0]['poll_opt1'];
      $poll_opt2 = $curr_poll[0]['poll_opt2'];
      $poll_opt3 = $curr_poll[0]['poll_opt3'];
      $poll_opt4 = $curr_poll[0]['poll_opt4'];
      $corrans = $curr_poll[0]['correct_ans'];

      echo "<h6><b>Question: </b>" . $poll_ques . "</h6>";

      $opt1Res = $poll->getOptAnsCount($poll_id, 'opt1');
      $opt1_count = $opt1Res;
      $opt1width = 0;
      if ($total_count != '0') {
        $opt1width = ($opt1_count / $total_count) * 100;
      }
      $ans = '';
      if ($corrans == 'opt1') {
        $ans = 'corrans';
      }
    ?>
      <div class="option"> <strong><?php echo $poll_opt1; ?></strong><span class="float-right badge badge-success"><?php echo $opt1width . '%'; ?></span>
        <div class="progress <?php echo $ans; ?>">
          <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: <?php echo $opt1width; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
      </div>
      <?php
      $opt2Res = $poll->getOptAnsCount($poll_id, 'opt2');
      $opt2_count = $opt2Res;
      $opt2width = 0;
      if ($total_count != '0') {
        $opt2width = ($opt2_count / $total_count) * 100;
      }
      $ans = '';
      if ($corrans == 'opt2') {
        $ans = 'corrans';
      }
      ?>
      <div class="option"> <strong><?php echo $poll_opt2; ?></strong><span class="float-right  badge badge-primary"><?php echo $opt2width . '%'; ?></span>
        <div class="progress <?php echo $ans; ?>">
          <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" style="width: <?php echo $opt2width; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
      </div>
      <?php
      if ($poll_opt3 != '') {
        $opt3Res = $poll->getOptAnsCount($poll_id, 'opt3');
        $opt3_count = $opt3Res;
        $opt3width = 0;
        if ($total_count != '0') {
          $opt3width = ($opt3_count / $total_count) * 100;
        }
        $ans = '';
        if ($corrans == 'opt3') {
          $ans = 'corrans';
        }
      ?>
        <div class="option"> <strong><?php echo $poll_opt3; ?></strong><span class="float-right badge badge-warning"><?php echo $opt3width . '%'; ?></span>
          <div class="progress <?php echo $ans; ?>">
            <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" style="width: <?php echo $opt3width; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
        </div>
      <?php
      }
      if ($poll_opt4 != '') {
        $opt4Res = $poll->getOptAnsCount($poll_id, 'opt4');
        $opt4_count = $opt4Res;
        $opt4width = 0;
        if ($total_count != '0') {
          $opt4width = ($opt4_count / $total_count) * 100;
        }
        $ans = '';
        if ($corrans == 'opt4') {
          $ans = 'corrans';
        }
      ?>
        <div class="option"> <strong><?php echo $poll_opt4; ?></strong><span class="float-right badge badge-danger"><?php echo $opt4width . '%'; ?></span>
          <div class="progress <?php echo $ans; ?>">
            <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" style="width: <?php echo $opt4width; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
        </div>
<?php
      }








      break;
  }
}


?>