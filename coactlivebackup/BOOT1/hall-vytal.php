<?php
require_once "controls/sesCheck.php";
require_once "functions.php";

$exhibit_hall_id = 'vytal';
$exhibitor_id = 'ef614ed5b9b711550d535dcecf862de5fa845302f1ad0792';

$halls = new Hall();
$verify = $halls->verifyHallId($exhibitor_id);
if (empty($verify)) {
    header('location: exhibition-halls.php');
}
?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=1100">
    <title>Vytal Teleconnect :: <?php echo $event_title; ?></title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/sidebar.css">
    <link rel="stylesheet" href="css/overlays.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

</head>

<body>
    <div id="preloader">
        <div class="preloader">
            <div class="sk-circle1 sk-child"></div>
            <div class="sk-circle2 sk-child"></div>
            <div class="sk-circle3 sk-child"></div>
            <div class="sk-circle4 sk-child"></div>
            <div class="sk-circle5 sk-child"></div>
            <div class="sk-circle6 sk-child"></div>
            <div class="sk-circle7 sk-child"></div>
            <div class="sk-circle8 sk-child"></div>
            <div class="sk-circle9 sk-child"></div>
            <div class="sk-circle10 sk-child"></div>
            <div class="sk-circle11 sk-child"></div>
            <div class="sk-circle12 sk-child"></div>
        </div>
    </div>

    <div id="main-wrapper">
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">

                <div class="navbar-collapse">

                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <?php require_once "attendance.php" ?>
                        </li>
                    </ul>
                </div>

            </nav>
        </header>

        <div class="page-wrapper">
            <div id="main-area">
                <div id="background-image" class="exhibit-hall-godrej-bg" style="background-image:url(img/hall-vytal.jpg); background-position:center top; "></div>

                <div id="overlays">
                    <div class="overlay-item back-button">
                        <button class="btn btn-back" onclick="history.back()"><i class="fas fa-arrow-alt-circle-left"></i> Back</button>
                    </div>
                    <div class="overlay-item vytal-tv-area">
                        <div class="exhib-actions">
                            <a class="hall-video vid-view" data-vidid="131573cb346169acb23af6bbae038cb237f78309681cc7f5" href="https://coact.live/BOOT/video-vytal.php"></a>
                        </div>
                    </div>

                    <a class="overlay-item vytal-samples-area" href="#" data-exhid="<?php echo $exhibitor_id; ?>" data-userid="<?php echo $_SESSION['user_id']; ?>" id="subVideosReq"></a>
                    <a class="overlay-item vytal-products-area" href="#" data-exhid="<?php echo $exhibitor_id; ?>" data-userid="<?php echo $_SESSION['user_id']; ?>" id="subDemoReq"></a>
                    <a class="overlay-item vytal-literature-area" href="#" data-exhid="<?php echo $exhibitor_id; ?>" data-userid="<?php echo $_SESSION['user_id']; ?>" id="subAppReq"></a>
                    <a class="overlay-item vytal-brodl-area dl-brochure res-dl" data-docid="21cede3357a49c02791f2a5c1543defeb30a434e20830abd" href="resources/vytal-brochure.pdf" target="_blank" download data-userid="<?php echo $_SESSION['user_id']; ?>" id="dlBro"></a>
                    <div class="overlay-item vytal-banner01-area">
                        <div class="exhib-actions">
                            <a class="hall-banner" href="img/stall-banners/vytal-01.jpg"></a>
                        </div>
                    </div>
                    <div class="overlay-item vytal-banner02-area">
                        <div class="exhib-actions">
                            <a class="hall-banner" href="img/stall-banners/vytal-02.jpg"></a>
                        </div>
                    </div>

                </div>
                <?php require_once "bottom-navmenu.php" ?>





            </div>
        </div>

    </div>

    <?php require_once "commons.php" ?>
    <div id="req-videos">
        <div id="req-videos-msg">
            Hello, <?php echo $user_name; ?>!
            <br><br>
            <div class="succ-msg"> THANKS. <br>WE HAVE REGISTERED YOUR REQUEST FOR BOOT ARCHIVED VIDEOS!</div>
        </div>
    </div>
    <div id="req-call">
        <div id="req-call-msg">
            Hello, <?php echo $user_name; ?>!
            <br><br>
            <div class="succ-msg"> THANKS. <br>WE HAVE REGISTERED YOUR REQUEST FOR DEMO CALL FROM VYTAL!</div>
        </div>
    </div>
    <div id="req-telemedicine">
        <div id="req-telmed-msg">
            Hello, <?php echo $user_name; ?>!
            <br><br>
            <div class="succ-msg"> THANKS. <br>WE HAVE REGISTERED YOUR REQUEST FOR TELEMEDICINE APP!</div>
        </div>
    </div>


    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.popupoverlay.js"></script>
    <script type="text/javascript" src="lightbox/html5lightbox.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script data-loc="<?php echo $exhibitor_id; ?>" src="js/site.js?v=2"></script>
    <script data-hall="<?php echo $exhibitor_id; ?>" src="js/exhibit-hall.js"></script>
    <script data-to="<?php echo $_SESSION['user_id']; ?>" src="js/functions.js"></script>
    <script>
        $('#req-videos').dialog({
            dialogClass: "requests",
            autoOpen: false,
            title: 'Request For Videos',
            position: {
                my: "center center",
                at: "center center"
            }
        });

        $('#req-call').dialog({
            dialogClass: "requests",
            autoOpen: false,
            title: 'Request For Calls',
            position: {
                my: "center center",
                at: "center center"
            }
        });
        $('#req-telemedicine').dialog({
            dialogClass: "requests",
            autoOpen: false,
            title: 'Request For Telemedicine',
            position: {
                my: "center center",
                at: "center center"
            }
        });
        $('#subVideosReq').on('click', function() {
            var exh_id = $(this).data('exhid');
            var user_id = $(this).data('userid');
            $.ajax({
                url: 'controls/server.php',
                data: {
                    action: 'videoreq',
                    exhId: exh_id,
                    userId: user_id
                },
                type: 'post',
                success: function(output) {
                    console.log(output);
                    if (output > 0) {
                        $('#req-videos').dialog('open');
                    } else {
                        alert('Please try again!');
                    }
                }
            });
        });

        $('#subDemoReq').on('click', function() {
            var exh_id = $(this).data('exhid');
            var user_id = $(this).data('userid');
            $.ajax({
                url: 'controls/server.php',
                data: {
                    action: 'demoreq',
                    exhId: exh_id,
                    userId: user_id
                },
                type: 'post',
                success: function(output) {
                    console.log(output);
                    if (output > 0) {
                        $('#req-call').dialog('open');
                    } else {
                        alert('Please try again!');
                    }
                }
            });
        });

        $('#subAppReq').on('click', function() {
            var exh_id = $(this).data('exhid');
            var user_id = $(this).data('userid');
            $.ajax({
                url: 'controls/server.php',
                data: {
                    action: 'appreq',
                    exhId: exh_id,
                    userId: user_id
                },
                type: 'post',
                success: function(output) {
                    console.log(output);
                    if (output > 0) {
                        $('#req-telemedicine').dialog('open');
                    } else {
                        alert('Please try again!');
                    }
                }
            });
        });
    </script>

</body>

</html>