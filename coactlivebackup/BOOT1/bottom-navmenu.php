<div id="bottom-menu">
    <nav class="bottom-nav">
        <ul id="bottomnav">
            <li> <a class="" href="lobby.php" title="Go To Lobby"><i class="fa fa-home"></i><span class="hide-menu">Lobby</span></a></li>
            <li> <a class="auditoriums_open" href="#" title="Go To Auditorium"><i class="fa fa-chalkboard-teacher"></i><span class="hide-menu"></span>Auditorium</a></li>
            <li> <a class="" href="exhibition-halls.php" title="Go To Exhibition Halls"><i class="fa fa-box-open"></i><span class="hide-menu"></span>Exhibition Halls</a></li>
            <li> <a class="" id="show_attendees" href="#" title="View Attendees"><i class="fa fa-users"></i><span class="hide-menu">Attendees</span></a></li>
            <li> <a class="" id="show_briefcase" href="#" title="My Briefcase"><i class="fas fa-briefcase bc">
                        <div id="chat-message"></div>
                    </i><span class="hide-menu">Briefcase</span></a></li>
            <li> <a class="agenda_open" href="#" title="View Agenda"><i class="fa fa-table"></i><span class="hide-menu"></span>Agenda</a></li>
            <li> <a class="" id="show_talktous" href="#" title="Talk to Us" data-from="<?php echo $_SESSION['user_id']; ?>"><i class="fas fa-comment-alt"></i><span class="hide-menu"></span>Talk To Us</a></li>
            <li> <a class="logout" href="?action=logout" title="Logout"><i class="fa fa-sign-out-alt logout"></i><span class="hide-menu"></span>Logout</a></li>

        </ul>
    </nav>
    <div id="helpline">
        For assistance:<br>
        <i class="fas fa-phone-square-alt"></i> +917314-855-655
    </div>
</div>