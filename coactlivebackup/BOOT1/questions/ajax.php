<?php
require_once "../controls/config.php";

if(isset($_POST['action']) && !empty($_POST['action'])) {
    
    $action = $_POST['action'];
    
    switch($action) {
        
        case 'getquestions':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $limit;
        
            //$sql = "SELECT COUNT(id) FROM tbl_session_questions";  
            $sess = $_POST['session'];
              $sql = "SELECT COUNT(id) FROM tbl_session_questions";  
              if($sess != '1'){
                  $sql .= " where tbl_session_questions.session_id='$sess'";
              }
            $rs_result = mysqli_query($link,$sql);  
            $row = mysqli_fetch_row($rs_result);  
            $total_records = $row[0];  
            $total_pages = ceil($total_records / $limit);
            ?>
            <div class="row bg-dark p-1">
                <div class="col-6">
                    Total Ques: <div id="ques_count" style="display:inline-block;"><?php echo $total_records; ?></div>
                </div>
                <div class="col-6>"><div id="ques_update"></div></div>
            </div> 
            <div class="row user-details">
                <div class="col-12">
                    <table class="table table-striped table-dark">
                      <thead class="thead-inverse">
                        <tr>
                          <th width="200">Name</th>
                          <th>Question</th>
                          <th width="200">Asked On</th>
                          <th width="300">For the Speaker?</th>
                          <th width="100"></th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                      
                      
                        $query="select tbl_session_questions.id, tbl_session_questions.user_id, tbl_session_questions.question,  tbl_session_questions.asked_at, tbl_session_questions.speaker, tbl_session_questions.answered, tbl_users.first_name, tbl_users.last_name, tbl_sessions.session_title from tbl_session_questions, tbl_users, tbl_sessions where tbl_session_questions.user_id=tbl_users.userid and tbl_session_questions.session_id=tbl_sessions.session_id";
                        if($sess != '1'){
                        $query .= " and tbl_session_questions.session_id='$sess'";
                        }
                        
                        $query .= " order by  answered asc, asked_at desc LIMIT 0, $limit";
                        //echo $query;
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        //echo $query;
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <td><?php echo $data['first_name'] .' '.$data['last_name'] ; ?></td>
                            <td>
                                <?php echo $data['question']; ?>
                                <!--<br>
                                <?php if($data['reply'] ==''){ ?>
                                <input type="button" data-toggle="modal" data-target="#replyQues" name="reply" value="Reply" id="<?php echo $data['id']; ?>" class="btn btn-sm btn-primary btn-reply">
                                <?php } else { echo '<b>Reply: </b>' . nl2br($data['reply']);?>
                                    <br>
                                    <input type="button" data-toggle="modal" data-target="#updreplyQues" name="reply" value="Update" id="<?php echo $data['id']; ?>" class="btn btn-sm btn-primary btn-update">  
                                <?php } ?>-->
                            </td>
                            <td><?php 
                                $date=date_create($data['asked_at']);
                                echo date_format($date,"M d, H:i a"); ?>
                            </td>
                            <td>
                            <?php if ($data['answered'] == '0') { ?>
                            <a href="#" class="btnSpk btn btn-sm <?php if ($data['speaker'] == '0') { echo 'btn-danger'; } else { echo 'btn-success'; } ?>" onClick="updSpk('<?php echo $data['id']; ?>','<?php echo $data['speaker']; ?>')"><?php if ($data['speaker'] == '0') { echo 'Yes/No?'; } else { echo 'Cancel?'; } ?></a>
                            <?php 
                            
                            if ($data['speaker'] == '1') { ?>
                            <a href="#" class="btnSpk btn btn-sm btn-danger" onClick="updSpkAns('<?php echo $data['id']; ?>','<?php echo $data['answered']; ?>')">Mark Answered</a>
                            <?php } 
                            }
                            else
                            {
                             ?>
                            <a href="#" class="btnSpk btn btn-sm btn-danger" onClick="updSpkAns('<?php echo $data['id']; ?>','<?php echo $data['answered']; ?>')">Mark Unanswered</a>
                            <?php    
                            }
                            ?>
                            </td>
                            <td>
                            <a href="#" class="btn btn-sm btn-danger" onClick="delQues('<?php echo $data['id']; ?>')">Delete</a>
                            </td>
                            
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>   
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;
        
        case 'getques':
              $sql = "SELECT question FROM tbl_session_questions where id='".$_POST['quesid']."'";  
              $rs_result = mysqli_query($link,$sql);  
              $row = mysqli_fetch_row($rs_result);  
              $ques = $row[0];  
              
              echo $ques;
        break;
        
        case 'getreply':
              $sql = "SELECT reply FROM tbl_session_questions where id='".$_POST['quesid']."'";  
              $rs_result = mysqli_query($link,$sql);  
              $row = mysqli_fetch_row($rs_result);  
              $reply = $row[0];  
              
              echo $reply;
        break;
        
        case 'getquesupdate':
                $sess = $_POST['session'];
              $sql = "SELECT COUNT(id) FROM tbl_session_questions";  
              if($sess != '1'){
                  $sql .= " where tbl_session_questions.session_id='$sess'";
              }
              $rs_result = mysqli_query($link,$sql);  
              $row = mysqli_fetch_row($rs_result);  
              $total_records = $row[0];  
              
              echo $total_records;
        break;
        
        case 'updateques':
              $newval = 0;
              if($_POST['val'] == 0)
              {
                  $newval = 1;
              }
              else
              {
                  $newval = 0;
              }
              $sql = "Update tbl_session_questions set show_speaker ='$newval' where id = '".$_GET['id']."'";  
              $rs_result = mysqli_query($link,$sql);  
        break;
        
        case 'updatespk':
              $newval = !$_POST['val'];
              /*if($_POST['val'] == 0)
              {
                  $newval = 1;
              }
              else
              {
                  $newval = 0;
              }*/
              $sql = "Update tbl_session_questions set speaker ='$newval', answered='0' where id = '".$_POST['ques']."'";  
              $rs_result = mysqli_query($link,$sql);  
              echo $sql;
              //$row = mysqli_fetch_row($rs_result);  
              //$total_records = $row[0];  
              
              //echo $sql;
        break;
         case 'updatespkans':
              $newval = 0;
              if($_POST['val'] == 0)
              {
                  $newval = 1;
              }
              else
              {
                  $newval = 0;
              }
              $sql = "Update tbl_session_questions set answered ='$newval' where id = '".$_POST['ques']."'";  
              $rs_result = mysqli_query($link,$sql);  
        break;
        
        case 'delques':
        
            $sql = "delete from tbl_session_questions where id = '".$_POST['ques']."'";  
            $rs_result = mysqli_query($link,$sql); 
        
        break;
        



        
    }
    
}


?>