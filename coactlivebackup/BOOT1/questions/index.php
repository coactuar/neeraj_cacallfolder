<?php
require_once "../controls/config.php";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Admin Login : Live Webcast</title>
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../css/styles.css" rel="stylesheet" type="text/css">

</head>

<body class="admin">
<div class="container-fluid">
    <div class="row mt-5 bg-white color-grey p-4">
        <div class="col-8 col-md-6 offset-2 offset-md-3">
            
                        <h6>Select Session</h6>

                        <h6><a href="questions.php?s=c019dda9202475eab9dfa480e58b8717a8d9f82e8de16268">Track 01 - Upper Extremity Trauma</a></h6>
                        <h6><a href="questions.php?s=eba5b22b83e820ba4ae1d74bb2cdf1763acfd14092488d8b">Track 02 - Pelvic Trauma & Hip Fractures</a></h6>
                        <h6><a href="questions.php?s=eb4ba9b1f1dd44459ff10bf0cd446463dbbf097d58d6b825">Track 03 - Part1 - Foot and Ankle Trauma</a></h6>
                        <h6><a href="questions.php?s=e30a38fa8710ded9da3d7ae88a65585eca655fc45a95fc78">Track 03 - Part2 - Spine Trauma</a></h6>
                        <h6><a href="questions.php?s=8b78a6a833c510f31590bb42be34e9a771d3bfdde5146ba7">Track 04 - Knee Trauma</a></h6>
                        <h6><a href="questions.php?s=60e6da05c75ffcee606d9d04e4b3ebfda84ad726f5b8887b">Track 05 - Sports Injuries; Arthroscopic Procedures</a></h6>
                        <h6><a href="questions.php?s=0e9dc7673aaac97395804dca900ba3f18d7a4735e42efe3b">Track 06 - Interesting Case Presentations</a></h6>
                        <!--<h6><a href="questions.php?s=1">All Sessions</a></h6>
                        <?php
                            $sql = "select * from tbl_sessions, tbl_auditoriums where show_session=1 and tbl_sessions.audi_id=tbl_auditoriums.audi_id";
                            $rs = mysqli_query($link, $sql);
                            while($data = mysqli_fetch_assoc($rs)){
                        ?><br>
                        <h6><a href="questions.php?s=<?php  echo $data['session_id']; ?>"><?php  echo str_replace('Auditorium','Track', $data['audi_name']) .' - '.$data['session_title'];?></a></h6>
                        <?php
                            }
                        ?>-->
                  </div>
                  
                
            </form>
            </div>
        
        </div>
    </div>
</div>

<script src="../js/jquery.min.js"></script>
<script>
$(function(){
  $(document).on('submit', '#login-form', function()
{  
var sess = $('#session').val();
if(sess == 0)
{
    $('#login-message').text('Please select session');
    $('#login-message').addClass('alert-danger');
    return false;
}
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
      
      if(data=="0")
      {
        $('#login-message').text('Invalid login. Please try again.');
        $('#login-message').addClass('alert-danger');
      }
      else if(data =='s')
      {
        window.location.href='questions.php?s='+sess;  
      }
      
  });
  
  return false;
});

});

</script>
</body>
</html>