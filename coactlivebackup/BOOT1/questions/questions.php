<?php
	require_once "../controls/config.php";

    
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Questions</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/all.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">

<div class="container-fluid">
     <div class="row mt-1">
        <div class="col-12">
            <?php
                $sess = $_GET['s'];
                
                if($sess == '1')
                {
                    echo 'All Sessions';
                }
                else{            
                $sql = "select * from tbl_sessions, tbl_auditoriums where show_session=1 and tbl_sessions.audi_id=tbl_auditoriums.audi_id and session_id='$sess'";
                $rs = mysqli_query($link, $sql);
                $data = mysqli_fetch_assoc($rs);
                //echo $data['audi_name'] . ' - ' . $data['session_title'];
                }
            
            ?>
        </div>
    </div>
     <div class="row mt-1">
        <div class="col-12">
            <div id="questions"> </div>
        </div>
    </div>
    
</div>


<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
$(function(){
    getQues('1');

});

function update(pageNum)
{
  getQues(pageNum);
}

function getQues(pageNum)
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getquestions', page: pageNum, session: '<?php echo $_GET['s']; ?>'},
        type: 'post',
        success: function(response) {
            
            $("#questions").html(response);
            
        }
    });
    
}



function getupdate()
{
	var curr = $('#ques_count').text();
	//alert(curr);
	
	$.ajax({ url: 'ajax.php',
         data: {action: 'getquesupdate', session: '<?php echo $_GET['s']; ?>'},
         type: 'post',
         success: function(output) {
					 var msg = output;
                     
					 if (Number(output) > Number(curr))
					 {
						 newmsg = Number(output) - Number(curr);
						 if(newmsg == "1")
						 { 
						 msg = newmsg+ " new question available.";
						 }
						 else{
						 msg = newmsg+ " new questions available.";
						 }
						 msg += " <a href='questions.php?s=<?php echo $_GET['s']; ?>' style='color:#0c0;'>Refresh</a>";
					 }
					 else
					 {
						 msg="";
					 }
					 
					 	$("#ques_update").html(msg);
					 
                  }
});
}

setInterval(function(){ getupdate(); }, 30000);

function updateQues(id)
{
    var qid = '#answer'+id;
    var curval = $(qid).val(); 
    $.ajax({
        url: 'ajax.php?id=' + id,
         data: {action: 'updateques', val: curval },
         type: 'post',
         success: function(output) {
             //alert(output);
             getQues('1');
         }
   });   
}

function updSpk(qid, spk)
{
    $.ajax({
        url: 'ajax.php?',
         data: {action: 'updatespk', ques:qid, val: spk },
         type: 'post',
         success: function(output) {
             getQues('1');
         }
   });   
    
}
function updSpkAns(qid, ans)
{
    $.ajax({
        url: 'ajax.php?',
         data: {action: 'updatespkans', ques:qid, val: ans },
         type: 'post',
         success: function(output) {
             getQues('1');
         }
   });   
    
}

function delQues(qid)
{
    if(confirm('Are you sure?')){
        $.ajax({
            url: 'ajax.php?',
             data: {action: 'delques', ques:qid},
             type: 'post',
             success: function(output) {
                 getQues('1');
             }
       });   
    }
    
}

$(document).ready(function() {
    
});


</script>
</body>
</html>