<?php 
require_once 'model/config.php';
require_once 'model/functions.php';

$fname ='';
$lname ='';
$email ='';
$phone ='';
$country ='0';
$state = '0';
$city = '0';
$topics ='';
$updates ='';
$isp = '';
$errors=[];
$succ = false;

if (isset($_POST['reguser-btn'])) {
    if (empty($_POST['fname'])) {
        $errors['fname'] = 'First Name required';
    }
    
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $email = $_POST['emailid'];
    $phone = $_POST['phone'];
    $country = $_POST['country'];
    $state = $_POST['state'];
    $city = $_POST['city'];
    if(isset($_POST['topic'])){
        $topics = $_POST['topic'];
    }
    if(isset($_POST['updates'])){
        $updates = $_POST['updates'];
    }
    $isp = $_POST['isp'];
    
    if(strlen($phone) != 10)
    {
        $errors['phone_len'] = 'Phone No. must be 10 digits.';
    }
    
    if($country == '0'){
        $errors['country'] = 'Select your country';
    }
    if($state == '0'){
        $errors['state'] = 'Select your state';
    }
    if($city == '0'){
        $errors['city'] = 'Select your city';
    }
    if($isp == '0'){
        $errors['isp'] = 'Select your Mobile Internet Service Provider';
    }

    if(count($errors)==0){
      $member = new User();
      $registrationResponse = $member->registerMember();
      //var_dump($registrationResponse);
      if($registrationResponse['status']=='success')
      {
          $succ = true;
      }
      else{
        $errors['msg'] = $registrationResponse['message'];
      }
    }
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $event_title; ?></title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/all.min.css">
<link rel="stylesheet" href="css/styles.css">

</head>

<body>

	<div class="container">
        <div class="row no-margin">
            <div class="col-12">
                <img src="img/reg-banner.jpg" class="img-fluid" alt=""/> 
            </div>
        </div>
        <div class="row bg-white color-grey">
            <div class="col-12 text-center">
                 <h3 class="reg-title">Register for BOOT International Live!</h3>
            </div>
        </div>
        <div class="row bg-white color-grey">
            <div class="col-12 col-md-8 offset-md-2">
                <?php if (!$succ) { ?>
                <div id="register-area">
                  <?php
                      if (count($errors) > 0): ?>
                      <div class="alert alert-danger">
                        <ul class="list-unstyled">
                        <?php foreach ($errors as $error): ?>
                        <li>
                          <?php echo $error; ?>
                        </li>
                        <?php endforeach;?>
                        </ul>
                      </div>
                    <?php endif;
                    ?>
                  <form method="POST">
                      <div class="row mt-2">
                          <div class="col-12 col-md-6">
                              <label>First Name<sup class="req">*</sup></label>
                              <input type="text" id="fname" name="fname" class="input" value="<?php echo $fname; ?>" autocomplete="off" required>
                          </div>
                          <div class="col-12 col-md-6">
                              <label>Last Name<sup class="req">*</sup></label>
                              <input type="text" id="lname" name="lname" class="input" value="<?php echo $lname; ?>" autocomplete="off" required>
                          </div>
                      </div>
                      <div class="row mb-1">
                        <div class="col-12">
                            Please enter the name correctly as the same will be used for creating Attendance Certificate.
                        </div>
                      </div>
                      <div class="row mt-3 mb-1">
                          <div class="col-12">
                              <label>Email ID<sup class="req">*</sup></label>
                              <input type="email" id="emailid" name="emailid" class="input" value="<?php echo $email; ?>" autocomplete="off" required>
                          </div>
                      </div>
                      <div class="row mt-3 mb-1">
                          <div class="col-12 col-md-6">
                              <label>Phone No.<sup class="req">*</sup></label>
                              <input type="number" id="phone" name="phone" class="input" value="<?php echo $phone; ?>" autocomplete="off" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
                          </div>
                          <div class="col-12 col-md-6">
                              <label>Country<sup class="req">*</sup></label>
                              <div id="countries">
                              <select class="input" id="country" name="country" onChange="updateState()" required>
                                  <option>Select Country</option>
                              </select>
                              </div>
                          </div>
                          
                      </div>
                      <div class="row mt-3 mb-1">
                          <div class="col-12 col-md-6">
                              <label>State<sup class="req">*</sup></label>
                              <div id="states">
                              <select class="input" id="state" name="state" onChange="updateCity()" required>
                                  <option>Select State</option>
                              </select>
                              </div>
                          </div>
                          <div class="col-12 col-md-6">
                              <label>City<sup class="req">*</sup></label>
                              <div id="cities">
                              <select class="input" id="city" name="city" required>
                                  <option>Select City</option>
                              </select>
                              </div>
                          </div>
                      </div>
                      <div class="row mt-3 mb-1">
                          <div class="col-12 text-left">
                              <label>Select Topics of Interest:<sup class="req">*</sup></label>
                              <label><b>Day 1</b></label>
                              <input type="checkbox" name="topic[]" value="upper-trauma">Upper Extremity Trauma <br>
                              <input type="checkbox" name="topic[]" value="pelvic-hip">Pelvic Trauma &amp; Hip Fractures/Recon<br>
                              <input type="checkbox" name="topic[]" value="foot-ankle">Foot &amp; Ankle Injuries and Spine Trauma<br><br>
                              <label><b>Day 2</b></label>
                              <input type="checkbox" name="topic[]" value="knee-trauma">Knee Trauma &amp; Recon<br>
                              <input type="checkbox" name="topic[]" value="sports-injuries">Sports Injuries, Arthroscopic Procedures<br>
                              <input type="checkbox" name="topic[]" value="presentations">Interesting Case Presentations
                          </div>
                      </div>
                      <div class="row divider">
                          <div class="col-12">
                              <label class="mt-2 lower">Which Internet Mobile Service Are You Using Currently?<sup class="req">*</sup></label>
                              <select id="isp" name="isp" class="input mb-2" required>
                                <option value="0">Select Internet Mobile Service Provider</option>
                                <option value="Vodafone">Vodafone</option>
                                <option value="Airtel">Airtel</option>
                                <option value="RelIO">Reliance Jio</option>
                                <option value="BSNL">BSNL</option>
                              </select>
                                
                              <input type="checkbox" name="updates[]" value="program">I would like to receive updates related to this program <br>
                              <input type="checkbox" name="updates[]" value="integrace">I would like to receive updates related to Integrace Pvt. Ltd. <br>
                          </div>
                      </div>
                      <div class="row mt-2 mb-3">
                          <div class="col-12">
                              <small><sup class="req">*</sup> denotes mandatory fields.</small><br><br>
                              <input type="submit" name="reguser-btn" id="btnSubmit" class="form-submit btn-register" value="" />
                              <a href="./" class="form-cancel"><img src="img/cancel-btn.jpg" alt=""/></a>
                          </div>
                      
                      </div>
                  </form>
                </div>
                <?php } else { ?>
                  <div id="registration-confirmation">
                      <div class="alert alert-success">
                      You are registered succesfully for the <b>BOOT International Live!</b><br>
                      Please check your email regarding instructions on how to login.
                      </div>
                      
                  <a href="./">Continue to Login</a> 
                  </div>
                <?php } ?>
                 
            </div>
        </div>
        <div class="row bg-white">
            <div class="col-12">
                <img src="img/line-h.jpg" class="img-fluid" alt=""/> 
            </div>
        </div>
        <div class="row bg-white p-2">
            <div class="col-4 bor-right p-2 text-center">
                <img src="img/in-assoc.png" class="img-fluid bot-img" alt=""/>
            </div>
            <div class="col-4 p-2 text-center">
                <img src="img/sci-partner.png" class="img-fluid bot-img" alt=""/>
            </div>
          <div class="col-4 bor-left p-2 text-center color-grey">
                <img src="img/brought-by.png" class="img-fluid bot-img" alt=""/>
                <div class="visit">
                Visit us at <a href="https://www.integracehealth.com/about.html" class="link" target="_blank">https://www.integracehealth.com/about.html</a>
                </div>
          </div>
        </div>
        <!--<div class="row no-margin">
            <div class="col-12">
                <img src="img/reg-bottom-banner.png" class="img-fluid" alt=""/> 
            </div>
        </div>-->
        <!--<div class="row bg-white color-grey">
            <div class="col-4 offset-8 text-center">
                 Visit us at <a href="https://www.integracehealth.com/about.html" class="link" target="_blank">https://www.integracehealth.com/about.html</a>
            </div>
        </div>-->
        <!--<div class="row bg-white color-grey">
              <div class="col-12 p-2 text-center">
            Visit us at <a href="https://www.integracehealth.com/about.html" class="link" target="_blank">https://www.integracehealth.com/about.html</a> </div>
          </div>-->
        
	</div>
<div id="code">IPL/O/WI/14072020</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
function getCountries()
{
    $.ajax({
        url: 'controls/server.php',
        data: {action: 'getcountries'},
        type: 'post',
        success: function(response) {
            
            $("#countries").html(response);
        }
    });
    
}

function updateState()
{
    var c = $('#country').val();
    if(c!='0'){
        $.ajax({
            url: 'controls/server.php',
            data: {action: 'getstates', country : c },
            type: 'post',
            success: function(response) {
                
                $("#states").html(response);
            }
        });
    }
}

function updateCity()
{
    var s = $('#state').val();
    if(s!='0'){
        $.ajax({
            url: 'controls/server.php',
            data: {action: 'getcities', state : s },
            type: 'post',
            success: function(response) {
                
                $("#cities").html(response);
            }
        });
    }
}

getCountries();
//updateState();
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<!--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-20"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-20');
</script>-->

</body>
</html>