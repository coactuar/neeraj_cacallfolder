<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Video</title>
<style>
html, body{
    height:100%;
}
body{
    margin:0;
    padding:0;
}
#player{
    width:100%;
    height:100vh;
}
</style>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>

</head>

<body>
<div id="player"></div>
  <script type="text/javascript" src="//cdn.jsdelivr.net/gh/clappr/clappr-level-selector-plugin@latest/dist/level-selector.min.js"></script>
  <script>
    var player = new Clappr.Player(
    {
        source: "https://d38233lepn6k1s.cloudfront.net/out/v1/4e511c852cbf431a829334a9cdcf90eb/4553131ab95f4d748725f284186e6605/57c32c311a6e4f23bdd4bd4d63d426a1/index.m3u8",      
        parentId: "#player",
        //poster: "img/poster.jpg",
        width: "100%",
        height: "100%",
        loop: true
        
    });
    
    player.play();
</script>
</body>
</html>