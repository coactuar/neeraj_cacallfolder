<?php
require_once "../controls/config.php";
require_once "../functions.php";

if (!isset($_SESSION["super_user"])) {

  header("location: ./");
  exit;
}
if (isset($_GET['action']) && !empty($_GET['action'])) {
  $action = $_GET['action'];
  if ($action == "logout") {
    unset($_SESSION['super_user']);
    header("location: ./");
    exit;
  }
}

$audi = new Session();
$audi01 = $audi->getAudi('Auditorium 01');
$audi02 = $audi->getAudi('Auditorium 02');
$audi03 = $audi->getAudi('Auditorium 03');

$audi01_id = $audi01[0]['audi_id'];
$audi02_id = $audi02[0]['audi_id'];
$audi03_id = $audi03[0]['audi_id'];
?>
<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Dashboard</title>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/all.min.css">
  <link rel="stylesheet" type="text/css" href="../css/styles.css">
  <link rel="stylesheet" type="text/css" href="../css/admin.css">

  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>
  <script type="text/javascript" src="//cdn.jsdelivr.net/gh/clappr/clappr-level-selector-plugin@latest/dist/level-selector.min.js"></script>


</head>

<body class="admin">
  <nav class="navbar navbar-expand-md navbar-dark">
    <!--<a class="navbar-brand" href="#"><img src="../img/logo.png" class="logo"></a>-->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Hello, <?php echo $_SESSION["super_user"]; ?>!</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="?action=logout">Logout</a>
        </li>
      </ul>

    </div>
  </nav>

  <div class="container-fluid">
    <div id="superdashboard">
      <div class="row">
        <div class="col-4">
          <div id="audi01" class="audi">
            <div class="title">Auditorium 1</div>
            <div class="video">
              <div id="vid01" class="video-player"></div>
              <?php
              $audiUrl01 = $audi->getCurrLiveSession($audi01_id);
              $sess01Id = $audiUrl01[0]['session_id'];
              ?>
            </div>
            <div class="title">Viewers: <div id="audi1viewer" style="display: inline-block;">0</div>
            </div>
            <div class="tabs">
              <a href="#" id="qa-audi1" onClick="showQA1()" class="active">Questions</a>
              <!-- <a href="#" id="poll-audi1" onClick="showPoll1()" class="">Polls</a> -->
            </div>
            <div id="questions-audi1" style="display:block;">
              <div id="audi1ques" class="details scroll"></div>
            </div>
            <!-- <div id="polls-audi1" style="display:none;">
              <div id="audi1poll" class="details scroll">Poll</div>
            </div> -->

          </div>

        </div>
        <div class="col-4">
          <div id="audi02" class="audi">
            <div class="title">Auditorium 2</div>
            <div class="video">
              <div id="vid02" class="video-player"></div>
              <?php
              $audiUrl02 = $audi->getCurrLiveSession($audi02_id);
              $sess02Id = $audiUrl02[0]['session_id'];
              ?>
            </div>
            <div class="title">Viewers: <div id="audi2viewer" style="display: inline-block;">0</div>
            </div>
            <div class="tabs">
              <a href="#" id="qa-audi2" onClick="showQA2()" class="active">Questions</a>
              <!-- <a href="#" id="poll-audi2" onClick="showPoll2()" class="">Polls</a> -->
            </div>
            <div id="questions-audi2" style="display:block;">
              <div id="audi2ques" class="details scroll"></div>
            </div>
            <!-- <div id="polls-audi2" style="display:none;">
              <div id="audi2poll" class="details scroll">Poll</div>
            </div> -->

          </div>

        </div>
        <div class="col-4">
          <div id="audi03" class="audi">
            <div class="title">Auditorium 3</div>
            <div class="video">
              <div id="vid03" class="video-player"></div>
              <?php
              $audiUrl03 = $audi->getCurrLiveSession($audi03_id);
              $sess03Id = $audiUrl03[0]['session_id'];
              ?>
            </div>
            <div class="title">Viewers: <div id="audi3viewer" style="display: inline-block;">0</div>
            </div>
            <div class="tabs">
              <a href="#" id="qa-audi3" onClick="showQA3()" class="active">Questions</a>
              <!-- <a href="#" id="poll-audi3" onClick="showPoll3()" class="">Polls</a> -->
            </div>
            <div id="questions-audi3" style="display:block;">
              <div id="audi3ques" class="details scroll"></div>
            </div>
            <!-- <div id="polls-audi3" style="display:none;">
              <div id="audi3poll" class="details scroll">Poll</div>
            </div> -->

          </div>

        </div>

      </div>
    </div>

  </div>


  <script src="../js/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>

  <script>
    function showQA1() {
      $('#audi01 .tabs a').removeClass('active');
      //getSessions(audi, 'day1', '');
      $('#qa-audi1').addClass('active');
      $('#questions-audi1').css('display', 'block');
      $('#polls-audi1').css('display', 'none');
    }

    function showPoll1() {
      $('#audi01 .tabs a').removeClass('active');
      //getSessions(audi, 'day1', '');
      $('#poll-audi1').addClass('active');
      $('#questions-audi1').css('display', 'none');
      $('#polls-audi1').css('display', 'block');
    }

    function showQA2() {
      $('#audi02 .tabs a').removeClass('active');
      //getSessions(audi, 'day2', '');
      $('#qa-audi2').addClass('active');
      $('#questions-audi2').css('display', 'block');
      $('#polls-audi2').css('display', 'none');
    }

    function showPoll2() {
      $('#audi02 .tabs a').removeClass('active');
      //getSessions(audi, 'day2', '');
      $('#poll-audi2').addClass('active');
      $('#questions-audi2').css('display', 'none');
      $('#polls-audi2').css('display', 'block');
    }

    function showQA3() {
      $('#audi03 .tabs a').removeClass('active');
      //getSessions(audi, 'day3', '');
      $('#qa-audi3').addClass('active');
      $('#questions-audi3').css('display', 'block');
      $('#polls-audi3').css('display', 'none');
    }

    function showPoll3() {
      $('#audi03 .tabs a').removeClass('active');
      //getSessions(audi, 'day3', '');
      $('#poll-audi3').addClass('active');
      $('#questions-audi3').css('display', 'none');
      $('#polls-audi3').css('display', 'block');
    }

    function audiViews(sess, ele) {
      $.ajax({
        url: '../controls/server.php',
        data: {
          action: 'getLiveSessionViewerCount',
          sessId: sess
        },
        type: 'post',
        success: function(output) {
          console.log(output);
          $(ele).text(output);

        }
      });

    }

    function audiQues(sess, ele) {
      $.ajax({
        url: '../controls/server.php',
        data: {
          action: 'getSessionQuestions',
          sessId: sess
        },
        type: 'post',
        success: function(output) {
          console.log(output);
          $(ele).html(output);

        }
      });

    }
  </script>
  <?php
  if ($audiUrl01 != null) {
  ?>
    <script>
      var player01 = new Clappr.Player({
        source: "<?php echo $audiUrl01[0]['session_webcast_url']; ?>",
        parentId: "#vid01",
        width: "100%",
        height: "100%",
        mediacontrol: {
          buttons: "#20386e"
        },
        autoplay: true,
        mute: true,
        poster: "../img/boot-poster.jpg",

      });

      player01.play();

      audiViews('<?php echo $sess01Id; ?>', '#audi1viewer');
      audiQues('<?php echo $sess01Id; ?>', '#audi1ques');

      setInterval(function() {
        audiViews('<?php echo $sess01Id; ?>', '#audi1viewer');
        audiQues('<?php echo $sess01Id; ?>', '#audi1ques');
      }, 15000);
    </script>
  <?php } ?>
  <?php
  if ($audiUrl02 != null) {
  ?>
    <script>
      var player02 = new Clappr.Player({
        source: "<?php echo $audiUrl02[0]['session_webcast_url']; ?>",
        parentId: "#vid02",
        width: "100%",
        height: "100%",
        mediacontrol: {
          buttons: "#20386e"
        },
        autoplay: true,
        mute: true,
        poster: "../img/boot-poster.jpg",

      });

      player02.play();

      audiViews('<?php echo $sess02Id; ?>', '#audi2viewer');
      audiQues('<?php echo $sess02Id; ?>', '#audi2ques');

      setInterval(function() {
        audiViews('<?php echo $sess02Id; ?>', '#audi2viewer');
        audiQues('<?php echo $sess02Id; ?>', '#audi2ques');
      }, 15000);
    </script>
  <?php } ?>
  <?php
  if ($audiUrl03 != null) {
  ?>
    <script>
      var player03 = new Clappr.Player({
        source: "<?php echo $audiUrl03[0]['session_webcast_url']; ?>",
        parentId: "#vid03",
        width: "100%",
        height: "100%",
        mediacontrol: {
          buttons: "#20386e"
        },
        autoplay: true,
        mute: true,
        poster: "../img/boot-poster.jpg",

      });

      player03.play();

      audiViews('<?php echo $sess03Id; ?>', '#audi3viewer');
      audiQues('<?php echo $sess03Id; ?>', '#audi3ques');

      setInterval(function() {
        audiViews('<?php echo $sess03Id; ?>', '#audi3viewer');
        audiQues('<?php echo $sess03Id; ?>', '#audi3ques');
      }, 15000);
    </script>
  <?php } ?>
</body>

</html>