<?php
//use Genseer\User;
require_once "model/config.php";
require_once 'model/functions.php';

$loginEmail = '';
$errors = [];

$succ = false;

if (isset($_POST['mainlogin-btn'])) {
  if (empty($_POST['loginEmail'])) {
    $errors['email'] = 'Email ID is required';
  }

  $loginEmail = $_POST['loginEmail'];

  if (count($errors) === 0) {

    $member = new User();
    $response = $member->loginMember($loginEmail);
    $errors['msg'] = $response;
  }
}
?>
<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $event_title; ?></title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/all.min.css">
  <link rel="stylesheet" href="css/styles.css">
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>

</head>

<body>
  <div class="container-fluid">

    <div id="login-area">
      <div class="row">
        <div class="col-12 col-md-6 col-lg-7 ">
          <div class="login-bg mb-3">
            <img src="img/login-banner.png" alt="" class="img-fluid" />
          </div>
          <div id="loginform-area">
            If already registered, login here:
            <form method="post">
              <?php
              if (count($errors) > 0) : ?>
                <div class="alert alert-danger alert-msg">
                  <ul class="list-errors">
                    <?php foreach ($errors as $error) : ?>
                      <li>
                        <?php echo $error; ?>
                      </li>
                    <?php endforeach; ?>
                  </ul>
                </div>
              <?php endif;
              ?>
              <div class="form-group">
                <!-- <label for="loginEmail">Enter your Email ID</label> -->
                <input type="text" name="loginEmail" placeholder="Enter Email ID" class="input" value="<?php echo $loginEmail; ?>">
              </div>
              <div class="form-group">
                <input type="submit" name="mainlogin-btn" id="btnLogin" class="btn btn-login" value="">
              </div>
            </form>
          </div>

        </div>
        <div class="col-12 col-md-6 col-lg-5 text-center">
          <div id="intro-video">
            <div id="player"></div>
          </div>
          <div id="reg-btn">
            <a href="register.php"><img src="img/reg-now-btn.png" alt="" /></a>
          </div>


        </div>
      </div>
    </div>
  </div>
  <div id="code">IPL/O/WI/14072020</div>



  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script>
    $(document).ready(function() {
      /*$('#message').hide();
    $("#loginForm").submit(function(event){
		submitForm();
		return false;
	});*/

    });

    function submitForm() {
      $.ajax({
        type: "POST",
        url: "chklogin.php",
        cache: false,
        data: $('form#loginForm').serialize(),
        success: function(response) {
          if (response == 'login') {
            $("#login-modal").modal('hide');
            location.href = 'lobby';
          } else if (response == '0') {
            $('#message').text('You are not registered').removeClass().addClass('alert alert-danger').fadeIn();
            return false;
          } else {
            $('#message').text(response).removeClass().addClass('alert alert-danger').fadeIn();
            return false;
          }
        },
        error: function() {
          alert("Error");
        }
      });
    }
  </script>
  <script>
    var player = new Clappr.Player({
      source: "https://d38233lepn6k1s.cloudfront.net/out/v1/948e04d355a44cdab717b403ead57a2e/4553131ab95f4d748725f284186e6605/57c32c311a6e4f23bdd4bd4d63d426a1/index.m3u8",
      parentId: "#player",
      width: "100%",
      height: "100%",
      mediacontrol: {
        buttons: "#20386e"
      },
      autoplay: false,
      poster: "img/boot-poster.jpg",

    });

    // player.play();
  </script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <!--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-20"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-20');
</script>-->

</body>

</html>