<?php
require_once "../controls/config.php";
$exh_id = $_GET['e'];

$sql = "select resource_title from tbl_exhibitor_resources where resource_id='" . $exh_id . "'";
$rs = mysqli_query($link, $sql);
$d = mysqli_fetch_assoc($rs);
$title = $d['resource_title'];


$sql = "SELECT * from tbl_exhibitor_resource_downloads, tbl_users where tbl_exhibitor_resource_downloads.user_id=tbl_users.userid and resource_id='" . $exh_id . "'";
//echo $sql;
$rs = mysqli_query($link, $sql);
$data = array();
if (mysqli_affected_rows($link) > 0) {
  $i = 0;
  while ($c = mysqli_fetch_assoc($rs)) {
    $data[$i]['First Name'] = $c['first_name'];
    $data[$i]['Last Name'] = $c['last_name'];
    $data[$i]['E-mail ID'] = $c['emailid'];

    $i++;
  }

  //var_dump($data);
  $filename = $title . "_downloads.xls";
  header("Content-Type: application/vnd.ms-excel");
  header("Content-Disposition: attachment; filename=\"$filename\"");
  ExportFile($data);
}

function ExportFile($records)
{
  $heading = false;
  if (!empty($records))
    foreach ($records as $row) {
      if (!$heading) {
        // display field/column names as a first row
        echo implode("\t", array_keys($row)) . "\n";
        $heading = true;
      }
      echo implode("\t", array_values($row)) . "\n";
    }
  exit;
}
