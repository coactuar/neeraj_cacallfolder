<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//define('__ROOT__', dirname(dirname(__FILE__)));


class SendEmail{
    
    public $to;
    public $name;
    public $subject;
    public $message;
    
    const MAILHOST = 'smtp.gmail.com';
    const MAILUSER = 'support@coact.co.in';
    const MAILPASS = 'coact2020';
    const MAILNAME = 'BOOT International Live';
    
    function __construct()
    {
        require_once 'vendor/autoload.php';

    }
    function set_to($to)
    {
        $this->to = $to;

    }
    function set_name($name)
    {
        $this->name = $name;

    }
    function set_message($message)
    {
        $this->message = $message;

    }
    
    function set_subject($subject)
    {
        $this->subject = $subject;

    }
    
    function send()
    {
        $mail = new PHPMailer(true);
        
        $mail->isSMTP();                                            // Send using SMTP
        $mail->Host       = SendEmail::MAILHOST;                    // Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = SendEmail::MAILUSER;                     // SMTP username
        $mail->Password   = SendEmail::MAILPASS;                               // SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS; //STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $mail->Port       = 465;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
    
        //Recipients
        $mail->setFrom(SendEmail::MAILUSER, SendEmail::MAILNAME);
        $mail->addAddress($this->to, $this->name);     // Add a recipient
    
        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $this->subject;
        $mail->Body    = $this->message;
    
        $mail->send();
    }
    
    function sendTemplate($name, $reglink, $siteurl)
    {
        $mail = new PHPMailer(true);
        
        $mail->isSMTP();                                            // Send using SMTP
        //$mail->SMTPDebug = SMTP::DEBUG_SERVER; 
        $mail->Host       = SendEmail::MAILHOST;                    // Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = SendEmail::MAILUSER;                     // SMTP username
        $mail->Password   = SendEmail::MAILPASS;                               // SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS; //STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $mail->Port       = 465;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
    
        //Recipients
        $mail->setFrom(SendEmail::MAILUSER, SendEmail::MAILNAME);
        $mail->addAddress($this->to, $this->name);     // Add a recipient
    
        
        // Content
        $mail->isHTML(true); 
        //$reglink = "https://coact.live/BOOTInternationalLive/verify.php?e=".$email."&t=".$token;
        //$siteurl = 'https://coact.live/BOOTInternationalLive';
        $message = file_get_contents('https://coact.live/BOOTInternationalLive/email_templates/reg-email.html');
        $message = str_replace('%name%', $name, $message);
        $message = str_replace('%reglink%', $reglink, $message);
        $message = str_replace('%siteurl%', $siteurl, $message);
            
        $mail->Subject = $this->subject;
        $mail->MsgHTML($message);
    
        $mail->send();
    }
    
}
?>