<?php
require_once 'constants.php';

class Hall
{

    private $ds;

    function __construct()
    {
        $this->ds = new DataSource();
    }

    public function addHall()
    {
        $errors = [];
        $succ = '';

        $exhib_name = '';


        if (empty($_POST['exhibname'])) {
            $errors['exhib'] = 'Exhibitor Name is required';
        }

        $exhib_name = $_POST['exhibname'];


        $exhibid = bin2hex(random_bytes(24)); // generate unique token
        $active = 1;

        $query = "Insert into tbl_exhibitors(exhibitor_name,exhibitor_id, active) values(?,?,?)";
        $paramType = 'ssi';
        $paramValue = array(
            $exhib_name,
            $exhibid,
            $active
        );
        $hallid = $this->ds->insert($query, $paramType, $paramValue);

        return $hallid;
    }

    public function getHallsList()
    {
        $query = "select * from tbl_exhibitors";
        $paramType = '';
        $paramValue = array();
        $halls = $this->ds->select($query, $paramType, $paramValue);

        return $halls;
    }

    public function getHall($hallid)
    {
        $query = "select * from tbl_exhibitors where exhibitor_id=?";
        $paramType = 's';
        $paramValue = array(
            $hallid
        );
        $hall = $this->ds->select($query, $paramType, $paramValue);

        return $hall;
    }

    public function verifyHallId($hallid)
    {
        $query = "select * from tbl_exhibitors where exhibitor_id=? and active='1'";
        $paramType = 's';
        $paramValue = array(
            $hallid
        );
        $hall = $this->ds->select($query, $paramType, $paramValue);

        return $hall;
    }

    public function addHallManager()
    {
        $errors = [];
        $succ = false;

        $exhib_id = '0';
        $exhib_name = '';
        $exhib_email = '';
        $exhib_pwd = '';

        if ($_POST['exhibid'] == '0') {
            $errors['exhib'] = 'Select Exhibitor is required';
        }

        if (empty($_POST['exhibName'])) {
            $errors['exhibname'] = "Exhibitor Manager's Name is required";
        }
        if (empty($_POST['exhibEmail'])) {
            $errors['exhibemail'] = "Exhibitor Manager's Email ID is required";
        }
        if (empty($_POST['exhibPwd'])) {
            $errors['exhibpwd'] = "Exhibitor Manager's Password is required";
        }

        $exhib_id = $_POST['exhibid'];
        $exhib_name = $_POST['exhibName'];
        $exhib_email = $_POST['exhibEmail'];
        $exhib_pwd = $_POST['exhibPwd'];

        $manager_id = bin2hex(random_bytes(24)); // generate unique token
        $createdon  = date('Y/m/d H:i:s');
        $active = 1;

        $exhib_hash_pwd = password_hash($exhib_pwd, PASSWORD_DEFAULT);

        $query = "Insert into tbl_exhibitor_logins(exhibitor_id,manager_id, name, emailid, hash_pwd, active) values(?,?,?,?,?,?)";
        $paramType = 'sssssi';
        $paramValue = array(
            $exhib_id,
            $manager_id,
            $exhib_name,
            $exhib_email,
            $exhib_hash_pwd,
            $active
        );
        $managerid = $this->ds->insert($query, $paramType, $paramValue);

        return $managerid;
    }

    public function addHallVideo()
    {
        $errors = [];
        $succ = false;

        $exhib_id = '0';
        $title = '';
        $url = '';
        $desc = '';

        if ($_POST['exhibid'] == '0') {
            $errors['exhib'] = 'Select Exhibitor is required';
        }

        if (empty($_POST['videoTitle'])) {
            $errors['title'] = "Video Title is required";
        }
        if (empty($_POST['videoURL'])) {
            $errors['url'] = "Video URL is required";
        }
        if (isset($_POST['videoDesc'])) {
            $desc = $_POST['videoDesc'];
        }


        $exhib_id = $_POST['exhibid'];
        $title = $_POST['videoTitle'];
        $url = $_POST['videoURL'];

        $video_id = bin2hex(random_bytes(24)); // generate unique token
        $addedon  = date('Y/m/d H:i:s');
        $active = 1;


        $query = "Insert into tbl_exhibitor_videos(exhibitor_id,video_id, video_title, video_url, description, active, added_on) values(?,?,?,?,?,?,?)";
        $paramType = 'sssssis';
        $paramValue = array(
            $exhib_id,
            $video_id,
            $title,
            $url,
            $desc,
            $active,
            $addedon
        );
        $videoid = $this->ds->insert($query, $paramType, $paramValue);

        return $videoid;
    }

    public function addHallResource()
    {
        $errors = [];
        $succ = false;

        $exhib_id = '0';
        $title = '';
        $url = '';
        $desc = '';

        if ($_POST['exhibid'] == '0') {
            $errors['exhib'] = 'Select Exhibitor is required';
        }

        if (empty($_POST['resTitle'])) {
            $errors['title'] = "Resource Title is required";
        }
        if (empty($_POST['resURL'])) {
            $errors['url'] = "Resource URL is required";
        }
        if (isset($_POST['resDesc'])) {
            $desc = $_POST['resDesc'];
        }


        $exhib_id = $_POST['exhibid'];
        $title = $_POST['resTitle'];
        $url = $_POST['resURL'];

        $res_id = bin2hex(random_bytes(24)); // generate unique token
        $addedon  = date('Y/m/d H:i:s');
        $active = 1;
        $download_count = 0;


        $query = "Insert into tbl_exhibitor_resources(exhibitor_id,resource_id, resource_title, resource_url, description, download_count, active, added_on) values(?,?,?,?,?,?,?,?)";
        $paramType = 'sssssiis';
        $paramValue = array(
            $exhib_id,
            $res_id,
            $title,
            $url,
            $desc,
            $download_count,
            $active,
            $addedon
        );
        $resid = $this->ds->insert($query, $paramType, $paramValue);

        return $resid;
    }


    public function updateMemberExhStatus($hallid, $userid)
    {
        $today = date('Y/m/d H:i:s', time());

        $query = "select * from tbl_exhibitor_visitors where exhibitor_id = ? and user_id=? and exit_time >= ?";
        $paramType = 'sss';
        $paramValue = array(
            $hallid,
            $userid,
            $today
        );

        $status = $this->ds->getRecordCount($query, $paramType, $paramValue);
        if ($status > 0) {
            $exit_time  = date('Y/m/d H:i:s', time() + 15);
            $query = "UPDATE tbl_exhibitor_visitors set exit_time=? where user_id=? and exhibitor_id=? and exit_time >= ?";
            $paramType = 'ssss';
            $paramValue = array(
                $exit_time,
                $userid,
                $hallid,
                $today
            );
            $this->ds->execute($query, $paramType, $paramValue);
            echo '1';
        } else {
            $entry_time  = date('Y/m/d H:i:s', time());
            $exit_time  = date('Y/m/d H:i:s', time() + 15);
            $query = "Insert into tbl_exhibitor_visitors(exhibitor_id, user_id, entry_time, exit_time) values(?,?,?,?)";
            $paramType = 'ssss';
            $paramValue = array(
                $hallid,
                $userid,
                $entry_time,
                $exit_time
            );
            $sesid = $this->ds->insert($query, $paramType, $paramValue);
            return $sesid;
        }
    } //

    public function getExhibitorTalkHistory($exhibitor_to)
    {
        $hall = $this->getHall($exhibitor_to);
        $exhib_hall_id = $hall[0]['exhibit_hall_id'];
        $exhibitor_name = $hall[0]['exhibitor_name'];

        $showLimit = 50;

        $query = "(select tbl_exhibitors_chat.id, first_name, message, user_id_from, chat_time from tbl_exhibitors_chat,tbl_users where ((user_id_to =?) OR (user_id_from=? AND user_id_to=?)) AND tbl_exhibitors_chat.user_id_from = tbl_users.id) UNION (SELECT tbl_exhibitors_chat.id, user_id_from,message, user_id_from, chat_time FROM `tbl_exhibitors_chat`, tbl_exhibitor_logins where user_id_from=? and tbl_exhibitors_chat.user_id_from=tbl_exhibitor_logins.exhibit_hall_id) ORDER by chat_time DESC limit $showLimit";
        $paramType = 'ssss';
        $paramValue = array(
            $exhibitor_to,
            $exhib_hall_id,
            $exhibitor_to,
            $exhib_hall_id,

        );

        $chathist = $this->ds->select($query, $paramType, $paramValue);

        return $chathist;
    } //

    public function sendExhMessage($from, $to, $msg)
    {
        $chat_time   = date('Y/m/d H:i:s');

        $query = "insert into tbl_exhibitors_chat(user_id_from, user_id_to, message, chat_time) values(?,?,?,?)";
        $paramType = 'ssss';
        $paramValue = array(
            $from,
            $to,
            $msg,
            $chat_time,

        );
        $chatid = $this->ds->insert($query, $paramType, $paramValue);

        return $chatid;
    }

    public function getExhibResources($exhibitorid)
    {
        $query = "select * from tbl_exhibitor_resources where exhibitor_id = ?";
        $paramType = 's';
        $paramValue = array(
            $exhibitorid
        );

        $reslist = $this->ds->select($query, $paramType, $paramValue);

        return $reslist;
    }
    /*Get a list of videos in a hall */
    public function getExhibVideos($exhibitorid)
    {
        $query = "select * from tbl_exhibitor_videos where exhibitor_id = ?";
        $paramType = 's';
        $paramValue = array(
            $exhibitorid
        );

        $vidlist = $this->ds->select($query, $paramType, $paramValue);

        return $vidlist;
    }

    public function getExhibVideoDetails($videoid)
    {
        $query = "select * from tbl_exhibitor_videos, tbl_exhibitors where tbl_exhibitor_videos.exhibitor_id =tbl_exhibitors.exhibitor_id and video_id = ?";
        $paramType = 's';
        $paramValue = array(
            $videoid
        );

        $reslist = $this->ds->select($query, $paramType, $paramValue);

        return $reslist;
    }

    public function updateVideoView($videoid, $attendeeid, $viewtime)
    {
        $query = "Insert into tbl_exhibitor_videos_views(video_id, user_id, view_time) values(?, ?, ?)";
        $paramType = 'sss';
        $paramValue = array(
            $videoid,
            $attendeeid,
            $viewtime
        );

        $viewid = $this->ds->insert($query, $paramType, $paramValue);
        return $viewid;
    }

    public function getExhibResDetails($resid)
    {
        $query = "select * from tbl_exhibitor_resources where id = ?";
        $paramType = 's';
        $paramValue = array(
            $resid
        );

        $reslist = $this->ds->select($query, $paramType, $paramValue);

        return $reslist;
    }

    public function updateResDL($resid, $userid, $dltime)
    {
        $query = "select * from tbl_exhibitor_resources where resource_id = ?";
        $paramType = 's';
        $paramValue = array(
            $resid
        );
        $resource = $this->ds->select($query, $paramType, $paramValue);
        if (!empty($resource)) {
            $count = $resource[0]['download_count'] + 1;

            $query = "UPDATE tbl_exhibitor_resources set download_count=? where resource_id=?";
            $paramType = 'ss';
            $paramValue = array(
                $count,
                $resid
            );

            $this->ds->execute($query, $paramType, $paramValue);

            $query = "Insert into tbl_exhibitor_resource_downloads(resource_id, user_id, dl_time) values(?, ?, ?)";
            $paramType = 'sss';
            $paramValue = array(
                $resid,
                $userid,
                $dltime
            );

            $dlid = $this->ds->insert($query, $paramType, $paramValue);
            return $dlid;
        }
    }
    /*List of visitors to a hall*/
    public function getVisitorsList($hallid)
    {
        //$query = "select first_name, last_name, entry_time, exit_time from tbl_exhibitor_visitors, tbl_users where exhibitor_id = ? and tbl_exhibitor_visitors.user_id = tbl_users.userid order by exit_time DESC";
        $query = "select tbl_exhibitor_visitors.user_id, max(entry_time) as timein, max(exit_time) as timeout from tbl_exhibitor_visitors, tbl_users where exhibitor_id = ? and tbl_exhibitor_visitors.user_id = tbl_users.userid GROUP by tbl_exhibitor_visitors.user_id  order by timein DESC limit 50";
        $paramType = 's';
        $paramValue = array(
            $hallid
        );

        $reslist = $this->ds->select($query, $paramType, $paramValue);

        return $reslist;
    }
    /*No. of visits by a visitor to a hall*/
    public function getVisitorVisitCount($hallid, $userid)
    {
        $query = "select * from tbl_exhibitor_visitors where exhibitor_id =? and user_id =?";
        $paramType = 'ss';
        $paramValue = array(
            $hallid,
            $userid
        );

        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $count;
    }

    /*No. of video views*/
    public function getVideoViewsCount($videoid)
    {
        $query = "select * from tbl_exhibitor_videos_views where video_id =?";
        $paramType = 's';
        $paramValue = array(
            $videoid
        );

        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $count;
    }

    /*Get List of video id and count*/
    public function getVideoViews()
    {
        $query = "SELECT tbl_exhibitor_videos_views.video_id, count(tbl_exhibitor_videos_views.video_id) as count FROM `tbl_exhibitor_videos_views`, tbl_exhibitor_videos where tbl_exhibitor_videos_views.video_id=tbl_exhibitor_videos.video_id GROUP by tbl_exhibitor_videos_views.video_id";
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->select($query, $paramType, $paramValue);

        return $count;
    }
    public function getExhVideoViews($exhibitorid)
    {
        $query = "SELECT tbl_exhibitor_videos_views.video_id, count(tbl_exhibitor_videos_views.video_id) as count FROM `tbl_exhibitor_videos_views`, tbl_exhibitor_videos, tbl_exhibitors where tbl_exhibitor_videos_views.video_id=tbl_exhibitor_videos.video_id and tbl_exhibitor_videos.exhibitor_id=tbl_exhibitors.exhibitor_id and tbl_exhibitors.exhibitor_id=? GROUP by tbl_exhibitor_videos_views.video_id order by count desc";
        $paramType = 's';
        $paramValue = array(
            $exhibitorid
        );

        $count = $this->ds->select($query, $paramType, $paramValue);

        return $count;
    }
    /*Get Video */
    public function getExhVideo($videoid)
    {
        $query = "SELECT * from tbl_exhibitor_videos where video_id=?";
        $paramType = 's';
        $paramValue = array(
            $videoid
        );

        $count = $this->ds->select($query, $paramType, $paramValue);

        return $count;
    }

    /*Get ResourceList */
    public function getExhResList($exhibitorid)
    {
        $query = "SELECT * from tbl_exhibitor_resources where exhibitor_id=? order by download_count desc";
        $paramType = 's';
        $paramValue = array(
            $exhibitorid
        );

        $count = $this->ds->select($query, $paramType, $paramValue);

        return $count;
    }
    /*Get Resource */
    public function getExhResource($resid)
    {
        $query = "SELECT * from tbl_exhibitor_resources where resource_id=?";
        $paramType = 's';
        $paramValue = array(
            $resid
        );

        $count = $this->ds->select($query, $paramType, $paramValue);

        return $count;
    }

    /*Count of all visitors to exhibition halls*/
    public function getExhVisitorsCount()
    {
        $query = "select count(distinct user_id) as cnt from tbl_exhibitor_visitors";
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->select($query, $paramType, $paramValue);

        return $count[0]['cnt'];
    }

    /*Count of visitors to individual exhibition hall*/
    public function getVisitorsCountByExh()
    {
        $query = "SELECT count(DISTINCT user_id) as cnt, exhibitor_id FROM `tbl_exhibitor_visitors` GROUP by exhibitor_id ORDER by cnt desc limit 10";
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->select($query, $paramType, $paramValue);

        return $count;
    }

    /*Count of halls visited by individual*/
    public function getVisitorsExhVisits()
    {
        $query = "SELECT count(DISTINCT exhibitor_id) as cnt, user_id FROM `tbl_exhibitor_visitors`  GROUP by user_id ORDER by cnt DESC limit 10";
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->select($query, $paramType, $paramValue);

        return $count;
    }

    /*Total Time spend at Halls*/
    public function getExhTimeSpent()
    {
        $query = "SELECT SUM(TIMESTAMPDIFF(SECOND, entry_time, exit_time)) as total FROM tbl_exhibitor_visitors";
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->select($query, $paramType, $paramValue);

        return $count[0]['total'];
    }

    /*Total Time spend at Halls by each person*/
    public function getExhVisitorTimeSpent()
    {
        $query = "SELECT SUM(TIMESTAMPDIFF(SECOND, entry_time, exit_time)) as total, user_id  FROM tbl_exhibitor_visitors GROUP by user_id ORDER by total DESC limit 10";
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->select($query, $paramType, $paramValue);

        return $count;
    }

    /*Total Time spend at each Hall by all visitors*/
    public function getExhTotalTimeSpent()
    {
        $query = "SELECT SUM(TIMESTAMPDIFF(SECOND, entry_time, exit_time)) as total, exhibitor_id  FROM tbl_exhibitor_visitors GROUP by exhibitor_id ORDER by total DESC limit 10";
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->select($query, $paramType, $paramValue);

        return $count;
    }

    /*Sample Request*/
    public function subRequests($exhId, $userid, $req)
    {
        $reqtime   = date('Y/m/d H:i:s');
        $query = "Insert into tbl_exhibitor_queries(exhibitor_id, user_id, request_for, req_time) values(?, ?, ?,?)";
        $paramType = 'ssss';
        $paramValue = array(
            $exhId,
            $userid,
            $req,
            $reqtime
        );

        $reqid = $this->ds->insert($query, $paramType, $paramValue);
        return $reqid;
    }
}
