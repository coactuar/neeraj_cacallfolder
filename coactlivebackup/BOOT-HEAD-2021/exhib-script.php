<script>
  $(function() {
    $('.resdl').on('click', function() {

      var res_id = $(this).data('docid');
      $.ajax({
        url: 'control/exhib.php',
        data: {
          action: 'updateFileDLCount',
          resId: res_id,
          userId: '<?= $userid ?>'
        },
        type: 'post',
        success: function() {
          //console.log(data);
        }
      });

    });

    $('.vidview').on('click', function() {
      var vid_id = $(this).data('vidid');
      $.ajax({
        url: 'control/exhib.php',
        data: {
          action: 'updateVideoView',
          vidId: vid_id,
          userId: '<?= $userid ?>'
        },
        type: 'post',
        success: function(response) {
          //console.log(response);
        }
      });

    });
  });
</script>