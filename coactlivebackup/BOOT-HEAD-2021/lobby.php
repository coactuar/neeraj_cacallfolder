<?php
require_once "logincheck.php";
$curr_room = 'lobby';

?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/lobby.jpg" usemap="#image-map">
            <map name="image-map">
               
                <!-- <area alt="Connect" title="Connect" href="#" coords="3500,1284,3478,1791,3734,1796,3747,1292" shape="poly"> -->
                <area alt="Photo Booth" title="Photo Booth" href="photobooth.php" coords="2520,1472,2184,1233" shape="rect">
            </map>
            <a href="https://player.vimeo.com/video/481880745" id="lobbyVideo" class="viewvideo"></a>
            <a href="#" id="enterAudi1" onClick="enterAudi1()">
                <div class="indicator d-6"></div>
            </a>
            <a href="#" id="enterAudi2" onClick="enterAudi2()">
                <div class="indicator d-6"></div>
            </a>
            <a href="#" id="enterAudi3" onClick="enterAudi3()">
                <div class="indicator d-6"></div>
            </a>
            <a href="games.php" id="engagement" >
                <div class="indicator d-6"></div>
            </a>
            <a href="exhibitionhalls.php" id="enterHall">
                <div class="indicator d-6"></div>
            </a>
            <a href="lounge.php" id="enterLounge">
                <div class="indicator d-6"></div>
            </a>
            <a href="photobooth.php" id="photoboot">
                <div class="indicator d-6"></div>
            </a>
            <a href="#" id="timeline">
                <div class="indicator d-6"></div>
            </a>
            <a href="https://origyn.s3.ap-south-1.amazonaws.com/conf-agenda.pdf" id="showAgenda">
                <div class="indicator d-6"></div>
            </a>
            <a href="resource.php" id="resource">
                <div class="indicator d-6"></div>
            </a>
            <a href="digital_cert.php" id="digitalcertificate">
                <div class="indicator d-6"></div>
            </a>
            <!--
            <a href="https://www.facebook.com/Integracehealth" target="_blank" id="connectFb"></a>-->
            <!-- <a href="#" target="_blank" id="connectTwtr"></a> -->
            <!-- <a href="https://www.instagram.com/integracehealthofficial/" target="_blank" id="connectInsta"></a>
            <a href="https://www.linkedin.com/company/integracehealth" target="_blank" id="connectLinked"></a>
            <a class="showpdf" href="https://origyn.s3.ap-south-1.amazonaws.com/conf-agenda.pdf" id="showAgenda"></a> -->
            <!-- <a class="showpdf" href="assets/resources/conf-agenda.pdf" id="showProfile"></a> -->
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<section class="videotoplay" id="gotoaudi1" style="display:none;">
    <video class="videoplayer" id="gotoaudi1video" preload="auto">
        <source src="toaudi.mp4" type="video/mp4">
    </video>
</section>
<section class="videotoplay" id="gotoaudi2" style="display:none;">
    <video class="videoplayer" id="gotoaudi2video" preload="auto">
        <source src="toaudi.mp4" type="video/mp4">
    </video>
</section>
<section class="videotoplay" id="gotoaudi3" style="display:none;">
    <video class="videoplayer" id="gotoaudi3video" preload="auto">
        <source src="toaudi.mp4" type="video/mp4">
    </video>
</section>
<?php require_once "scripts.php" ?>
<script>
    var audi1Video = document.getElementById("gotoaudi1video");
    audi1Video.addEventListener('ended', audi1End, false);
    var audi2Video = document.getElementById("gotoaudi2video");
    audi2Video.addEventListener('ended', audi2End, false);
    var audi3Video = document.getElementById("gotoaudi3video");
    audi3Video.addEventListener('ended', audi3End, false);

    function enterAudi1() {
        $('#content').css('display', 'none');
        $('#gotoaudi1').css('display', 'block');
        audi1Video.currentTime = 0;
        audi1Video.play();
    }

    function audi1End(e) {
        location.href = "auditorium1.php";
    }

    function enterAudi2() {
        $('#content').css('display', 'none');
        $('#gotoaudi2').css('display', 'block');
        audi2Video.currentTime = 0;
        audi2Video.play();
    }

    function audi2End(e) {
        location.href = "auditorium2.php";
    }

    function enterAudi3() {
        $('#content').css('display', 'none');
        $('#gotoaudi3').css('display', 'block');
        audi3Video.currentTime = 0;
        audi3Video.play();
    }

    function audi3End(e) {
        location.href = "auditorium3.php";
    }

</script>
<script src="assets/js/image-map.js"></script>
<script>
    ImageMap('img[usemap]', 500);
</script>
<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>