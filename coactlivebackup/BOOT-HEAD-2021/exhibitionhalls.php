<?php
require_once "logincheck.php";
$curr_room = 'exhibitionhall';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/exhibitionhall.jpg" usemap="#image-map">
            <map name="image-map">
            <area  title="TV" href="https://player.vimeo.com/video/481733317" class="viewvideo" coords="4583,1166,3353,445" shape="rect">\
            
            <area  alt="Dubinor Left" title="Dubinor Left" href="dubinor-left.php" coords="1388,1944,1961,2524" shape="rect">
            <area  alt="Esoz" title="Esoz" href="ebovpg.php" coords="2894,2806,2258,2262" shape="rect">
            <area  alt="Stiloz" title="Stiloz" href="stiloz.php" coords="3304,2368,2915,1951" shape="rect">
            <area  alt="Dubinor" title="Dubinor" href="dubinor.php" coords="4300,1930,4731,2375" shape="rect">
            <area  alt="Vkinnecthealth" title="Vkinnecthealth" href="vkinnecthealth.php" coords="4774,2213,5360,2778" shape="rect">
            <area  alt="Bandhan" title="Bandhan" href="bandhan.php" coords="6328,1837,5777,2312" shape="rect">
            <area  alt="BonDk" title="BonDk" href="bondk.php" coords="1820,1428,2265,1803" shape="rect">
            <area  alt="Esoz" title="Esoz" href="esoz.php" coords="2534,1520,2972,1937" shape="rect">
            <area  alt="Colsmart" title="Colsmart" href="colsmartA.php" coords="3615,1414,4046,1781" shape="rect">
            <area  alt="BondK2" title="BondK2" href="bondk2.php" coords="5071,1477,4576,1916" shape="rect">
            <area  alt="Lizolid" title="Lizolid" href="lizolid.php" coords="5346,1378,5834,1788" shape="rect">
                <!-- <area href="mumfermax.php" coords="257,3069,278,2391,307,2249,1409,2200,1494,2341,1487,2949" shape="poly">
                <area href="fenza.php" coords="1777,2569,1791,2109,1904,2088,1918,1961,2271,1954,2278,2032,2596,2060,2611,2512" shape="poly">
                <area href="vytal.php" coords="2985,2368,2992,1951,3077,1951,3084,1831,3635,1838,3628,1951,3699,1951,3699,2375" shape="poly">
                <area href="fenzawash.php" coords="4165,2368,4172,1965,4328,1944,4335,1838,4717,1810,4710,1951,4872,1979,4879,2375" shape="poly">
                <area href="dubagest.php" coords="5233,2508,5240,2063,5310,2056,5331,1893,5946,1900,5946,2077,6031,2098,6038,2579" shape="poly">
                <area href="iq.php" coords="6278,2941,6278,2326,6384,2304,6392,2170,7473,2227,7466,2403,7466,3054" shape="poly"> -->
            </map>
           
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "scripts.php" ?>
<script src="assets/js/image-map.js"></script>
<script>
    ImageMap('img[usemap]', 500);
</script>
<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>
