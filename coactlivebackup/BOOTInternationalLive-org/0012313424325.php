<?php 
include('commons/header.php');
//require ('functions/reg.php');



if (empty($_GET['title'])){
    $title ='';
	$varified='0';
}else{
    $title =$_GET['title'];
	$varified='1';
}

if (empty($_GET['first_name'])){
    $fname ='';
}else{
    $fname =$_GET['first_name'];
}

if (empty($_GET['last_name'])){
    $lname ='';
}else{
    $lname =$_GET['last_name'];
}

if (empty($_GET['email_id'])){
    $email ='';
}else{
    $email =$_GET['email_id'];
}

if (empty($_GET['phone_no'])){
    $phone ='';
}else{
    $phone =$_GET['phone_no'];
}

if (empty($_GET['speciality'])){
    $specialty ='';
}else{
    $specialty =$_GET['speciality'];
}

if (empty($_GET['education'])){
    $education ='';
}else{
    $education =$_GET['education'];
}

if (empty($_GET['areaofintrest'])){
    $aoi ='';
}else{
    $aoi =$_GET['areaofintrest'];
}








?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $event_title; ?></title>
<link rel="stylesheet" href="assects/css/bootstrap.min.css">
<link rel="stylesheet" href="assects/css/all.min.css">
<link rel="stylesheet" href="assects/css/styles.css">

</head>

<body>

	<div class="container">
        <div class="row no-margin">
            <div class="col-12">
                <img src="assects/img/reg-banner.png" class="img-fluid" alt=""/> 
            </div>
        </div>
        <div class="row bg-white color-grey">
            <div class="col-12 text-center">
                 <h3 class="reg-title">Register for BOOT International Live!</h3>
            </div>
        </div>
        <div class="row bg-white color-grey">
            <div class="col-12 col-md-8 offset-md-2">
              
                <div id="register-area">
                 
                  <form method="POST" id="reg-form">
				 
                  <input type="hidden" id="app" name="app" value="<?= $varified; ?>"> 
                      <div class="row mt-2">
					   <div class="col-12 col-md-2">
                              <label>Title<sup class="req">*</sup></label>
                              <input type="text" id="title" name="title" class="input" value="<?php echo $title; ?>" autocomplete="off" required>
                          </div>
                          <div class="col-12 col-md-5">
                              <label>First Name<sup class="req">*</sup></label>
                              <input type="text" id="fname" name="fname" class="input" value="<?php echo $fname; ?>" autocomplete="off" required>
                          </div>
                          <div class="col-12 col-md-5">
                              <label>Last Name<sup class="req">*</sup></label>
                              <input type="text" id="lname" name="lname" class="input" value="<?php echo $lname; ?>" autocomplete="off" required>
                          </div>
                      </div>
                      <div class="row mb-1">
                        <div class="col-12">
                            Please enter the name correctly as the same will be used for creating Attendance Certificate.
                        </div>
                      </div>
                      <div class="row mt-3 mb-1">
                          <div class="col-12 col-md-6">
                              <label>Email ID<sup class="req">*</sup></label>
                              <input type="email" id="emailid" name="emailid" class="input" value="<?php echo $email; ?>" autocomplete="off" required>
                          </div>
						    <div class="col-12 col-md-6">
                              <label>Phone No.<sup class="req">*</sup></label>
                              <input type="number" id="phone" name="phone" class="input" value="<?php echo $phone; ?>" autocomplete="off" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
                          </div>
                      </div>
					   <!--
                      <div class="row mt-3 mb-1">
                          <div class="col-12 col-md-6">
                              <label>Phone No.<sup class="req">*</sup></label>
                              <input type="number" id="phone" name="phone" class="input" value="<?php echo $phone; ?>" autocomplete="off" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" required>
                          </div>
						 
                          <div class="col-12 col-md-6">
                               <label>Specialty<sup class="req">*</sup></label>
                              <input type="text" id="specialty" name="specialty" class="input" value="<?php echo $specialty ; ?>" autocomplete="off" required>
                          </div>
                          
                      </div>
					 
                      <div class="row mt-3 mb-1">
                          <div class="col-12 col-md-6">
                               <label>Education<sup class="req">*</sup></label>
                              <input type="text" id="education" name="education" class="input" value="<?php echo $education ; ?>" autocomplete="off" required>
                          </div>
                          <div class="col-12 col-md-6">
                             
                               <label>Areas of interest<sup class="req">*</sup></label>
                              <input type="text" id="aoi" name="areaofintrest" class="input" value="<?php echo $aoi ; ?>" autocomplete="off" required>
                                
                          </div>
                      </div>
					  -->
					  <div class="row mt-3 mb-1">
                          <div class="col-12 col-md-6">
                              <label>Pincode<sup class="req">*</sup></label>
                              <input type="number" id="pincode" name="pincode" class="input" value="" autocomplete="off" required>
                          </div>
                          <div class="col-12 col-md-6">
                              <label>Country<sup class="req">*</sup></label>
                              <div id="countries">
                              <select class="input" id="country" name="country" onChange="updateState()" required>
                                  <option>Select Country</option>
                              </select>
                              </div>
                          </div>
                          
                      </div>
					   <div class="row mt-3 mb-1">
                          <div class="col-12 col-md-6">
                              <label>State<sup class="req">*</sup></label>
                              <div id="states">
                              <select class="input" id="state" name="state" onChange="updateCity()" required>
                                  <option>Select State</option>
                              </select>
                              </div>
                          </div>
                          <div class="col-12 col-md-6">
                              <label>City<sup class="req">*</sup></label>
                              <div id="cities">
                              <select class="input" id="city" name="city" required>
                                  <option>Select City</option>
                              </select>
                              </div>
                          </div>
                      </div>
                      <div class="row mt-3 mb-1">
                          <div class="col-12 text-left">
                              <label>Select Topics of Interest:<sup class="req">*</sup></label>
                           
                               <label>What topics would be attended in each of the 3 days<sup class="req">*</sup></label>
                              <label><b>Day 1</b></label>
                              <input type="checkbox" name="topic[]" value="upper-trauma">Upper Extremity Trauma <br>
							   <input type="checkbox" name="topic[]" value="foot-ankle">Foot &amp; Ankle Trauma<br>
                              <input type="checkbox" name="topic[]" value="sine-and-pathological">Spine Trauma & Pathological fractures<br>
							   <label><b>Day 2</b></label>
							   <input type="checkbox" name="topic[]" value="knee-trauma">Knee Trauma<br>
							   <input type="checkbox" name="topic[]" value="hip-and-pelvic-trauma">Hip & Pelvic Trauma<br>
							   <input type="checkbox" name="topic[]" value="infections-management">Infections management<br>
								<label><b>Day 3</b></label>
                              <input type="checkbox" name="topic[]" value="ota-president-oration">OTA President Oration<br>
                              <input type="checkbox" name="topic[]" value="breakthrough-innovation-in-trauma">Breakthrough/ Innovation in Trauma<br>
                              <input type="checkbox" name="topic[]" value="take-home-messages">Take home Messages<br>
                          </div>
                      </div>
                      <div class="row divider">
                          <div class="col-12">
                              <input type="checkbox" name="updates[]" value="program">I would like to receive updates related to this program <br>
                              <input type="checkbox" name="updates[]" value="Integrace">I would like to receive updates related to Integrace Pvt. Ltd. <br>
                          </div>
                      </div>
                      <div class="row mt-2 mb-3">
                          <div class="col-12">
                              <small><sup class="req">*</sup> denotes mandatory fields.</small><br><br>
                              <input type="submit" name="reguser-btn" id="btnSubmit" class="form-submit btn-register" value="" />
                              <a href="./" class="form-cancel"><img src="assects/img/cancel-btn.jpg" alt=""/></a>
                          </div>
                      
                      </div>
                  </form>
                </div>
               
                 
            
                 
            </div>
        </div>
         <div class="row bg-white">
            <div class="col-12">
                <img src="assects/img/line-h.jpg" class="img-fluid" alt=""/> 
            </div>
        </div>
        <div class="row bg-white p-2">
            <div class="col-4 bor-right p-2 text-center">
                <img src="assects/img/in-assoc.png" class="img-fluid bot-img" alt=""/>
            </div>
            <div class="col-4 p-2 text-center">
                <img src="assects/img/sci-partner.png" class="img-fluid bot-img" alt=""/>
            </div>
          <div class="col-4 bor-left p-2 text-center color-grey">
                <img src="assects/img/brought-by.png" class="img-fluid bot-img" alt=""/>
                <div class="visit">
                Visit us at <a href="https://www.integracehealth.com/about.html" class="link" target="_blank">https://www.integracehealth.com/about.html</a>
                </div>
          </div>
        </div>
	
        
	</div>
<div id="code">IPL/O/BR/09042021</div>

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
   <h4> <div id="login-message"></div></h4>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>

<script src="assects/js/jquery.min.js"></script>
<script src="assects/js/bootstrap.min.js"></script>

<script>
$(function() {
    $("select").trigger("change");
});
$(document).on('submit', '#reg-form', function()
{  
  $.post('functions/reg.php', $(this).serialize(), function(data)
  {
      console.log(data);
      if(data == 's')
      {
        $('#login-message').text('You are registered succesfully for the BOOT International Live. Please check your email regarding instructions on how to login.');
        $('#login-message').addClass('alert-success');
		$("#exampleModalCenter").modal('show');
		 $('#btnSubmit').fadeOut(); 
		   $('#btnSubmit').delay(5000).fadeIn();
        
          return false;
      }
      else if (data == '1')
      {
          $('#login-message').text('You are already registered.');
          $('#login-message').addClass('alert-danger');
		$("#exampleModalCenter").modal('show');
         
		   $('#btnSubmit').fadeOut(); 
		     $('#btnSubmit').delay(5000).fadeIn();
          return false;
      }
      else
      {

          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger');
		  $("#exampleModalCenter").modal('show');  
		  $('#btnSubmit').fadeOut(); 	  
		  $('#btnSubmit').delay(5000).fadeIn(); 	  
          return false;
      }
  });
  
  return false;
});
</script>
<script>
function getCountries()
{
    $.ajax({
        url: 'functions/server.php',
        data: {action: 'getcountries'},
        type: 'post',
        success: function(response) {
            
            $("#countries").html(response);
        }
    });
    
}

function updateState()
{
    var c = $('#country').val();
    if(c!='0'){
        $.ajax({
            url: 'functions/server.php',
            data: {action: 'getstates', country : c },
            type: 'post',
            success: function(response) {
                
                $("#states").html(response);
            }
        });
    }
}

function updateCity()
{
    var s = $('#state').val();
    if(s!='0'){
        $.ajax({
            url: 'functions/server.php',
            data: {action: 'getcities', state : s },
            type: 'post',
            success: function(response) {
                
                $("#cities").html(response);
            }
        });
    }
}

getCountries();
//updateState();
</script>


</body>
</html>